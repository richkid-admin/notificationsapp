﻿using System;
using System.Net.Http;
using System.Security;
using System.Security.Principal;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace NotificationsAppAPI.Filters
{
    public class AuthenticaionAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            // Checking if client is allowed to send a request to the API
            // Client sends the account key in base 64
            // the function decode it and then compares it with the account key stored in DB
            try
            {
                if (actionContext.Request.Headers.Authorization != null)
                {
                    string decodeAuthToken = string.Empty;
                    var authToken = actionContext.Request.Headers.Authorization.Parameter;
                    var checkIfBearer = actionContext.Request.Headers.Authorization.Scheme;
                    try
                    {
                        decodeAuthToken = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(authToken));

                    }
                    catch (Exception ex)
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                        return;
                    }

                    if (IsAccountKeyExists(decodeAuthToken) && checkIfBearer.Equals("Bearer"))
                    {
                        try
                        {
                            Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(decodeAuthToken), null);
                        }
                        catch (SecurityException secureException)
                        {
                            Console.WriteLine("{0}: Permission to set Principal " +
                                "is denied.", secureException.GetType().Name);
                        }
                    }
                    else
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                        return;
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
                    return;
                }
            }
            catch (Exception ex)
            {
                actionContext.Response = actionContext.Request.CreateResponse(System.Net.HttpStatusCode.BadGateway);
                return;
            }
        }

        private bool IsAccountKeyExists(string accountKey)
        {
            return BLL.Services.AccountServices.CheckIfAccountExistByAccountKey(accountKey);
        }
    }
}