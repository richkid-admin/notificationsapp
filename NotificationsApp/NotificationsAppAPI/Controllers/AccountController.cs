﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NotificationsAppAPI.Controllers
{
    public class AccountController : ApiController
    {     
        [Filters.Authenticaion]
        [HttpGet]
        public async Task<string> GetAccount()
        {
            string requestBody = await Request.Content.ReadAsStringAsync();
            string accountKey = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Request.Headers.Authorization.Parameter));

            BLL.Account account = BLL.Services.AccountServices.LoadAccountByAccountKey(accountKey);
            account.IsActivated = BLL.Services.AccountServices.IsAccountActive(account);
            if (account == null || account.AccountID == 0)
            {
                throw new HttpRequestException(HttpStatusCode.NoContent.ToString());
            }

            var JSONAccount = JsonConvert.SerializeObject(account);

            if (JSONAccount != null)
            {
                return JSONAccount;
            }
            else
            {
                throw new HttpRequestException(HttpStatusCode.NoContent.ToString());
            }
        }
    }
}
