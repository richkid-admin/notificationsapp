﻿using BLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using BLL.Services;
using PostalServices;

namespace NotificationsAppAPI.Controllers
{
    public class MessageController : ApiController
    {
        [Filters.Authenticaion]
        [HttpGet]
        public async Task<string> GetAllMessagesOfAccount()
        {
            string requestBody = await Request.Content.ReadAsStringAsync();
            string accountKey = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Request.Headers.Authorization.Parameter));

            List<BLL.Message> messages = BLL.Services.MessageServices.LoadAllMessagesByAccountKey(accountKey);

            if (messages == null)
            {
                throw new HttpRequestException(HttpStatusCode.NotFound.ToString());
            }

            var JSONMessages = JsonConvert.SerializeObject(messages);

            return JSONMessages;
        }

        [Filters.Authenticaion]
        [HttpPost]
        [Obsolete("Moved to services during automated messages worker development",true)]
        public async Task<string> PostMessage_BeforeAutomation()
        {
            dynamic jPostMessageLog = new JObject();

            string requestBody = await Request.Content.ReadAsStringAsync();
            string accountKey = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Request.Headers.Authorization.Parameter));
            jPostMessageLog.AccountKey = accountKey;

            Account account = AccountServices.LoadAccountByAccountKey(accountKey);
            jPostMessageLog.Account = Newtonsoft.Json.JsonConvert.SerializeObject(account);

            Package package = PackageServices.LoadPackageByID(account.PackageID);

            PulseemPostalService pulseemPostalService = new PulseemPostalService();
            dynamic tempMessage = new JObject();
            Message message = new Message();

            jPostMessageLog.Status = "Account and package data loaded";
            // Check conditions to send message:
            // 1. Active account => Number of subscribers <= 200 OR payment approved by Mor/Chen
            // 2. Number of subscribers <= Max Subscribers of current package of the account
            try
            {
                bool accountAllowedToSend = account.NumberOfSubscribers <= package.MaxSubscribers && AccountServices.IsAccountActive(account);
                jPostMessageLog.SendingAllowed = accountAllowedToSend;
                jPostMessageLog.Status = "Sending" + (accountAllowedToSend ? " " : "not") + "allowed";

                if (accountAllowedToSend)
                {
                    message.AccountID = account.AccountID;
                    try
                    {
                        tempMessage = JsonConvert.DeserializeObject(requestBody);
                        jPostMessageLog.Message = tempMessage;
                    }
                    catch (Exception ex)
                    {
                        jPostMessageLog.Status = "Input message details unpacking failed";
                        throw new HttpRequestException(HttpStatusCode.BadRequest.ToString());
                    }
                    // Required fields -> Message Display Name, Message Headline, Message Text -> Can not be null or empty
                    // Validate required fields and populate message fields
                    if (MessageServices.ValidateInputAndPopulateMessage(tempMessage, message, account.NumberOfSubscribers))
                    {
                        //save message before sending notification
                        message.MessageID = BLL.Services.MessageServices.Save(message);
                        jPostMessageLog.Message.MessageID = message.MessageID;

                        if (message.MessageID == 0)
                        {
                            jPostMessageLog.Status = "Message not saved correctly BEFORE sending to delivery service";

                        }

                        message.NotificationID = pulseemPostalService.SendNotification(account.Data.Credentials.PulseemAPIKey, message, DateTime.Now);
                        jPostMessageLog.Message.NotificationID = message.NotificationID;
                        if (message.NotificationID == 0)
                        {
                            jPostMessageLog.Status = "Message failed to be sent to pulseem";
                            throw new HttpRequestException(HttpStatusCode.InternalServerError.ToString());
                        }
                        else
                        {
                            message.Status = "Created";
                            jPostMessageLog.Message.Status = message.Status;

                            message.MessageID = MessageServices.Save(message); // if we got here - message can be properly created and saved
                            if (message.MessageID == 0)
                            {
                                jPostMessageLog.Status = "Message not saved correctly AFTER sending to delivery service";
                                throw new HttpRequestException(HttpStatusCode.InternalServerError.ToString());
                            }
                        }
                    }
                    else
                    {
                        jPostMessageLog.Status = "Input message details received are not valid";
                        throw new HttpRequestException(HttpStatusCode.BadRequest.ToString()); // Message body is incorrect
                    }
                }
                else
                {
                    throw new HttpRequestException(HttpStatusCode.MethodNotAllowed.ToString());
                }
                var JSONMessage = JsonConvert.SerializeObject(message);

                if (JSONMessage != null)
                {
                    jPostMessageLog.Status = "Message was saved and sent successfully";
                    return JSONMessage;
                }
                else
                {
                    jPostMessageLog.Status = "Message failed serialization to response object";
                    throw new HttpRequestException(HttpStatusCode.NotFound.ToString());
                }
            }
            catch (Exception ex)
            {
                dynamic respone = new JObject();

                if (ex.Message.Equals("BadRequest"))
                {
                    respone.fail = "Message fields were not filled correctly. Please check data before try again!";
                }
                else if (ex.Message.Equals("MethodNotAllowed"))
                {
                    respone.fail = "Account number: " + account.AccountID + " is not active or number of subscribers greater than current package's max subscribers. Please check data before try again!";
                }
                else // InternalServerError => Message save failed or notification not created in pulseem
                {
                    respone.fail = "Message not being saved in DB or notification not created in pulseem. Please check data before try again!";
                }
                jPostMessageLog.StatusInternalDescription = respone.fail;
                jPostMessageLog.Errors = ex.ToString();

                return JsonConvert.SerializeObject(respone);
            }
            finally
            {
                string sPostMessageLog = Newtonsoft.Json.JsonConvert.SerializeObject(jPostMessageLog);
                BLL.Services.LogServices.WriteLog("PushNotificationLogs/PostMessageLogs", "PostMessageLog", sPostMessageLog);
            }

        }
        [Filters.Authenticaion]
        [HttpPost]
        public async Task<string> PostMessage()
        {
            dynamic jManualPostMessageLog = new JObject(); 
            PulseemPostalService pulseemPostalService = new PulseemPostalService();
            dynamic tempMessage = new JObject();
            Account account = new Account();
            Package package = new Package();
            string sOutMessage = string.Empty;

            try
            {
                string requestBody = await Request.Content.ReadAsStringAsync();
                string accountKey = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Request.Headers.Authorization.Parameter));
                jManualPostMessageLog.AccountKey = accountKey;

                account = AccountServices.LoadAccountByAccountKey(accountKey);
                if (account == null)
                {
                    jManualPostMessageLog.Status = $"Account could not be loaded with account key: {accountKey}";
                    throw new HttpRequestException(HttpStatusCode.BadRequest.ToString());
                }
                jManualPostMessageLog.Account = JObject.FromObject(account);

                package = PackageServices.LoadPackageByID(account.PackageID);
                if (package.PackageID == 0)
                {
                    jManualPostMessageLog.Status = $"Package could not be loaded for account id: {account.AccountID}";
                    throw new HttpRequestException(HttpStatusCode.InternalServerError.ToString());
                }

                jManualPostMessageLog.Status = "Account and package data loaded";
                try
                {
                    tempMessage = JsonConvert.DeserializeObject(requestBody);
                    jManualPostMessageLog.InMessage = tempMessage;
                }
                catch (Exception ex)
                {
                    jManualPostMessageLog.Status = "Input message details unpacking failed";
                    throw new HttpRequestException(HttpStatusCode.BadRequest.ToString());
                }
                BLL.PostMessageLog postMessageLog = Services.PostMessageServices.PostMessage(tempMessage, account, package, pulseemPostalService);
                jManualPostMessageLog.PostMessageLog = JObject.FromObject(postMessageLog);
                Exception exceptionIfPostMessageFailed = null;
                jManualPostMessageLog.Status = postMessageLog.StatusDescription;
                switch (postMessageLog.PostMessageStatus)
                {
                    case PostMessageStatus.SendingNotAllowed:
                        exceptionIfPostMessageFailed = new HttpRequestException(HttpStatusCode.MethodNotAllowed.ToString());
                        break;
                    case PostMessageStatus.MessageNotValidatedAndCopied:
                        exceptionIfPostMessageFailed = new HttpRequestException(HttpStatusCode.BadRequest.ToString());
                        break;
                    case PostMessageStatus.MessageNotSavedBeforeSend:
                    case PostMessageStatus.MessageNotSent:
                    case PostMessageStatus.MessageNotSavedAfterSend:
                        exceptionIfPostMessageFailed = new HttpRequestException(HttpStatusCode.InternalServerError.ToString());
                        break;
                }
                if (exceptionIfPostMessageFailed != null)
                {
                    throw exceptionIfPostMessageFailed;
                }

                sOutMessage = JsonConvert.SerializeObject(postMessageLog.OutMessage);

                if (!string.IsNullOrEmpty(sOutMessage))
                {
                    jManualPostMessageLog.Status = "Message was saved and sent successfully";
                }
                else
                {
                    jManualPostMessageLog.Status = "Message failed serialization to response object";
                    throw new HttpRequestException(HttpStatusCode.NotFound.ToString());
                }
            }
            catch (Exception ex)
            {
                dynamic respone = new JObject();

                if (ex.Message.Equals("BadRequest"))
                {
                    respone.fail = "Message fields were not filled correctly. Please check data before try again!";
                }
                else if (ex.Message.Equals("MethodNotAllowed"))
                {
                    respone.fail = "Account number: " + account.AccountID + " is not active or number of subscribers greater than current package's max subscribers. Please check data before try again!";
                }
                else // InternalServerError => Message save failed or notification not created in pulseem
                {
                    respone.fail = "Message not being saved in DB or notification not created in pulseem. Please check data before try again!";
                }
                sOutMessage = JsonConvert.SerializeObject(respone);

                jManualPostMessageLog.Status = respone.fail;
                jManualPostMessageLog.Errors = ex.ToString();

            }
            finally
            {
                string sPostMessageLog = Newtonsoft.Json.JsonConvert.SerializeObject(jManualPostMessageLog);
                BLL.Services.LogServices.WriteLog("PushNotificationLogs/ManualPostMessageLogs" /*PostMessageRequestLogs*/, "PostMessageRequestLog", sPostMessageLog);
            }
            return sOutMessage;

        }
    }
}
