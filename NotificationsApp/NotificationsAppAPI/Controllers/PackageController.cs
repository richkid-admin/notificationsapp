﻿using BLL;
using BLL.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NotificationsAppAPI.Controllers
{
    public class PackageController : ApiController
    {
        [Filters.Authenticaion]
        [HttpGet]
        public async Task<string> GetAllPackages()
        {
            string requestBody = await Request.Content.ReadAsStringAsync();
            string accountKey = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Request.Headers.Authorization.Parameter));

            List<BLL.Package> packages = BLL.Services.PackageServices.LoadAllPackages();

            if (packages == null)
            {
                throw new HttpRequestException(HttpStatusCode.NotFound.ToString());
            }

            var JSONMessages = JsonConvert.SerializeObject(packages);

            return JSONMessages;
        }

        [Filters.Authenticaion]
        [HttpPost]
        public async Task<string> UpgradePackage(int newPackageID)
        {
            string requestBody = await Request.Content.ReadAsStringAsync();
            string accountKey = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Request.Headers.Authorization.Parameter));
            Account account = AccountServices.LoadAccountByAccountKey(accountKey);
            AccountPackageLog accountPackageLog = new AccountPackageLog();
            dynamic respone = new JObject();

            if (account == null || account.AccountID == 0)
            {
                throw new HttpRequestException(HttpStatusCode.NotFound.ToString());
            }

            try
            {
                accountPackageLog = PackageServices.UpgradePackage(newPackageID, account);
            }
            catch (Exception ex)
            {
                
                if (ex.Message.Equals("NoContent"))
                {
                    respone.fail = "Problem With saving data in accountID number: " + account.AccountID + ", Please check data before try again!";
                }
                else if (ex.Message.Equals("NotFound"))
                {
                    respone.fail = "PackageID to upgrade not exists. Please check data before try again!";
                }
                else if (ex.Message.Equals("MethodNotAllowed"))
                {
                    respone.fail = "Account number: " + account.AccountID + " is not active. Please check data before try again!";
                }
                else // InternalServerError => log save failed
                {
                    respone.fail = "Problem With saving upgrade log. Please check data before try again!";
                }
            }


            if (respone.fail == null )
            {
                Services.MailServices.SendMailForAccountPackageUpgrade(accountPackageLog); // Confirmation mail to Mor and Chen
                respone.success = true;
            }
            return JsonConvert.SerializeObject(respone);
        }
    }
}
