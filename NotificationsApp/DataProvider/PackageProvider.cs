﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using Domain.Entities;

namespace DataProvider
{
    public class PackageProvider
    {
        internal PackageProvider()
        {
        }

        public List<BLL.Package> LoadAllPackcages()
        {
            List<Domain.Entities.Package> entityPackages = new List<Domain.Entities.Package>();
            List<BLL.Package> BLLPackages = new List<BLL.Package>();

            try
            {
                BLLPackages = ConvertListOfPackages(entityPackages);
            }
            catch (Exception)
            {
                BLLPackages = new List<BLL.Package>();
            }

            return BLLPackages;
        }

        internal List<BLL.Package> ConvertListOfPackages(List<Domain.Entities.Package> entityPackages)
        {
            List<BLL.Package> BLLPackages = new List<BLL.Package>();

            try
            {
                foreach (Domain.Entities.Package entityPackage in entityPackages)
                {
                    BLLPackages.Add(ConvertPackageEntityToBusiness(entityPackage));
                }
            }
            catch (Exception ex)
            {
                BLLPackages = new List<BLL.Package>();
            }

            return BLLPackages;
        }

        internal BLL.Package ConvertPackageEntityToBusiness(Domain.Entities.Package entityPackage)
        {
            BLL.Package BLLPackage = new BLL.Package();
            try
            {

                BLLPackage.PackageID = entityPackage.PackageID;
                BLLPackage.MaxSubscribers = (int)entityPackage.MaxSubscribers;
                BLLPackage.Name = entityPackage.Name;
                BLLPackage.Data = !string.IsNullOrEmpty(entityPackage.Data)
                    ? Newtonsoft.Json.JsonConvert.DeserializeObject<BLL.Package.PackageData>(entityPackage.Data) : new BLL.Package.PackageData();

            }
            catch (Exception ex)
            {
                BLLPackage = new BLL.Package();
            }
            return BLLPackage;
        }

        internal Domain.Entities.Package ConvertPackageBLLToBusiness(BLL.Package BLLPackage)
        {
            Domain.Entities.Package entityPackage = new Domain.Entities.Package();


            return entityPackage;
        }
    }
}