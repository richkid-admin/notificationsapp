﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WorkerWebServices
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            bool IsTestEnv = !System.Configuration.ConfigurationManager.AppSettings["Env"].Equals("Production");
            Environment.SetEnvironmentVariable("IsTestEnv", IsTestEnv.ToString());
            WorkerServices.AccountsSubscribersUpdateWorker.Run(HttpContext.Current);

        }
    }
}
