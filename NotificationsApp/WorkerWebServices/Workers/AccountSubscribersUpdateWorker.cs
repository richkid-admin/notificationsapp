﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Common;
namespace WorkerServices
{
    public class AccountsSubscribersUpdateWorker 
    {
        public static HttpContext _Context { get; set; }
        public static bool _IsTestEnv { get; set; }
        public static void Run(HttpContext context)
        {
            _Context = context;
            _IsTestEnv = bool.Parse(Environment.GetEnvironmentVariable("IsTestEnv"));
            TimeSpan TimeBetweenSamples = new TimeSpan(0, 10, 0);
            TimeSpan delayUntilInitialRun = TimeSpan.Zero;
            if (!_IsTestEnv)
            {
                TimeBetweenSamples = new TimeSpan(24, 0, 0);
                var startTimeToday = DateTime.Today.AddHours(9.0); //9 am - morning
                var startTimeTomorrow = startTimeToday.AddDays(1);
                var now = DateTime.Now;
                if (now > startTimeToday)
                {
                    delayUntilInitialRun = startTimeTomorrow - now;
                }
                else
                {
                    delayUntilInitialRun = startTimeToday - now;
                }
            }
            Worker StatisticsUpdateWorker = new Worker
            {
                RunFunction = SendMailToAccountsWithUpdatedSubscribersCount,
                TimeInterval = TimeBetweenSamples,
                TimeDelay = delayUntilInitialRun
            };

            StatisticsUpdateWorker.StartWorker();

        }

        private static bool SendMailToAccountsWithUpdatedSubscribersCount() 
        {
            bool success = true;
            dynamic jarrLog = new JArray();

            List<BLL.Message> messagesToUpdateAllAccounts = new List<BLL.Message>();
            List<BLL.Account> allAccounts = new List<BLL.Account>();


            if (_IsTestEnv)
            {
                var richkidShop = BLL.Services.AccountServices.LoadAccountByID(119);
                allAccounts.Add(richkidShop);

            }
            else
            {
                allAccounts = BLL.Services.AccountServices.LoadAllAccounts();
            }


            var accountIDToLastSendLogForAccount = Services.AccountSubscribersUpdateLogServices.LoadLastLogForAccounts();
            foreach (var account in allAccounts)
            {
                var log = new BLL.AccountSubscribersUpdateLog();
                log.AccountID = account.AccountID;
                log.IsSuccessful = true;
                log.UpdateSendDate = DateTime.Now;
                bool shouldAccountRecieveMail = false;
                try
                {
                    shouldAccountRecieveMail = Services.AccountSubscribersUpdateLogServices.ShouldAccountRecieveMail(account, accountIDToLastSendLogForAccount, log,_IsTestEnv);
                    if (shouldAccountRecieveMail)
                    {
                        log.CurrentNumberOfSubscribers = account.NumberOfSubscribers;
                        log.Data.Emails = account.Data.Emails;

                        string updateErrors = string.Empty;
                        string updateContentsForCurrentAccount = WorkerWebServices.Helpers.RenderHelper.GetView("~/Views/Workers/AccountsSubscribersUpdate/GlobalUpdateStep.cshtml", account, _Context);
                        log.IsSuccessful = Services.MailServices.SendSubscribersUpdateMail(account, updateContentsForCurrentAccount, out updateErrors, _IsTestEnv);
                        log.Data.Errors = updateErrors;
                    }
                }
                catch (Exception ex)
                {
                    log.Data.Errors = ex.ToString();
                    log.IsSuccessful = false;
                    success = false;
                }
                finally
                {
                    if (shouldAccountRecieveMail || !log.IsSuccessful)
                    {
                        Services.AccountSubscribersUpdateLogServices.Save(log);
                    }
                }
            }
            if (!success)
            {
                try
                {
                    dynamic failData = new JObject();
                    failData.AutomationName = "אוטומציית עדכון מנויים - שגיאה במהלך עדכון מנויים";
                    Services.MailServices.SendMailAfterAutomationWorkerFail(failData);
                }
                catch (Exception ex)
                {
                }
            }
            return success;
        }

    }
}
