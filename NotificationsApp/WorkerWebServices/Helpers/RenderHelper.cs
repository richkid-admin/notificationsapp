﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WorkerWebServices.Helpers
{
    public class RenderHelper
    {
        public static string GetView(string viewName, object Model, HttpContext context)
        {
            //string razorText = System.IO.File.ReadAllText(viewName);
            //string emailBody = Engine.Razor.RunCompile(razorText, "templateKey", typeof(Model), model);

            //Create an instance of empty controller
            //Controllers.RenderController controller = new Controllers.RenderController();
            //return controller.GetView(viewName, Model);

            //Create an instance of Controller Context
            //var ControllerContext = new ControllerContext(System.Web.HttpContext.Current.Request.RequestContext, controller);
            //System.Web.Routing.RequestContext newContext = new System.Web.Routing.RequestContext(); 


            //Create a string writer
            using (var sw = new StringWriter())
            {
                //get the master layout
                //var master = HttpContext.Current.Request.IsAuthenticated ? "_InternalLayout" : "_ExternalLayout";
                var controller = CreateController<Controllers.RenderController>(context);

                var c = controller.ControllerContext;
                //var ControllerContext = new ControllerContext(new HttpContextWrapper(c), controller.ControllerContext.RouteData, controller.ControllerContext.Controller);

                //Get the view result using context controller, partial view and the master layout
                var viewResult = ViewEngines.Engines.FindPartialView(c, viewName);

                //Crete the view context using the controller context, viewResult's view, string writer and ViewData and TempData
                controller.ViewData = new ViewDataDictionary(Model);
                var viewContext = new ViewContext(c, viewResult.View, controller.ViewData, controller.TempData, sw);

                //Render the view in the string writer
                viewResult.View.Render(viewContext, sw);

                //release the view
                viewResult.ViewEngine.ReleaseView(c, viewResult.View);

                //return the view stored in string writer as string
                return sw.GetStringBuilder().ToString();
            }
        }

        //public static string test(string viewName, object Model)
        //{
        //    var c = HttpContext.Current.Request
        //}
        public static T CreateController<T>(HttpContext context, RouteData routeData = null)
            where T : Controller, new()
        {
            // create a disconnected controller instance
            T controller = new T();

            // get context wrapper from HttpContext if available
            HttpContextBase wrapper = null;
            if (context != null)
                wrapper = new HttpContextWrapper(context);
            else
                throw new InvalidOperationException(
                    "Can't create Controller Context if no active HttpContext instance is available.");

            if (routeData == null)
                routeData = new RouteData();

            // add the controller routing if not existing
            if (!routeData.Values.ContainsKey("controller") && !routeData.Values.ContainsKey("Controller"))
                routeData.Values.Add("controller", controller.GetType().Name
                                                            .ToLower()
                                                            .Replace("controller", ""));

            controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);
            return controller;
        }
    }

}