﻿using BLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Common;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace NotificationsApp.Controllers
{
    [Filters.LoginFilters]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Models.LoginModel loginModel = new Models.LoginModel
            {
                Error = string.Empty
            };

            return View("../Login/Login", loginModel);
        }

        public ActionResult Home(int error = 0, string errorMsg = "")
        {
            Models.AccountModel accountModel = new Models.AccountModel
            {
                ListOfAccounts = BLL.Services.AccountServices.LoadAllAccounts(),
                ListOfPackages = BLL.Services.PackageServices.LoadAllPackages(),
                ErrorCode = error,
                ErrorMsg = errorMsg
            };

            return View(accountModel);
        }

        public ActionResult AddNewAccount()
        {
            return View("../Accounts/AddNewAccount");
        }

        [HttpPost]
        public ActionResult AddNewAccount(FormCollection coll)
        {
            Models.AccountModel accountModel = new Models.AccountModel();
            PostalServices.PulseemPostalService pulseemPostalService = new PostalServices.PulseemPostalService();
            string formName = coll["name"];
            string name = !string.IsNullOrWhiteSpace(formName) ? formName.Trim() : "";

            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException();
            }

            string APIKey = pulseemPostalService.AddSubAccount(name, out bool success, "");

            //Created new account in database
            if (!string.IsNullOrWhiteSpace(APIKey))
            {
                if(!success)
                    return Content(APIKey);

                //accountModel.Account = BLL.Services.AccountServices.CreateAccount(coll["name"], APIKey);
                accountModel.Account = Services.AccountServices.CreateAccount(coll["name"], APIKey);
                if (accountModel.Account == null || accountModel.Account.AccountID == 0)
                {
                    return RedirectToAction("../Home/Home", new { error = 1 });
                }
            }
            else
            {
                return RedirectToAction("../Home/Home", new { error = 2 });
            }

            return RedirectToAction("../Home/EditAccount", new { accountID = accountModel.Account.AccountID });
        }




        public ActionResult EditAccount(int accountID)
        {
            Models.AccountModel accountModel = new Models.AccountModel
            {
                Account = BLL.Services.AccountServices.LoadAccountByID(accountID),
                ListOfPackages = BLL.Services.PackageServices.LoadAllPackages(),
                AccountPackageLogsForAccount = BLL.Services.AccountPackageLogServices.LoadAccountPackageLogsByAccountID(accountID)
            };
            var filtersModel = new BLL.SendAutomationFilters();
            filtersModel.SendAutomationStatus = SendAutomationStatus.Active;
            filtersModel.PageNumber = 1;
            filtersModel.AccountsData = new List<string>() { accountID.ToString() };
            accountModel.IsForSingleAccount = true;
            //accountModel.SendAutomations = Services.SendAutomationServices.GetSendAutomationsModelByPageAndFilters_Overloading(1,SendAutomationStatus.All,new List<string>() {accountID.ToString() },out int numOfPages);
            accountModel.SendAutomations = Services.SendAutomationServices.GetSendAutomationsModelByPageAndFilters_Model(filtersModel);
            accountModel.CurrentPage = filtersModel.PageNumber;
            accountModel.NumOfPages = filtersModel.NumberOfPages;
            accountModel.AccountIDToAccount = new Dictionary<int, Account>() { [accountModel.Account.AccountID] = accountModel.Account };

            return View("../Accounts/EditAccount", accountModel);
        }

        //[OutputCache(Duration = 0, NoStore = true)]
        [HttpPost]
        public ActionResult DeleteAccount(int accountID)
        {
            var success = false;

            try
            {
                success = BLL.Services.AccountServices.Delete(accountID);
            }
            catch (Exception ex)
            {
                return Content("Fail Delete: " + ex.Message + " - " + ex.StackTrace + " - " + ex.InnerException);
            } 

            return RedirectToAction("../Home/Home");
        }

        [HttpPost]
        public ActionResult SaveEditedAccount(int accountID)
        {
            int errorCode = 0;
            var form = Request.Form;
            BLL.Account tempAccount;

            #region Get Account        
            try
            {
                tempAccount = BLL.Services.AccountServices.LoadAccountByID(accountID);

                if (tempAccount == null)
                {
                    tempAccount = new Account();
                }
            }
            catch (Exception ex)
            {
                tempAccount = new Account();
            }
            #endregion

            #region Get PackageID From FormCollction
            int packageID = 0;
            if (!string.IsNullOrEmpty(form["packageID"]))
            {
                int.TryParse(form["packageID"], out packageID);
            }
            BLL.Package tempPackage = BLL.Services.PackageServices.LoadPackageByID(packageID);
            #endregion

            #region Get PackageID From FormCollction

            int numberOfSubscribers = 0;
            if (!string.IsNullOrEmpty(form["numberOfSubscribers"]))
            {
                int.TryParse(form["numberOfSubscribers"], out numberOfSubscribers);
            }
            #endregion

            #region Update Table Of Account Package Logs
            // mark on log if the upgrade has been charged by Mor/Chen
            var logsForAccountID = BLL.Services.AccountPackageLogServices.LoadAccountPackageLogsByAccountID(tempAccount.AccountID);
            int accountPackageLogID = 0;
            List<BLL.AccountPackageLog> accountPackageLogsToUpdateChargeToFalse = logsForAccountID;
            List<BLL.AccountPackageLog> accountPackageLogsToUpdateChargeToTrue = new List<AccountPackageLog>();
            List<BLL.AccountPackageLog> accountPackageLogsToSave = new List<AccountPackageLog>();
            var logSaveSuccess = true;
            foreach (var key in form.Keys)
            {
                if (key.ToString().ToLower().Contains("ischarged"))
                {
                    var splited = key.ToString().Split('_');
                    var accountPackageLogIDStr = splited[1];
                    Int32.TryParse(accountPackageLogIDStr, out accountPackageLogID);
                    if (accountPackageLogID != 0)
                    {
                        var accountPackageLog = logsForAccountID.Where(l => l.AccountPackageLogID == accountPackageLogID).FirstOrDefault();
                        accountPackageLogsToUpdateChargeToFalse.Remove(accountPackageLog);
                        if (accountPackageLog != null && accountPackageLog.AccountPackageLogID != 0)
                        {
                            accountPackageLog.IsCharged = form["isCharged_" + accountPackageLogID].ToLower().Equals("true") ? true : false;
                            accountPackageLogsToUpdateChargeToTrue.Add(accountPackageLog);
                        }
                    }
                }
            }

            foreach (var logToUpdate in accountPackageLogsToUpdateChargeToFalse) // for each upgrade log that is not true => mark as false
            {
                logToUpdate.IsCharged = false;

            }

            accountPackageLogsToSave.AddRange(accountPackageLogsToUpdateChargeToFalse);
            accountPackageLogsToSave.AddRange(accountPackageLogsToUpdateChargeToTrue);

            logSaveSuccess = BLL.Services.AccountPackageLogServices.SaveListOfAccountsPackageLogs(accountPackageLogsToSave);
            if (!logSaveSuccess)
            {
                errorCode = 4;
            }
            #endregion

            #region Update Account Fields
            // Update fields that are editable - Name, PackageID and Notification Key
            tempAccount.Name = !string.IsNullOrWhiteSpace(form["name"]) ? form["name"].Trim() : tempAccount.Name;

            if (packageID > 0 && tempPackage != null)
            {
                if (tempAccount.PackageID != packageID)
                {
                    var currentPackage = tempAccount.PackageID;
                    tempAccount.PackageID = packageID;
                    AccountPackageLog accountPackageLog = new AccountPackageLog
                    {
                        AccountID = tempAccount.AccountID,
                        CurrentPackageID = currentPackage,
                        NewPackageID = packageID,
                        TimeOfUpgrade = DateTime.Now
                    };
                    accountPackageLog.AccountPackageLogID = BLL.Services.AccountPackageLogServices.Save(accountPackageLog);
                    if (accountPackageLog.AccountPackageLogID == 0)
                    {
                        errorCode = 4;
                    }
                }
            }
            tempAccount.PackageID = packageID > 0 && tempPackage != null ? packageID : tempAccount.PackageID;



            tempAccount.Data.Credentials.PulseemNotificationKey = !string.IsNullOrWhiteSpace(form["pulseemNotificationKey"]) ? form["pulseemNotificationKey"].Trim() : tempAccount.Data.Credentials.PulseemNotificationKey;
            tempAccount.PaymentApproved = !string.IsNullOrEmpty(form["PaymentApproved"]) && form["PaymentApproved"].ToLower().Equals("true") ? true : false;

            if (numberOfSubscribers != 0)
            {
                tempAccount.NumberOfSubscribers = numberOfSubscribers;
            }

            if (!string.IsNullOrEmpty(form["emails"]))
            {
                try
                {
                    tempAccount.Data.Emails = form["emails"]
                               .Split(',')
                               .Where(e => e.IsValidEmail())
                               .ToList();
                }
                catch (Exception ex)
                {
                    errorCode = 6;
                }
            }
            if (!string.IsNullOrEmpty(form["primaryDomain"]))
            {
                //Primary domain might not be a fully valid url, but is still valid in our terms
                //string primaryDomain = form["primaryDomain"];
                //if (!primaryDomain.IsValidUrl())
                //{
                //    errorCode = 7;
                //}
                //else
                //{
                //    tempAccount.Data.PrimaryDomain = primaryDomain;
                //}
                tempAccount.Data.PrimaryDomain = form["primaryDomain"];

            }
            // AutomationData - implemented because of misunderstanding, not in use
            #region AutomationData

            //if (tempAccount.Data.AutomationTypeToData == null)
            //{
            //    tempAccount.Data.AutomationTypeToData = new Dictionary<AutomationTypes, AccountAutomationData>();
            //}
            //var automationTypeToNewValue = form.AllKeys
            //    .Where(k => k.StartsWith("isActive_AutomationType_"))
            //    .ToDictionary(k => (AutomationTypes)Enum.Parse(typeof(AutomationTypes), k[k.Length-1].ToString()), k => form[k]);
            //foreach (var automationTypeAndNewValue in automationTypeToNewValue)
            //{
            //    if (!tempAccount.Data.AutomationTypeToData.ContainsKey(automationTypeAndNewValue.Key))
            //    {
            //        tempAccount.Data.AutomationTypeToData[automationTypeAndNewValue.Key] = new AccountAutomationData();
            //    }
            //    tempAccount.Data.AutomationTypeToData[automationTypeAndNewValue.Key].IsActive = !string.IsNullOrEmpty(automationTypeAndNewValue.Value) && automationTypeAndNewValue.Value.ToLower().Equals("true");
            //}
            #endregion AutomationData

            #endregion

            tempAccount.AccountID = BLL.Services.AccountServices.Save(tempAccount);

            if (tempAccount.AccountID == 0)
            {
                errorCode = 3;
            }
            Models.AccountModel accountModel = new Models.AccountModel();
            if (errorCode != 0)
            {
                accountModel = new Models.AccountModel
                {
                    Account = BLL.Services.AccountServices.LoadAccountByID(accountID),
                    ListOfPackages = BLL.Services.PackageServices.LoadAllPackages(),
                    AccountPackageLogsForAccount = BLL.Services.AccountPackageLogServices.LoadAccountPackageLogsByAccountID(accountID),
                    SaveMsg = "השמירה נכשלה"
                };
            }
            else
            {
                accountModel = new Models.AccountModel
                {
                    Account = BLL.Services.AccountServices.LoadAccountByID(accountID),
                    ListOfPackages = BLL.Services.PackageServices.LoadAllPackages(),
                    AccountPackageLogsForAccount = BLL.Services.AccountPackageLogServices.LoadAccountPackageLogsByAccountID(accountID),
                    SaveMsg = "השמירה הצליחה"
                };
            }
            return View("../Accounts/EditAccount", accountModel);
        }
        public ActionResult GetAccountsByFilters(BLL.AccountsFilters filters)
        {
            Models.AccountModel accountModel = new Models.AccountModel()
            {
                ListOfPackages = BLL.Services.PackageServices.LoadAllPackages(),
                ErrorCode = 0
            };
            accountModel.ListOfAccounts = Services.AccountServices.LoadAccountsByFilters(filters);
            return View(accountModel);
        }
        [HttpGet]
        public ActionResult KeepAlive_Original()
        {
            int statusCode = 200;
            string resp = string.Empty;
            //string fullUrl = "http://localhost:51713/Home/KeepAlive";
            //string fullUrl = "https://notify.apps.getmood.io/NotificationsAPI/Home/KeepAlive";
            string fullUrl = "https://notify.apps.getmood.io/WorkerServices/Home/KeepAlive";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12/*(SecurityProtocolType)(0xc0 | 0x300 | 0xc00);*/;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(fullUrl);
                using (HttpWebResponse PulseemResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                using (Stream stream = PulseemResponse.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    resp = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                statusCode = 500;
            }
            return new HttpStatusCodeResult(statusCode);
        }
        [HttpGet]
        public ActionResult KeepAlive_BeforeWorker()
        {
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;
            string apiUrl = "https://notify.apps.getmood.io/NotificationsAPI/Home/KeepAlive";
            string workersUrl = "https://notify.apps.getmood.io/WorkerServices/Home/KeepAlive";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12/*(SecurityProtocolType)(0xc0 | 0x300 | 0xc00);*/;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(apiUrl);
            using (HttpWebResponse resp = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                statusCode = resp.StatusCode;
            }
            httpWebRequest = (HttpWebRequest)WebRequest.Create(workersUrl);
            using (HttpWebResponse resp = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                statusCode = resp.StatusCode;
            }
            return new HttpStatusCodeResult(statusCode);
        }
        [HttpGet]
        public ActionResult KeepAlive()
        {
            return new HttpStatusCodeResult(200);
        }
    }
}