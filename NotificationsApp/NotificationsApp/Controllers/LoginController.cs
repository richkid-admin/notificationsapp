﻿using NotificationsApp.Auxiliary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotificationsApp.Controllers
{
    public class LoginController : Controller
    {

        public ActionResult Index(string loginError)
        {
            Models.LoginModel loginModel = new Models.LoginModel();


            loginModel.Error = loginError ?? string.Empty;
            return View("../Login/Login", loginModel);
        }

        [HttpPost]
        public ActionResult Login(FormCollection coll)
        {
            string userName = coll["UserName"].ToString();
            string password = coll["Password"].ToString();
            
            
            BLL.User user = new BLL.User(); // If we'll add more user types (beside admin) than we'll have to load current user.
            try
            {                
                string userIP = string.Empty;
                userIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                userIP = string.IsNullOrEmpty(userIP) ? Request.ServerVariables["REMOTE_ADDR"] : userIP;
                userIP = string.IsNullOrEmpty(userIP) ? Request.UserHostAddress : userIP;

                if (!string.IsNullOrEmpty(userIP))
                {
                    string userIPSplit = userIP.Split(',')[0];

                    if (userName.Equals("Admin") && password.Equals("Admin"))
                    {
                        //if (userIPSplit.Equals("212.179.228.72") || userIPSplit.Equals("::1"))
                        if (userIPSplit.Equals("62.56.224.206") || userIPSplit.Equals("::1"))
                        {
                            user.Type = BLL.UserType.Admin;
                            Navigation.CurrentUser = user;
                            return RedirectToAction("Home", "Home");
                        }
                        else
                        {
                            return RedirectToAction("Index", new { loginError = "אינך מורשה להיכנס למערכת" });
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", new { loginError = "פרטי ההתחברות שגויים" });
                    }
                }
                else
                {
                    return RedirectToAction("Index", new { loginError = "פרטי ההתחברות שגויים" });
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Index", new { loginError = "פרטי ההתחברות שגויים" });
            }
        }

        public ActionResult Logout()
        {
            Navigation.CurrentUser = null;

            Models.LoginModel loginModel = new Models.LoginModel();
            loginModel.Error = string.Empty;
            return View("Login", loginModel);
        }
    }
}