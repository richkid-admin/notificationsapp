﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotificationsApp.Controllers
{
    [Filters.LoginFilters]
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult OperationalReport(string orderBy = null)
        {
            List<Models.OperationalReportModel> operationalReportModel = new List<Models.OperationalReportModel>();

            var allAccounts = BLL.Services.AccountServices.LoadAllAccounts();
            var allMessages = BLL.Services.MessageServices.LoadAllMessages();
            var allPackages = BLL.Services.PackageServices.LoadAllPackages();
            allPackages = allPackages.OrderBy(p => p.MaxSubscribers).ToList();

            foreach (var account in allAccounts)
            {
                Models.OperationalReportModel tempModel = new Models.OperationalReportModel();

                tempModel.SiteName = account.Name;
                tempModel.AccountCreationDate = account.CreationDate;
                tempModel.NumberOfSubscribers = account.NumberOfSubscribers;
                tempModel.CurrentPackage = allPackages.Where(p => p.PackageID == account.PackageID).FirstOrDefault().Name;

                tempModel.NumberOfMessagesInLase30Days = allMessages.Where(m => m.AccountID == account.AccountID && m.CreationDate >= DateTime.Now.AddMonths(-1)).Count();
                tempModel.NumberOfMessagesInLase90Days = allMessages.Where(m => m.AccountID == account.AccountID && m.CreationDate >= DateTime.Now.AddMonths(-3)).Count();

                foreach (var package in allPackages)
                {
                    if (package.MaxSubscribers > account.NumberOfSubscribers)
                    {
                        tempModel.NeededPackage = package.Name;
                        break;
                    }
                }

                operationalReportModel.Add(tempModel);
            }

            if (orderBy != null && !orderBy.Equals("0"))
            {
                Auxiliary.Navigation.SortParam = orderBy;
                if (orderBy.Equals("name"))
                {
                    operationalReportModel = operationalReportModel.OrderBy(m => m.SiteName).ToList();
                    Auxiliary.Navigation.SortValue = "שם האתר";
                }
                else if (orderBy.Equals("numOfSubscribers"))
                {
                    operationalReportModel = operationalReportModel.OrderByDescending(m => m.NumberOfSubscribers).ToList();
                    Auxiliary.Navigation.SortValue = "מספר מנויים";
                }
                else if (orderBy.Equals("package"))
                {
                    operationalReportModel = operationalReportModel.OrderBy(m => m.CurrentPackage).ToList();
                    Auxiliary.Navigation.SortValue = "חבילה";
                }
                else if (orderBy.Equals("numberOfMessagesSentIn30Days"))
                {
                    operationalReportModel = operationalReportModel.OrderByDescending(m => m.NumberOfMessagesInLase30Days).ToList();
                    Auxiliary.Navigation.SortValue = "מספר הודעות שנשלחו ב30 יום האחרונים";
                }
                else if (orderBy.Equals("numberOfMessagesSentIn90Days"))
                {
                    operationalReportModel = operationalReportModel.OrderByDescending(m => m.NumberOfMessagesInLase90Days).ToList();
                    Auxiliary.Navigation.SortValue = "מספר הודעות שנשלחו ב90 יום האחרונים";
                }
            }

            return View("../Reports/OperationalReport", operationalReportModel);
        }
    }
}