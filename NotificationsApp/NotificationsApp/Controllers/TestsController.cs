﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NotificationsApp.Controllers
{
    public class TestsController : Controller
    {
        public bool ChangeLastSendDateToSendAutomation(int sendAutomationID,int hoursToAdd)
        {
            return Services.SendAutomationServices.ChangeLastSendDateToAutomation(sendAutomationID, 7, hoursToAdd);

        }
        public ActionResult TestGet()
        {
            return Content("TEST OK");
        }
        public ActionResult GetTestPost()
        {
            return PartialView("~/Views/Tests/_TestPost.cshtml");
        }
        public ActionResult TestPost(FormCollection fc)
        {
            string result = "OK";
            if (fc == null || fc.Keys.Count == 0)
            {
                result = "fc empty";
            }
            return Content(result);
        }
        public ActionResult Scrollable()
        {
            return View("~/Views/Tests/_Scrollable.cshtml");
        }
    }
}