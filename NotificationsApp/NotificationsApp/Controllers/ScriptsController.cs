﻿using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls; 
using System.Web.Mvc;
using System.Text;
using System.Data;

namespace NotificationsApp.Controllers
{
    public class ScriptsController : Controller
    {
        // GET: Scripts
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public string MigrateAccountsFromMoodCMS()
        {
            try
            {
                string usersExcelPath;
                string resultsExcelPath;
                //if (env.Equals("production"))
                //{
                //string rootPath = @"C:\Inetpub\vhosts\getmood.io\httpdocs\warehouse\"; //unauthroized access
                string rootPath = @"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\tmpFiles\";
                usersExcelPath = rootPath + "MoodNotificationUsers.xlsx";
                resultsExcelPath = rootPath + "MoodNotificationUsersWithAccountKey.xlsx";
                //}
                //else
                //{
                //    usersExcelPath = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsers.xlsx";
                //    resultsExcelPath = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsersWithAccountKey.xlsx";
                //}
                //read excel
                List<string> AccountKeys = new List<string>();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage pck = new ExcelPackage();
                using (System.IO.FileStream s = System.IO.File.OpenRead(usersExcelPath))
                {
                    pck.Load(s);
                    var ws = pck.Workbook.Worksheets["Sheet1"];
                    ws.InsertColumn(5, 1); //new accountKeys col
                    string currUsername;
                    string currNotificationKey;
                    string currAPIKey;
                    BLL.Account currAccount;
                    for (int i = 2; i <= ws.Dimension.Rows; i++)
                    {
                        //foreach mood account - get: name and key

                        currUsername = ws.Cells[i, 2].Value != null ? ws.Cells[i, 2].Value.ToString().Trim() : "";
                        currNotificationKey = ws.Cells[i, 3].Value != null ? ws.Cells[i, 3].Value.ToString().Trim() : "";
                        currAPIKey = ws.Cells[i, 4].Value != null ? ws.Cells[i, 4].Value.ToString().Trim() : "";

                        //create notifications app account, salvage accountKey
                        //currAccount = BLL.Services.AccountServices.CreateAccount(currUsername, currAPIKey);
                        currAccount = Services.AccountServices.CreateAccount(currUsername, currAPIKey);
                        //save accountKey in excel
                        if (currAccount != null)
                        {
                            currAccount.Data.Credentials.PulseemNotificationKey = currNotificationKey;
                            BLL.Services.AccountServices.Save(currAccount);
                            ws.Cells[i, 5].Value = currAccount.AccountKey;
                        }
                    }

                    pck.SaveAs(resultsExcelPath);
                }
            }
            catch (Exception ex)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(ex);
            }
            return string.Empty;
        }
        public string AddNotificationKeyToAccounts()
        {
            string res = string.Empty;
            try
            {
                string usersExcelPath;
                //if (env.Equals("production"))
                //{
                string rootPath = @"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\tmpFiles\";
                usersExcelPath = rootPath + "MoodNotificationUsersWithAccountKey.xlsx";
                //}
                //else
                //{
                //    usersExcelPath = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsersWithAccountKey.xlsx";
                //}

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage pck = new ExcelPackage();
                using (System.IO.FileStream s = System.IO.File.OpenRead(usersExcelPath))
                {
                    pck.Load(s);
                    var ws = pck.Workbook.Worksheets["Sheet1"];
                    string currNotificationKey;
                    string currAccountKey;
                    BLL.Account currAccount;
                    for (int i = 2; i <= ws.Dimension.Rows; i++)
                    {

                        currNotificationKey = ws.Cells[i, 3].Value != null ? ws.Cells[i, 3].Value.ToString().Trim() : "";
                        currAccountKey = ws.Cells[i, 5].Value != null ? ws.Cells[i, 5].Value.ToString().Trim() : "";

                        currAccount = BLL.Services.AccountServices.LoadAccountByAccountKey(currAccountKey);
                        currAccount.Data.Credentials.PulseemNotificationKey = currNotificationKey;
                        BLL.Services.AccountServices.Save(currAccount);
                    }
                }
            }
            catch (Exception ex)
            {
                res = Newtonsoft.Json.JsonConvert.SerializeObject(ex);
            }
            return res;

        }

        public ActionResult SaveAllPulseemAPIKeys()
        {
            try
            {
                List<BLL.Account> accounts = BLL.Services.AccountServices.LoadAllAccounts();
                List<string> apiKeys = new List<string>();
                foreach (var account in accounts)
                {
                    apiKeys.Add("AccountID: " + account.AccountID + ", API Key: " + account.Data.Credentials.PulseemAPIKey);
                }

                //string docPath =
                //  Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                // Write the string array to a new file named "WriteLines.txt".
                using (StreamWriter outputFile = new StreamWriter(@"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\warehouse\OldAPIKeys.txt"))
                {
                    foreach (string line in apiKeys)
                        outputFile.WriteLine(line);
                }
            }
            catch (Exception ex)
            {
                return Content("Fail - " + ex.Message);
            }

            return Content("success");
        }

        public string ReadNumOfSubscribersFromFile()
        {
            string status = "";
            try
            {
                string fPath = @"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\NotificationsAPI\App_Data\AccountIDToSubsCount.json";
                string sAccountIDToAccountKey = System.IO.File.ReadAllText(fPath);
                var accountIDToAccountKey = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, int>>(sAccountIDToAccountKey);
                if (accountIDToAccountKey != null && accountIDToAccountKey.Count > 0)
                {
                    BLL.Services.AccountServices.UpdateStatistics(accountIDToAccountKey);
                }
                status = "Success";

            }
            catch (Exception ex)
            {
                status = ex.ToString();
            }
            return status;
        }

        public ActionResult UpdateSendAutomations_HasAlreadySentMessages()
        {
            var allSendAutomations = Services.SendAutomationServices.LoadAllSendAutomations(false);
            allSendAutomations.AddRange(Services.SendAutomationServices.LoadAllSendAutomations(true));
            var automationsWithMessages = Services.SendAutomationServices.LoadSendAutomationsWithMessages();
            var automationsWithMessagesIDs = new HashSet<int>(automationsWithMessages.Select(sa => sa.SendAutomationID));
            if (allSendAutomations.First().AccountID == 0)
            {
                return Content(allSendAutomations.First().TemplateMessage.MessageText);
            }
            if (automationsWithMessages.Count == 0)
            {
                return Content("NOT OK");
            }
            foreach (var automationToUpdate in allSendAutomations)
            {
                if (automationToUpdate.AutomationData == null)
                {
                    automationToUpdate.AutomationData = new BLL.AutomationData();
                }
                automationToUpdate.AutomationData.HasSentMessages = automationsWithMessagesIDs.Contains(automationToUpdate.SendAutomationID);
                Services.SendAutomationServices.Save(automationToUpdate);
            }
            return Content("OK");
        }

        public ActionResult UpdateAPIKeysForAccounts_HardCoded()
        {
            string oldToNewApiKeyToConvert = "[{\"Old\":\"jf2jzcSBQBHtwfXsGfrZxw==\",\"New\":\"^dTpxgZF8&qlP#[X(c4UY8!7KmJ^O%w19&s3*}amlNc^k13pj\"},{\"Old\":\"/K/eePp3opGVmHs5z+jPkA==\",\"New\":\"*JF#Ea1Pf)gYlEIPBv%Yy-c#JJ*9q#cU9c{%UlEla6c15Sb6-\"},{\"Old\":\"OZ1UETo2o9mHQCyjQEUo9w==\",\"New\":\"Yl@RyGNRiYsMPVJR(OEL6GQP!6keJeePd2F4GFQPrhBIm3*QW\"},{\"Old\":\"IdwddPzfDAtD3XCs1i1ZeA==\",\"New\":\"eCO8N4HE&3azUrRo5AveJo]sqjN)[sDCz&[Jd]N-B{1Ck{k{S\"},{\"Old\":\"bxgk+czQgOjZqA8KlBPJWw==\",\"New\":\"KgKAQE{5#68!z#u(aPhj{@8MqpDx{kha1lv@JlDTl3k3D!Z$C\"},{\"Old\":\"0r4zFNp6LtI1NnBaasLdSg==\",\"New\":\"b%Z[MqrLZ0(fVpVX7XdAnpy}-K15c@n7OR7SDMYl#ITXNs(w4\"},{\"Old\":\"NFkq8IkhNOknKxMwOWy4XA==\",\"New\":\"[UG(mQOZhOKiC@akE8ehGrHB8zB4EG7aSu0oJWDscV89VfJ!i\"},{\"Old\":\"Lalo4Pf1V+qGzZiLfeZp9Q==\",\"New\":\"Kv[Teoy9uMv0$sCfRtQxl@wm5nl&PhEWVbOY(K(0{Zrq%[caF\"},{\"Old\":\"h/73g0eMHIRsB36S+Xl/DQ==\",\"New\":\"32#v7fm8IJamTY7dJG)[8U0ZPG0f&d7WtQlE@5a4bSTR^ckEp\"},{\"Old\":\"MuEiN+juKuDug88AIbiqCA==\",\"New\":\"h32ooQ!Rj%B^ix*#6ZNWClgQW&m3hAqv[W1o08T@Bpt1i5@Kh\"},{\"Old\":\"bdS3BiJjF2s7y96AogIb+A==\",\"New\":\"DZd[*fSBjv@WvnyE%tP76T!Z]wV8jPWo[CBGDkP*VlhjJEqO1\"},{\"Old\":\"HAv6lkj6HM6WVib7iDQiJw==\",\"New\":\"(xSP#uetJWcw5cf0z6YX]m3Z4atMJ4k0ZabzrlCw9xpEd@{HO\"},{\"Old\":\"oE3P8rfgOaPNwh4oqCOPzA==\",\"New\":\"18m4teXgKT1L&qa(--6QySxh#QIuZ$SHmvY8$vBS%NKmV@tXv\"},{\"Old\":\"IZs62Jl80u/CX9BVcin4tA==\",\"New\":\"PuuCWwAd(#cOSJZBg#fMfhD62e0u0kvU-$dvC[)EMlHc^Ksa0\"},{\"Old\":\"pnW2MmkCTznSeCMHlpVmXQ==\",\"New\":\"OQyQCg%0q6*CuVf@s4t}AWFTf@jf4csc&4WjFCQ*c2v}($b5}\"},{\"Old\":\"rpfQSTpquFQLQnUWNGeFeg==\",\"New\":\"ABJbc6-PmRL]EZiX3ZLfMNe}8jY3SHa2-g%G-k@eZZ#H)Bsdu\"},{\"Old\":\"I+rVCzzA89H2tH7ZcZXXrQ==\",\"New\":\"NnEhhQI)mkWHr(ywy9^$Lme%ceome3rs560t0(Xt[0XtPdj0m\"},{\"Old\":\"XYDiQMt+2Zg6KqFCMSU/Vw==\",\"New\":\"Cf9tbqKMB45CRxW[uHJc1%u!GxtL^1]iikze[XYe%Y9pGSWzq\"},{\"Old\":\"p0plsyvbXfMsKGHumBJuyg==\",\"New\":\"Mrb2dWL1BdAbb2{%7u4[AeDRR(dOj5}tMH0((qU5[2j!c-4-U\"},{\"Old\":\"qACvBjrTCR8QTp+HTuEn+A==\",\"New\":\"KezYAS)ukB&WCgwvaxEYAhxVuden4)Qb1(T2ajqX-s@eoOI)S\"},{\"Old\":\"qxxXHAb4zYVj/y1FY251Bw==\",\"New\":\"42U0ouMe5FK}RaNG^gbyr2xbFTI6AhG8%oPG}%hAf08p&y$wg\"},{\"Old\":\"7jlVtyuypDNPq7ttdss+/Q==\",\"New\":\"aqk$dhv@!(!IzCqcH#qq*diGEnZkBR#FmSKhLwUULKa14lCj&\"},{\"Old\":\"kAtfFXTD2TvTJBSGXymiKQ==\",\"New\":\"Ei0zwMpMZGHCqKnEE5Drr(v%d8-0uDf3AQ&oqbeP5GJqY))^K\"},{\"Old\":\"VtdNbCBJFjMMI6R1HBnj0w==\",\"New\":\"H^06otyW9}Xkdzjc]7FXBt3]irnAL-cm^aoEi%V9qCSdAi#j(\"},{\"Old\":\"TkOg8CxjSs//sULR2wvJNg==\",\"New\":\"F)J01bgSO8S0soBL6f}Kf&Bq!p[5t3WBN0Pj2G%47Wyr0&bFs\"},{\"Old\":\"sahFg4bpLHL59rlnuB6LNw==\",\"New\":\"C9Z!iZrQAE5h6dhojuwy)naiuCtS23]*FtCfe@MlC0$-t@]PA\"},{\"Old\":\"AcP5yA0Ay9rkflIKnv3KXg==\",\"New\":\"hp2B}$7Dtno&Iw12K4}LMpRC[USq%Fq0B&^LUQ6Obh]Y%f&h%\"},{\"Old\":\"B77K+oQtJiJf2k6g2qe0lw==\",\"New\":\"M#EAy)v[*4!@uA3(M$$C8i734JKiW4XMZm53-5Fc)J&{p&nKI\"},{\"Old\":\"fJRu+wl3mdqa71POWxu7ww==\",\"New\":\"YeW@e6^%4Iwur@6O81VD*HYPyg!pC-^m6qCB[t3}X0ZS2fs4M\"},{\"Old\":\"zAqNWDdLxeu79XOf9PtubQ==\",\"New\":\"H#4F6l#gl]]bCpYFhH$wu3lz69D!OAxzX^{Gnk}$1!%#{@rgX\"},{\"Old\":\"3rin8jZEVdRCN7lx708zHQ==\",\"New\":\"IpfPCSP)c0zL*@}SDJy)0m*Z0IWK{evqdD]F-53ud8$wUs50v\"},{\"Old\":\"CTOJ3cmzdeFT8bBWh52syg==\",\"New\":\"lEQgjU]CIE9@i&{]@-Az]z#6lOP[L#OIu$^NNDIZO9!zXwjCf\"},{\"Old\":\"GIAXZQ8CBszvWWRwD86IMw==\",\"New\":\"fdysmc$cW-y)Fmqe8svhTpmJ*asYi3si05[*VN1Rp8q0EK(mv\"},{\"Old\":\"51YTbZ89875MCM75pyKxKA==\",\"New\":\"K4K%cE@6Bz-O]#}CQ2jTmctf3LHg3-ptVRQv)XnMA#V4f^8@I\"},{\"Old\":\"g8XHaN1qNnD8gkQWuOs9Sw==\",\"New\":\"^zPV*@xf*B*[Pla@z3S2#i3&9r#jyLr@-RmIX2d4$DLnEGBF0\"},{\"Old\":\"e3WFhYh4U0m7efiaBugqkQ==\",\"New\":\"0R*iT4YheJPgayk*7@61r#vV&ChzBa0Ov%[u%YhI(d4EITCq&\"},{\"Old\":\"b5rN69VqG0GZrgPeT9Evmg==\",\"New\":\"O79%[5!UVewBLxpF$iMUyyH#cWWdw$@r)e@*6&hI-#FB^VE4d\"},{\"Old\":\"lW328y9o1vuB7m+k+0lKyw==\",\"New\":\"AkVq!K8E0O0)2aHOTNb[^1cw21VCNJqw}J@0D{aJH3kKU2pP5\"},{\"Old\":\"1+J5ah9K3g7bRWqDYAteeg==\",\"New\":\"Dwmc1W3]0S8tkPoFjXty5l8%i7UCAtfUz0Sjd#Uq^qxEVx53d\"},{\"Old\":\"hGTgsFUpTSl5oEmPAVS1Rg==\",\"New\":\"76DxFOri*[gasqLLo(k#{L76-a{ypiIk4ydqR6[U}0vCf)56{\"},{\"Old\":\"S5moI+9q/Qans9T0SPZeNA==\",\"New\":\"hE%yKGKftBO[h%^8U@2jtScSu6FnE]X!-R&27esmNOP}$WifW\"},{\"Old\":\"fg03FrDOc8N9wCIqgb8YTQ==\",\"New\":\"4R6cZErOc03rLozDW3Boi3$H}zSEh90wi1sR$MY&kP6AL]BRZ\"},{\"Old\":\"PYMePdX+heWR3i6bcsWcHA==\",\"New\":\"LqpPc3p]194b6pS)pm0L7Ge]yeKc(]kcdmEJnlv([c(knEOoe\"},{\"Old\":\"LCXHKD8YQFV3ZmSrEvMvrA==\",\"New\":\"6kmb^j6V$(qpvBNdB$-O)ZVJ6[!m]ZzbrKRLiFY$D3cziNJBQ\"},{\"Old\":\"BGoqdJ59lXMoWKG0m82Osw==\",\"New\":\"!nYI9)L[K$&iBI4qXLZu2ovFi}WO#mKE60R%&y}z$Tj8]y(vW\"},{\"Old\":\"+RF3X4spidaGmVPk+m10ig==\",\"New\":\"Ss1aKahaS})3DXLziKUqHQaK%SfO!053jlMG9CFk2a}V8kIl7\"},{\"Old\":\"UcV7IGtZquHdtIGVC+sDjA==\",\"New\":\"mC{jY0uP02rCe*jkT@zEaagIbJwh{gfa*ZpSEbhT30XG5@pUh\"},{\"Old\":\"vsJ+jZmvCE/MNR+CPleJsg==\",\"New\":\"}mM^XrAVyCRggCaYg0lX#!c!K@ZH-I*!0&xBcv9qOPAxwBw0E\"},{\"Old\":\"dRuMY4jtX+I9LrgiZ914wg==\",\"New\":\"%e&nU%uJCON&oowA-57]oAvHOUq!jCDP1Pi[ROZgbWf5nvWZv\"},{\"Old\":\"OykU7bY8RmuW/rDg7MERTA==\",\"New\":\"P@B5xTVBPK&]pZIRqeud*)]{5ez9KN*cg27$Hvxbk8I#RVth6\"},{\"Old\":\"DEzg+XtXnHbK85NavuHstA==\",\"New\":\"QHo}sFu1^N#Vx#)Dbq8dOb6UegvI%ZC^@({MUxdqewXUPKzRk\"},{\"Old\":\"1AfVJvMfEyHfn08sH4AKVw==\",\"New\":\"YhiB&)}yNBjSmJ&@JncliYT7pb0RqjvYAvc7Sgm*SSUVW]%ge\"},{\"Old\":\"o3cOvqY36TkreZKHPwB93A==\",\"New\":\"DUMGyl9C#GHz#dwbCpTVPDTG&g#RNN@E5F)95$PL$C-Jv7*EP\"},{\"Old\":\"3L49pvgzCSfG0JHSR49dCg==\",\"New\":\"5c%prDomjQ@KxBhkeTY%A*y0gQL#pKb)yBDo%!D@)kget@mmC\"},{\"Old\":\"Gp690YjctSEObAKscj/UlA==\",\"New\":\"TKLip4Dg%WXz#(X**4R-kQnqqF#2iUZB*#*YA8sacP#-ISM38\"},{\"Old\":\"yImKLSGYmjka2dpYiCUerQ==\",\"New\":\"a%tblBn8b$2}-iS4YcN[T4g!Rc#Z%iSOYWMo]SCV})IO%HU]E\"},{\"Old\":\"i56BnK+yJdLi4D9XCCo40g==\",\"New\":\"qyA^SALb7t)OsW$0ID2FcftY-ne!nCAi)pLW^IjkwSHxi3-{f\"},{\"Old\":\"4MDJd/dxszq9mfKMat5DFA==\",\"New\":\"d9t6gQ0Ra$L1tCQ&{9c3bhvNrbhqHd#p0*vd&rWZ]W#iMN1C{\"},{\"Old\":\"UXK4rgAEs6xECr4k6hjFGA==\",\"New\":\"0azq*Y$33s45[PxR60C8X-vv!3[oSwHv0[CK-dyzKnDunkc&8\"},{\"Old\":\"DLy5gzVS3pyxIqrfRLN+YA==\",\"New\":\"r8oDm7EEEO)d#gHhz&sK5GROaZO]pDa{^hVf7V[BhM)5N}z8E\"},{\"Old\":\"MW5qkBpbvS+8r1HZ8gPj2Q==\",\"New\":\"mAfrp58fn)$)0)yA9NJQcSq(C#UEKJU)9KlO5^^dqi{wt92sd\"},{\"Old\":\"D0xSiH99j1wbV+lR1u9fEA==\",\"New\":\"drzLL#X#QM(yUvs]PBaU(dqt^1!LYBtYhly5SOe}7D3tiYAL4\"},{\"Old\":\"8ODUqkcmS4sC4YCcX/J0JQ==\",\"New\":\"&EGp}pb51oeqQ8rj!2YDjcPPqdVIVb%GF2Bq[ggAgsdT&1Ais\"},{\"Old\":\"0EvDWa3+i/GPOJX0Ga5AAQ==\",\"New\":\"#AHzgcVHT7xQ*kLam*tq[D$u{nDOBNH!fDo7m2ut*&whpt6L)\"},{\"Old\":\"hDUQFC6QPy/pYtT3cUN39A==\",\"New\":\"*7tOdJIl[0-lie8uqU@^6qVew-w#R[kZ6KO4m7W0me{Z2N^^w\"},{\"Old\":\"H4tBeUQFVS0XeC1xdzYGYA==\",\"New\":\"@GbiViwvTk0T%%PQnlB2ucXHGO)alr0kPXGxVU}yRZwlquc3V\"},{\"Old\":\"++fmmms06c/dyUMW3ycefQ==\",\"New\":\"#h!&OC^N]C0CN@7l3Wh{op@YeZ4CIZ{TN1ekxLF*RnQ^%D2sE\"},{\"Old\":\"ryyWdedn6UYs6qj/54Kx2Q==\",\"New\":\"u6i3vKIRh]sAddW-G00Nk#3p)TU*An1{v-G3*LEj&8Tg*StTZ\"},{\"Old\":\"RjqP09u05uNpT1jJRhcKiA==\",\"New\":\"TIuwEGQvDxlMcWJ@@p@)AtgR5nho$d4j7}aBR$7Ww5U^Df4jp\"},{\"Old\":\"uXu/pF8iT5h83eBbqN+tNQ==\",\"New\":\"FISe#o3hwSfNu@aGU-v#z&k(lqk2#0C5^Ezl[f(T*3Hs{QZ{I\"},{\"Old\":\"DbuqUm39u3DhQJvA8tHPdw==\",\"New\":\"H7VeXUNn}v)rBn1aca0E!(F4{d$f8BQr]iRmOhhJT1Ft&6Xt-\"},{\"Old\":\"zEX+Mxvk3s5KtKlB6RCsiA==\",\"New\":\"oOdABpv8)S*yV6ZE6}SrDZ1Uq4Z})aNpg08xp8)^sYFr#^wH-\"},{\"Old\":\"sApNci2c05YWiDYXkKyxPg==\",\"New\":\"4#6d9LCGA}ioCrf2S9L9N&Txw{C-*tz{u6CPtS@cIIK*RsDpo\"},{\"Old\":\"FZ18LS7/bkGWtyDk+1RhdA==\",\"New\":\"0z4sG]HARbGFusN5wQJrj*5%B8CK#gE!VfD{N*^v60{srlgfs\"},{\"Old\":\"xOua6ce+zDriwX36GhRHcw==\",\"New\":\"{rJI9Nn5YC#gam}vsQlL!jQggNih5oBU0WtxpvRjQs]bYlKHm\"},{\"Old\":\"uX69LIoPMmu5xS/BYeqP4A==\",\"New\":\"{WB7gaX5&%AZHrhmaV[0o[Nj[0T]MpIZpKNu-ly$9jh(ncbIy\"},{\"Old\":\"eYg7YTTcgY+KK4xVb9bmRQ==\",\"New\":\"0{Y8QG1w)$^l9jtRzlXl9tB^RN9w]WE@g(}Fwn]M-U!O@f@xH\"},{\"Old\":\"x+hGV4f5XdqZdja2zhB+Sw==\",\"New\":\"iZ]k]zL(8gu7Mhj4nW{&4Hfi]00ykaOko8{}TVNn2Dr4$vDet\"},{\"Old\":\"wjPFMJMHQd3OpDm7wq5SGQ==\",\"New\":\"SUFceQDGKrTivVwaSh!HQipkJboEso]4H8yaxCfQFb2s5roAv\"},{\"Old\":\"XnCE7Nc0I/QF5/R3lfXpww==\",\"New\":\"ipTo8iKvv}d]-GqKFF^S99Rc{{WrL3ms(B-IrXBZ-GjIY&[1m\"},{\"Old\":\"9TO5+JnFP+k7/rqhGc2ezg==\",\"New\":\"*]^2uTS3q}8Rn6BNdhzHH&tcjx0m&dxVN(-Tz4za)S3ycNcOv\"},{\"Old\":\"M7Zbshl9uaXioadHrAIrLw==\",\"New\":\"Es1xNwJ(nVx[5(74[Yw)![7qId5{kMdQ4#@WNeS7lM*zxw6!%\"},{\"Old\":\"xSfBKbg9aTC9qTndymM+Cw==\",\"New\":\"@AMhbfF(BW0GBWb#q!!#jvl]Iu1-b%tUAX@TdC#Bi*fuyYzj5\"},{\"Old\":\"Q8a2l+xZzo5tPPHUZvuWcA==\",\"New\":\"}P%NMg}w5T(t82(akKD@r0E9i}}}c&!VXT(nGUQdwt&09rZ5p\"},{\"Old\":\"BkmxLJ/Wu9kLUIrVNd1OAg==\",\"New\":\"Va&iq1Hwb%-cq#N0VPoY&%s-8ti[Yv4N60qY5)SXu#VlT^PqL\"},{\"Old\":\"IeUWnu+2qzbIcyo5mMGsew==\",\"New\":\"Ppbgs6J0zTFT&0i&A]Oz4j}Nwaut[t!1[tPECp8on1mRq7YpS\"},{\"Old\":\"K0n8odClsqcXZWiHLW1k9Q==\",\"New\":\"HBA]Qz6@dndZh2yI8%6hDGuDd3*]hrm^oza2R&X5GP&lNowjU\"},{\"Old\":\"HbYV/k0AlmZF+sE1kYBBcQ==\",\"New\":\"mk1Xi6*5CpQ3GkjqzJjyyc}3Ts7lHeWaVXUO{j}-^F4NUHUx]\"},{\"Old\":\"6Aul3jxkYg3kgKIcTZnxhg==\",\"New\":\"zmf^ty!CyW8Pg3}IpCvvuEqQVXwvAZ<Omyc*Z)@]vYlyp06\"},{\"Old\":\"/3MtLmUfY5aZwAgSJmCApA==\",\"New\":\"0}6&Kc0UTpZDU7WKhf$P6nNQk&BXiEtM7Xm$uk{zfvWsMC$[H\"},{\"Old\":\"w5pjubItbHFwcLyRNAYgCg==\",\"New\":\"&Z87!6S%rFMLC)7wXBDs5nTuij3nDuq41]BadIf^T!U@uSD7{\"},{\"Old\":\"8AzZjn2vVgZxHkHqTE4SLQ==\",\"New\":\"eqE#2Y#hu$oyr5kw6zmU{4vdgOfMBEVV0Sa(drGZHK6vnW06&\"},{\"Old\":\"IOEFkF1+6e08Z6FNPN3YjQ==\",\"New\":\"As)w{e2Y)*0MmUCRVKQ#^q7k]0pk%XI{pW{OOmBT6dv)dKRy3\"},{\"Old\":\"TZm4Z1MT+9+AfJX4gAoQIg==\",\"New\":\"UAK-HRj!#@2Igx(zu7WZSAsW30-4@Yqa6xg8]63hxbERx0aIC\"},{\"Old\":\"KXbyGQWniRGMmXCrObiS0g==\",\"New\":\"K73F%u}}lUSAz@4hEYMg-t5]vrX&*(VwtQrQZh%efB}VPJI$G\"},{\"Old\":\"DLmU7Ut5nJFAOeZj+R+dAA==\",\"New\":\"Gfoo{bmgjqtLTA^i]k#tP[ODKPOC)7FPrW0a]lMjJDvj45ZSP\"},{\"Old\":\"rBBf/C4pVWN/Jg+TqzJENw==\",\"New\":\"0JXKrdD@X0yQ(&8yIP])V1R!KEtCE4[)2QHB2jhxhN6KFt!JI\"},{\"Old\":\"nXV8J1ulW7pDrS6tg816ug==\",\"New\":\"[Md4S5P)@2F!r{^j(8i-WfjA4^V-c-c[[*2rrV[*q#j8S5&7k\"},{\"Old\":\"8jsrByCSaHj9bMGm9ulu6w==\",\"New\":\"W-LkXVL0u)UI$3x!B9jUaL!z*JqXpS2n))DHhStWhmGUnxhAk\"},{\"Old\":\"W0m5AdId7bzGewf3CEhiig==\",\"New\":\"SL}X3#DhmOUC%O]l4E4Vb#JbkWFyWe1Y]pSJ6Q^U-b{LL@-hg\"},{\"Old\":\"aw7COZr5R/1xrAWTv8PUqw==\",\"New\":\"XNwF2][MgH2g[IoKrwsM02QS6rw%g&{)%70[arsDc0y^Y76!K\"},{\"Old\":\"cOhRnDUUmBfocgG96+A9fw==\",\"New\":\"Lq8)]HB3kLt@9qVw*Muz7&QSJZ&{8$OkAJAM6}9dTomfEV(39\"},{\"Old\":\"U3xFnHFgEYsYXqIIaEOsYw==\",\"New\":\"J[T5[)at8juT{vdKRU$GBf$}#b7kNdF-cX!}YJBB06}-ZES^Q\"},{\"Old\":\"5uQFjTEzLZqfzx8hv13tiA==\",\"New\":\"$o4)HeKvSJXKDoiWArEe53L#j0t@h$!#10Di#G&xO7L5X3xtW\"},{\"Old\":\"TyclUXZ3QtbEO/TIyid8QQ==\",\"New\":\"0Xq@Mt9g]m)HC7koC49sNE3oxudb*2Af2I[04e}8xe]F{8902\"},{\"Old\":\"PzA/gYDS0EMB7dk7XldZvA==\",\"New\":\"swLepa#$KN6wzj#ep1OWt!j(e^Utbouf8XpFh9WT#fqHH8^o[\"},{\"Old\":\"bkmZM06HV4VsMkNDP/5ZXA==\",\"New\":\"iXm23y-V}zU42#0VU!YU@06AQdf[QQk]{yQooSc9Ck1p@DuRv\"},{\"Old\":\"NnyAclAbou7CJYmKgBxbTA==\",\"New\":\"U${}o0njl1r1#[TpW@&FvcocoaVs-GRP7Xh)eEYNaUwbvd[e1\"},{\"Old\":\"VKAyighqt+qwsQkaQEmJCA==\",\"New\":\"RakmMx8YndQz#8WjkSlt)}43a)-Df05GvC7Fuvzy!349)QXaw\"},{\"Old\":\"yPIK135rZjHgfLcQ8oF/9g==\",\"New\":\"{8cwA(UHGwk^]KEM)tD#(@%8Du@Wbeu9XRv&i@ErLYiLwKz{v\"},{\"Old\":\"EYISu4F6dPjg3O3pZ5dMwA==\",\"New\":\"7e2}tmD{(555dn]{MF#U}fYTjJmitZ3vE9z8&!pYEw2#xrBSI\"},{\"Old\":\"2QJxVm4HDaDHBzCwkAfg9Q==\",\"New\":\"bEEj-E4x}J@x$8yk]bF2-*rS^n8{3UA*dcL*0rIB8zL42NJh@\"},{\"Old\":\"f3AZvWrDW5ZPqVBPhhCneg==\",\"New\":\"N)%!9QGN]064azNiW$*8^RCY1MdtYoQtsmrSOf^MTz&JX40MC\"},{\"Old\":\"viqnYcL49eyDMClb6nJbEQ==\",\"New\":\"ZZuoKRh#N!9s1gBBmiJH)rAC[MH%X1@uk8!w(]$ioIoImX2(R\"},{\"Old\":\"bV6Yf6UBnD437/B+zx+VQw==\",\"New\":\"pR0[E*#{&tl-*jZ[P2OxoLVOwg8VEILmM9!eZzO$DRc$Sq6NC\"},{\"Old\":\"F5DYPVX80I67oHdv1xHR4A==\",\"New\":\"n9RK3Bd5tque!3OE0Q2e0UTHZCYANzlY03uLU0xB]8xeH8%XH\"},{\"Old\":\"MX6wBvWw0jsHs4U6eftRbA==\",\"New\":\"-5$wqJp#ILo#03F!eJNS(Dk650gGp}8}esHfwb3W%DSrL}FpR\"},{\"Old\":\"LIDA9xrwyv5fSHvsdztcHQ==\",\"New\":\"@JUxcT3G*tsioc}#n4h!pyK-NU]RaR4AODF$ILu5yJ5&^ToQ4\"},{\"Old\":\"AjFLVg2TW5CqcyDqMpo1SA==\",\"New\":\"7hkzRmTcl]6L)O0iLc0$lOwvcT$V]0*j-00TFOIEF5lTkGyXK\"},{\"Old\":\"AUYt5C/U7/j/HHmRDTYKWQ==\",\"New\":\"^*iODpFWyJAFr1k)D[7[6VYBdqsc@)sAT2KxfzDc7^C9aKHX]\"},{\"Old\":\"AktuVNtUdGvkHSmB2QxeFQ==\",\"New\":\"6v%f1#Q)v3BE$U6*)ydWr8JCSDkCi$%vrqoa!pzF8wmkZeH35\"},{\"Old\":\"R9KMBwVtgBkPo9sSWPqq2A==\",\"New\":\"CEN$BYB^&U7{wiqm3Zn{pHw!!l}C%GrxhOQ!Yv13I{7(A6s[0\"},{\"Old\":\"oY0kVRh/xACRfiu+p5ixgQ==\",\"New\":\"W$-Pxc2]MJyCqSSxUGXP7LnBR{voRgi8zOnM7yP29a$gMSMs%\"},{\"Old\":\"0PQxAxDDGNKVH9hGp6/YYw==\",\"New\":\"E&@sR3Yd]4*NXAj*[PS{jNvn57NPvUzw2Dd)iXEjMN[*${*sD\"},{\"Old\":\"tqGZp9BvzVmNi1xHVAW6Lg==\",\"New\":\"kl&5d(p$Z(u*frR(t&e[c%OT9ZfqL@5O%SXH9m08u5au&JpSM\"},{\"Old\":\"qvmiD1Dta+QoUmyugUeJJw==\",\"New\":\"sfOo)bHp(I6}9Z@9(6sa-!O]MFT9ylF#5@h$E3F#)j8sI}s-F\"},{\"Old\":\"LuKjDckn5yQExUNb0rzR5Q==\",\"New\":\"QjI7mb0XEYuI4v(X]$#Z){^LjTf2%yVIN[F(kFJJdah0555)}\"},{\"Old\":\"d9/4ibwHD6trsfnmF6zepA==\",\"New\":\"$8AfQ^2zi0FI2#uCYQlCZZ$lE!VCYR24JklbRl9EQzasp(KH2\"},{\"Old\":\"y7wnGBQroGBi+y4IiacfyQ==\",\"New\":\"DQbGXY^u3tRH#}iA0-S%i#{ty@e01pQsFJ&R27*5-kJMf6P9-\"},{\"Old\":\"63n2pRvY9eS4+9QFhSBAVw==\",\"New\":\"Y3cab@6Zl}GE7Z2F^]Rta9KGxg3}APS5e5q!dD2Xl^(1K6Qri\"},{\"Old\":\"bKgoGtQCndA/iCl2nJnAlA==\",\"New\":\"{!90*%px}7(GtVOFAyd0Fv9U2-n]A0RT0ke$C^x-AOT0Xu@X#\"},{\"Old\":\"DoXxti3OvSJHaqoHSB7wdQ==\",\"New\":\"j2}K*PE2^9Q0cgU(iz1b00*}e0atJAgycX$Z1THRB$15ukdsi\"},{\"Old\":\"Qu5RM9y+WXQtnJTK0nMczA==\",\"New\":\"P9StAsBj[j#cP#tFoISfz^p!&hMYkHwo5Rf20(uC@g1v4QdI^\"},{\"Old\":\"VC4zwKiIpZ+I8FhVp+OyVg==\",\"New\":\"c00rRaorL2wu8Hsp-9M*K7MNAL{}}DWO]-8HgLEpSsMa*2!5*\"},{\"Old\":\"t0CxVTJYE8f/FGTwc2Jkzw==\",\"New\":\"O4xGn9@qbbx!KuKJq]ZWXz1(dX2wo]!F]uo-^JE%%2E5nYnTc\"},{\"Old\":\"T+KDpFQTaQl3gVndirDcLA==\",\"New\":\"wdYsC3vb0m0S1lFb#F0v^T^]eW$6@%qGtanjI]L98)Q9PJy0Y\"},{\"Old\":\"W4i8X38doRg0GC96pguL5w==\",\"New\":\"rf0&^&JH@aPq]eFwaL@m2ihAv[jlZijGG@}Xn9BX^{Gezi1*V\"},{\"Old\":\"Fxgsh179kmZOnl5Dfbassg==\",\"New\":\"HNOXL}1!o)eqq{cA*9JdrOibSR09J({AsKCZ^bs}8#bz#&Tc2\"},{\"Old\":\"CDrUjhEfXiy7gQK1/CChuw==\",\"New\":\"LX52!VWn2bc*mt489iJS099}v&}I5hN1^CmH@QXqma$T6VC%7\"},{\"Old\":\"XC+O/F+06BjkBVfsu5LA0A==\",\"New\":\"Dx#KoV5uYwOwxdQ2OlbwC#QD!XfF#VfyLvwGU$G]&u#V3Jx(!\"},{\"Old\":\"OblhF6q7DSOKe+a/BVjCGg==\",\"New\":\"m[fT}l00*oBdZTOWT508O#q5UPEDo!gd@3zIgZ)ovWdz8%HTn\"},{\"Old\":\"y2H/QtOEKsS/D+Z8m73Hcw==\",\"New\":\"]4[p-Q#DvgOW[@NgBO]voGdH58bjsSWTNIVGv^rYLpJ7RA9*r\"},{\"Old\":\"+P2xUvIQLALzAmFvJtXz3A==\",\"New\":\"0lRM2aA}%y3)A^os%KIYH#fkC3@Z30M}W#0tR-A$amPueOjCa\"},{\"Old\":\"WxZPDYko3dFisQAwq1m8Kw==\",\"New\":\"bVi$TeD*pK{^W-${]gJNr4tv[KcI%*zvNTq*%RA$G]UJzS8m0\"},{\"Old\":\"wAo81s+k3bYaI1FU7POraA==\",\"New\":\"0t8h*}#UZrVYQYR2N(orM8mfaaMydr6CrHSZ0j)1SB4mI9Q!B\"},{\"Old\":\"S91MHoGqJLrHSWBag9ozNA==\",\"New\":\"{^f%Y8TseqLWp%M)#YnC36y{U9oB%M1c9YOt^hdXQoLrO*-[-\"},{\"Old\":\"MyQqdKSUWCpqIO10KyO4bA==\",\"New\":\"8i[4Wq#BTntZFR{ooaM(D}#YBO1p%Y&wZz9O{b@Z)CZld#Z3)\"},{\"Old\":\"SsvOyC3AVUr7x951cMF5lA==\",\"New\":\"*@ZxbHgZqF5h1reAt$$rj2L3RwXJ7UtvYBt*EYXjONZ*V(3Rl\"},{\"Old\":\"fV8XpciiqOjYSkVr9j+GPA==\",\"New\":\"hhgJU(NVrl(69^)cUzw}3c8^dP*e0yFC^W9Ao%72I@IkgeD5v\"},{\"Old\":\"Hun2GwcIAPIuJuwY5lk44w==\",\"New\":\"#lcoCDi[h*bp[An]CSrSwfir0}eRmy$g92RA$03nH^!yT0kt&\"},{\"Old\":\"CrAny2X4L0cripsXtNjZKA==\",\"New\":\")ctZGNtR5{*VzC9GwncYq@&cCMd^ZxHLsLMt[9u^l*I(dlX]z\"},{\"Old\":\"CZfbYCvUvI99CFVE4q+WgA==\",\"New\":\"l4]t(WK*oM4c]6O8C4E*G3x[HOK*Ttj!Y3Y}E9MIrg}[F6lkA\"},{\"Old\":\"j/zSIjK0yzK/NWxCTs7puA==\",\"New\":\"]j0BH18%^Yi*e)uJpS2-pAg$w0$JTOJ^!N&&taJ#eI()MxOM0\"},{\"Old\":\"K+WBN7zLDQ2aJCPxDVrgiA==\",\"New\":\"(eDq4J)(ZL#37Q4y(N*@kjFH*r}KBYybR^OcePNDu53bqRZ26\"},{\"Old\":\"W5+yiKZ4JG0Y7YqhbjQs8w==\",\"New\":\"mv&RiYSomhQ0eI(Gm!A%uz%#ewoC]&%tM^HVu0vm0(u)%G9(7\"},{\"Old\":\"PHDCtV3YLgDxoEnaJ3i0dw==\",\"New\":\"cY9!&04aRgO@T9Ph1Gzj3&10OxX$eTzRuV%R46]pz8bxN(EX7\"},{\"Old\":\"bBjtGfHCLbHam5b6YIcqBw==\",\"New\":\"#pE*5}n&yDqT}18qaXYmKLzhcLyrYTSq!&}bk1fan(00]yvqA\"},{\"Old\":\"z8WLhpahwr0rF0innoF3hw==\",\"New\":\"gpHtzTREiirqqanRiP$1h}$Pg07l]PC7zPK0yV(pP64FlK3{N\"},{\"Old\":\"jF/9AEXZlYuWgOynSvMCDQ==\",\"New\":\"r7BTo-BLHX[h94TUT50Z-[(aGeAGTd4n8fcS3rU14E%&UZ^%6\"},{\"Old\":\"TYp3cU/VZo23NVwJwVfgrA==\",\"New\":\"NHxmCIbg[HN)IhQ!xeti-G}wNlMdKR#IY-Fm)LSvXNM{w8%Up\"},{\"Old\":\"u6vmlFtPG27rC2MLkNGwzw==\",\"New\":\"at3@(StGl!fqo1swo#t(A3@[#Z^uz0#0CJt^90FS13%)-J07A\"},{\"Old\":\"uQeWU3Gsej6IlQ4TrWsWgw==\",\"New\":\"8M$Wu$6}6bDj2wv1%KtKI]4V(27MkFoCCB4aNPzhhgmTBh}L-\"},{\"Old\":\"50w4BcL8wfXmgKqV8XtTOg==\",\"New\":\")cs!][s]j[EwlRj(}CY&A4Z3BtZrRZk61wl[)fOnYk)L9{C{7\"},{\"Old\":\"q0FFXqzQTFbkrUciKwMUKw==\",\"New\":\"^x69qMipAUMIXj9qPNYHxF^xmCwIET8JcF@Sm@@quQzxczDLZ\"},{\"Old\":\"x8xn++4DT9Oihv+IOz1GgQ==\",\"New\":\"4@Z%v(j2G39G]psu0IJF!7@3StZ@[-0mF*bml9!!W)xmc$cfy\"},{\"Old\":\"8z/8hasQvOYpegHpDT9h9w==\",\"New\":\"BKjsBBEg@ExV0mq[mn{v(oO$soadh46ttkvuLP81NS--nP8x(\"},{\"Old\":\"O4iAzl7frOprIOG3P93aCA==\",\"New\":\"D!{Y%k!Mx5pGAGZ}oGiVopY@^oXS!srV&75Sxn!PN7j&]v%CC\"},{\"Old\":\"RhZCHx/Ehil+n2cocUfZIw==\",\"New\":\"4CwTGy($KBFX]h]KbZO&*{FhQID)b#Cvx-UuqFV4broKkZd)!\"},{\"Old\":\"lYje+T5U3j2mtHVgq8y1pw==\",\"New\":\"qH2m$twDvlW@sJajbEB3Gu!Jt&v[ZaxpJ8pgeVNt(BjcN3sv#\"},{\"Old\":\"ald6ekwzJWyO0Gc1MzeIcg==\",\"New\":\"}0E&Z-b[lgt0w$D^C6L*GN](r7^v-$D5G2JsiN}#]aYr^93-H\"},{\"Old\":\"HN84+C1FODUZbkQo0qh8vw==\",\"New\":\"TCvd48nA0H]Ru7^tmEV{M&bXtGN-C%#3nEKG$iElk^^q{*Qpg\"},{\"Old\":\"BFZTeercp/ZDNyqoItiNsA==\",\"New\":\"[z8dMhVX8qN7C#Yf7XoL[$-A^*hA0g*5&$sk1dNIm@o!LwfkU\"},{\"Old\":\"dKGxTiUUmaEnjUln3uZiRQ==\",\"New\":\"6}aPFv)y40I0J-$EkLkQ{MC]$4}3gfDcv!5Kqnp%yL}IN^(k3\"},{\"Old\":\"Yf7TV6wrg2HS1BPf7qRGXg==\",\"New\":\"y)V0e4Aa7{qM@as*5hrexFv1E!e@Sd]qoF0^qj0xl#830B[Mh\"},{\"Old\":\"el/VQwmhCcQvzPmREx7LiQ==\",\"New\":\"sDeVJ5]HFTl)cmk^)fFX}*Km1)0iUID5CTp(Fo*cb3-%#3lj2\"},{\"Old\":\"hiuKyhvfdxZnPIEvvJGIOQ==\",\"New\":\"H3q#Zq5yoZstM@aewBSZ9jzL&f8yo56joEi@M$D@}M$(TqVuw\"},{\"Old\":\"1HEoUyzZVsjNQTcIG9of4Q==\",\"New\":\"u0rX!gV*T9PZ!J)mWH[gx!F!oE6hky)UI(6w0Z{&ZKKp9O*RM\"},{\"Old\":\"bxhphaiFEEyN/6GwgdB++w==\",\"New\":\"J8r^8p@76*tQWX]x0^!qk1S)]duCdUUziG@}W8kmbDE^JugRb\"},{\"Old\":\"YDhYnY6VxGmThhwgZxD+Yw==\",\"New\":\"c-A@ZxSCf6f@MTz$YW!O)Z-p1*Mh&Z0Q23Y(Y{$8eO0c5RscP\"},{\"Old\":\"PxfAKNH3ozBzKsqk6CyGxQ==\",\"New\":\"^%gKpom0M@unRb%208jobxlG*hR)3ckWgHzn4%I%d8WPTcv1W\"},{\"Old\":\"DGiPpWlEHM1ShhipUpowoA==\",\"New\":\"sY2@uF7!R7-0}YALNx8msEO6{r7L7j5VVE8@W2e0M7NEUvi1D\"},{\"Old\":\"ZsxYDNW806S5c0IOv08Z3w==\",\"New\":\"PAdKEomOc[cm9I@VnEH&aEzS-Csa{j8Mqqg]&(vocN}S^MxF(\"},{\"Old\":\"1D7o8LslGcM83WNcgptS4A==\",\"New\":\"7fH)RcQpfAwX^lQPfW%#O%^px6GjDVNx11F7CmoUm2YoN1C&[\"},{\"Old\":\"MC8N58d8VAkiKDYG5Lk9lw==\",\"New\":\"2A5U3C0[m1*-C@d#UmCs4$K(Op-^Q%}4%r&y33{}&v%d!X$uD\"},{\"Old\":\"xFX3eV/+nLuti7beQuVjXA==\",\"New\":\"JSL)5l7O4yZNxoaKW9Y3OA0QYHby9j7uQ5HRrb*1x@01Hf509\"},{\"Old\":\"dmeBY0CfS1Br1gMOsPRZ3w==\",\"New\":\"T9@A5o*9pbrhw^%O6#kD&iv^-7jcUP-HC](LBHg0ftlFmIr2u\"},{\"Old\":\"8KH7zx6xFDAPgSbArJOIoA==\",\"New\":\"[bBwgCb7){q]-R10Na0$OSo$mcQMFi{%KQOr0[XFupt^N]2BP\"},{\"Old\":\"G+dL5a0J+++ENunzFcmLJA==\",\"New\":\"HCLL)-HuDGZ0-#E!(Ki6A3WgdHcZ0X()yiY71#hBfNIwlpOL0\"},{\"Old\":\"6tYY26g3ptXhL8PkNRf38g==\",\"New\":\"B57ap%[X-RXb-qk5({7nuhrouIeX^&W#xJWbwTAI26ee&fpLx\"},{\"Old\":\"pEtuxGvxx4z+qxXxGLmsTQ==\",\"New\":\"eaH$HvFEV-N4ujVeZ2WMkA0%saFD4g#Jd(fbV9SL{gNN1({0e\"},{\"Old\":\"/1Lf42+7DEXEhhqRKT/Ekw==\",\"New\":\"-&YSH@3y}$ML#olzDlmpA*-OuA08&p{VAvF-rRDCh!27^b!$g\"},{\"Old\":\"1bbb/f8+GVYIE5+VFNeA1g==\",\"New\":\"}g9YiP&rxg2XDjaDkJtszqc^D9w5fHOQG(VpFmmCu01WaRLc6\"},{\"Old\":\"kqCnjqu2c9swOcpJZ4WeQg==\",\"New\":\"g5RnfnfH$jWUo^K1D5AxM)P$6Hz@xLX4(pjLAX{P*G@l)]qBX\"},{\"Old\":\"bVs0wv/emcooVuK8Cg1w2w==\",\"New\":\"kG75jfQ!ADzl!KyCqwi]{a@TT^00o1a3iGuE!F}WtnlT!gyMQ\"},{\"Old\":\"1qX9iSmsv10DP1gterTVew==\",\"New\":\"pD*SGZfole-(HS39zgEY29^}Qc8EbE63O{5sRJqm8gGF1m$Y-\"},{\"Old\":\"C+oRbCBM7EifQEri7YBKag==\",\"New\":\"fi26pA[rL*i(bqgtV&uVBQox&b8zpizKi]HXb5GiF(]xiFKAj\"},{\"Old\":\"gpCqNZmja5FWzMVAS2MGcw==\",\"New\":\"gUZwO5y{-uu&vwx[XHI-&jI61)xE{keCbuculmnKE55lkO40z\"},{\"Old\":\"Z5GzZTh/KTihcBVFKxIWJQ==\",\"New\":\"w5YtCQ$@aav9-Apg8MQx&@[WBFAib01&K$W0e0-3U*2T^Tb{E\"},{\"Old\":\"5rmli//1sDA4A8kWLruQcg==\",\"New\":\"0F!b47Ex[ZTH]-i90V%aX5!eL*[cXc0]1Vxh4#E!eHMO72s74\"},{\"Old\":\"tUv6Gs7TWkClhArWdO7SQQ==\",\"New\":\"%h%bby&}e%SpOa]Cb}4KrhQW{(6cUxnrx5B{8%t(^k9v#%1rL\"},{\"Old\":\"V2QU1L5m6Z0rRpG9a8xfQA==\",\"New\":\"#p[(wBQcowt89WrLm$0}52qxxm2Z[h)X0O0K5NMW3-nrQG^-n\"},{\"Old\":\"iVkLAudW7v65uX8osFeA0Q==\",\"New\":\"lSPE#[3a&CiXzsu[u{69[ZcIF!uRD(iSHJ^1hnr6v[cfj9!{K\"},{\"Old\":\"4tUMjfF9J6X00AUQ0mp+Xw==\",\"New\":\"2w@a$p8JT^CFPfdPnSa5^rfbGnl0gs0cfU68mM1gewp-fuh^B\"},{\"Old\":\"ACrM6Q9JgtPOrelJV60uVQ==\",\"New\":\"Xug^wNAmc1rF4az{H2[#NzQsp8^9h&Ae3750{JSTXPaVpTL67\"},{\"Old\":\"davWNFnezv/iSlPF2Rziyg==\",\"New\":\"HtaSB(ANT-mf#nVj7]}5$z#GYeeyJsmS0g3js{th50^y$Bmzb\"},{\"Old\":\"dfXNrjCA676xPwKjz6Wzzw==\",\"New\":\"#c8zHQ@yS^X3T6lh!6cioDc54WMs@6P!HXUj}JSC-#0[wQcNh\"},{\"Old\":\"foscUCOvCAyAHLpZnVOeSA==\",\"New\":\"zHn8xDt){ccQMSV6bw@B@3)ssads0RR@%nF^(#N4Z(#vSrRc-\"},{\"Old\":\"iEZXLl9f8dkvFBp2YnFXUA==\",\"New\":\"wT2EglIMuxtcLPv-@ND0]AiFYos@1lOpJgiiDd30-vu@(&dhV\"},{\"Old\":\"NLH2VVJpgxdJFCbhhsV0+A==\",\"New\":\"e78iQQdwauUfUVznrKp)2Usm34&[PieaUu!MgHVqQbCL0RI@d\"},{\"Old\":\"sZhO5daIxT6z3hBMldtQ8A==\",\"New\":\"ZNHR)DDBonbl}2XnmWjP#Do2WHuk7%1-KuP%^YNLS6b)85JXS\"},{\"Old\":\"vjTESMWafcKp4fNcJWn+rw==\",\"New\":\"Ftc{!vpjD3x[8N$6%I7qJ])u0af#XNzjtbzsF0p*-jo3XxI#$\"},{\"Old\":\"4cIaqEt66FEq/u0VhJFUEw==\",\"New\":\"JymVQexF@S0AI7IM0[HOS8UCR&WQx}gI]OxSj1DZmE6OGg)s^\"}]";
            dynamic outputJson = new JArray();
            try
            {
                dynamic oldToNewApiKeyToProcess = Newtonsoft.Json.JsonConvert.DeserializeObject<JArray>(oldToNewApiKeyToConvert);
                var APIKeyToAccount = BLL.Services.AccountServices.LoadAllAccounts().ToDictionary(acc => acc.Data.Credentials.PulseemAPIKey, acc => acc);
                //string warehousePath = @"C:\Users\user\Desktop";
                string warehousePath = @"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\warehouse";
                //string usersExcelPath = Path.Combine(warehousePath, "MdaOldVsNewKeys.xlsx");
                string usersNewExcelPath = Path.Combine(warehousePath, "MdaOldVsNewKeys__status.xlsx");

                string accountUpdateStatus = string.Empty;
                foreach (dynamic oldAndNewAPIKeyPair in oldToNewApiKeyToProcess)
                {
                    string oldAPIKey = oldAndNewAPIKeyPair.Old;
                    if (APIKeyToAccount.ContainsKey(oldAPIKey))
                    {
                        string newAPIKey = oldAndNewAPIKeyPair.New;
                        var currentAccount = APIKeyToAccount[oldAPIKey];
                        currentAccount.Data.Credentials.PulseemAPIKey = newAPIKey;
                        int savedAccountID = BLL.Services.AccountServices.Save(currentAccount);
                        oldAndNewAPIKeyPair.Status = savedAccountID == 0 ? "NotSaved" : "Saved";
                        oldAndNewAPIKeyPair.AccountID = savedAccountID;
                    }
                    else
                    {
                        oldAndNewAPIKeyPair.Status = "NoAccount";
                    }

                    outputJson.Add(oldAndNewAPIKeyPair);
                }
                SaveJsonAsCSV(outputJson, usersNewExcelPath);
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content(ex.Message + " --- " + ex.InnerException + " -- " + ex.StackTrace.ToString());
            }

        }
        private void SaveJsonAsCSV(JArray jData,string outputFilePath)
        {
            var sData = Newtonsoft.Json.JsonConvert.SerializeObject(jData);
            var table = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTable>(sData);
            FileInfo f = new FileInfo(outputFilePath);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage package = new ExcelPackage(f);

            ExcelWorksheet ws = package.Workbook.Worksheets.Add("Data");
            ws.Cells["A1"].LoadFromDataTable(table, false);
            package.Save();
        }
        public ActionResult UpdateAPIKeysForAccounts_Reversed()
        {
            //TODO: להשלים את השמירה של המפתחות החדשים
            //TODO: לדאוג להדגיש בקובץ את המפתחות שאין להם חשבונות
            try
            {
                var APIKeyToAccount = BLL.Services.AccountServices.LoadAllAccounts().ToDictionary(acc => acc.Data.Credentials.PulseemAPIKey, acc => acc);
                string usersExcelPath = @"C:\Users\user\Desktop\MdaOldVsNewKeys.xlsx";
                string usersExcelToUpdateStatus = @"C:\Users\user\Desktop\MdaOldVsNewKeys__status.xlsx";
                string accountUpdateStatus = string.Empty;

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage pck = new ExcelPackage();
                using (System.IO.FileStream s = System.IO.File.OpenRead(usersExcelPath))
                {
                    pck.Load(s);
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets[0];
                    int colCount = worksheet.Dimension.End.Column;  //get Column Count
                    int rowCount = worksheet.Dimension.End.Row;     //get row count

                    for (int row = 2; row <= rowCount; row++)
                    {
                        string newAPIKey = worksheet.Cells[row, 2].Value?.ToString();
                        if (APIKeyToAccount.ContainsKey(newAPIKey))
                        {
                            var currentAccount = APIKeyToAccount[newAPIKey];
                            string oldAPIKey = worksheet.Cells[row, 1].Value?.ToString();
                            currentAccount.Data.Credentials.PulseemAPIKey = oldAPIKey;
                            int savedAccountID = BLL.Services.AccountServices.Save(currentAccount);
                            worksheet.SetValue(row, 4, currentAccount.AccountID);
                            accountUpdateStatus = savedAccountID == 0 ? "NotSaved" : "Saved";
                        }
                        else
                        {
                            accountUpdateStatus = "NoAccount";
                            worksheet.Cells[row, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[row, 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                            worksheet.Cells[row, 3].Style.Font.Bold = true;
                        }
                        worksheet.SetValue(row, 3, accountUpdateStatus);
                    }
                    //pck.SaveAs(usersExcelToUpdateStatus);
                }
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

        }
        public ActionResult UpdateAPIKeysForAccounts()
        {
            try
            {
                var APIKeyToAccount = BLL.Services.AccountServices.LoadAllAccounts().ToDictionary(acc => acc.Data.Credentials.PulseemAPIKey, acc => acc);
                string warehousePath = @"C:\Users\user\Desktop";
                string usersExcelPath = Path.Combine(warehousePath, "MdaOldVsNewKeys.xlsx");
                string usersNewExcelPath = Path.Combine(warehousePath, "MdaOldVsNewKeys__status.xlsx");
                //string warehousePath = @"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\warehouse";
                //string usersExcelPath = Path.Combine(warehousePath, "MdaOldVsNewKeys.xlsx");
                //string usersNewExcelPath = Path.Combine(warehousePath, "MdaOldVsNewKeys__status.xlsx");

                string accountUpdateStatus = string.Empty;

                //ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage pck = new ExcelPackage();
                using (System.IO.FileStream s = System.IO.File.OpenRead(usersExcelPath))
                {
                    pck.Load(s);

                    ExcelWorksheet worksheet = pck.Workbook.Worksheets[0];
                    worksheet.SetValue(1, 3, "ChangeStatus");
                    worksheet.SetValue(1, 4, "AccountID");
                    for (int i = 1; i < 5; i++)
                    {
                        worksheet.Cells[1, i].Style.Font.Bold = true;
                        worksheet.Cells[1, i].Style.Font.UnderLine = true;
                    }


                    int colCount = worksheet.Dimension.End.Column;  //get Column Count
                    int rowCount = worksheet.Dimension.End.Row;     //get row count
                    for (int row = 2; row <= rowCount; row++)
                    {
                        string oldAPIKey = worksheet.Cells[row, 1].Value?.ToString();
                        if (APIKeyToAccount.ContainsKey(oldAPIKey))
                        {
                            var currentAccount = APIKeyToAccount[oldAPIKey];
                            string newAPIKey = worksheet.Cells[row, 2].Value?.ToString();
                            currentAccount.Data.Credentials.PulseemAPIKey = newAPIKey;
                            int savedAccountID = BLL.Services.AccountServices.Save(currentAccount);
                            worksheet.SetValue(row, 4, currentAccount.AccountID);
                            accountUpdateStatus = savedAccountID == 0 ? "NotSaved" : "Saved";
                        }
                        else
                        {
                            accountUpdateStatus = "NoAccount";
                            worksheet.Cells[row, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                            worksheet.Cells[row, 3].Style.Fill.BackgroundColor.SetColor(Color.FromArgb(255, 0, 0));
                            worksheet.Cells[row, 3].Style.Font.Bold = true;
                        }
                        worksheet.SetValue(row, 3, accountUpdateStatus);
                    }
                    pck.SaveAs(usersNewExcelPath);
                }
                return Content("Success");
            }
            catch (Exception ex)
            {
                return Content(ex.Message + " --- " + ex.InnerException + " -- "  + ex.StackTrace.ToString());
            }

        }

        public ActionResult BackupAccountsAsJson() // back up all old api keys. also DB back up was made
        {
            List<BLL.Account> accounts = BLL.Services.AccountServices.LoadAllAccounts();
            BLL.Services.LogServices.WriteLog("AccountAPIKeysBackup", "AccountAPIKeysBackup.json", Newtonsoft.Json.JsonConvert.SerializeObject(accounts));
            return Content("Success");
        }
        public void AddNotificationKeyToAccounts(string env)
        {
            string usersExcelPath;
            string resultsExcelPath;
            if (env.Equals("production"))
            {
                string rootPath = @"C:\Inetpub\vhosts\getmood.io\httpdocs\warehouse\";
                usersExcelPath = rootPath + "MoodNotificationUsersWithAccountKey.xlsx";
            }
            else
            {
                usersExcelPath = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsersWithAccountKey.xlsx";
            }

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            using (System.IO.FileStream s = System.IO.File.OpenRead(usersExcelPath))
            {
                pck.Load(s);
                var ws = pck.Workbook.Worksheets["Sheet1"];
                string currNotificationKey;
                string currAccountKey;
                BLL.Account currAccount;
                for (int i = 2; i <= ws.Dimension.Rows; i++)
                {

                    currNotificationKey = ws.Cells[i, 3].Value != null ? ws.Cells[i, 3].Value.ToString().Trim() : "";
                    currAccountKey = ws.Cells[i, 5].Value != null ? ws.Cells[i, 5].Value.ToString().Trim() : "";

                    currAccount = BLL.Services.AccountServices.LoadAccountByAccountKey(currAccountKey);
                    currAccount.Data.Credentials.PulseemNotificationKey = currNotificationKey;
                    BLL.Services.AccountServices.Save(currAccount);
                }
            }
        }

        public void MigrateAccountsFromMoodCMS(string env)
        {
            string usersExcelPath;
            string resultsExcelPath;
            if (env.Equals("production"))
            {
                string rootPath = @"C:\Inetpub\vhosts\getmood.io\httpdocs\warehouse\";
                usersExcelPath = rootPath + "MoodNotificationUsers.xlsx";
                resultsExcelPath = rootPath + "MoodNotificationUsersWithAccountKey.xlsx";
            }
            else
            {
                usersExcelPath = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsers.xlsx";
                resultsExcelPath = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsersWithAccountKey.xlsx";
            }
            //read excel
            List<string> AccountKeys = new List<string>();

        }

        public string XlsxToJson()
        {
            try
            {
                string warehousePath = @"C:\Users\user\Desktop";
                var file = System.IO.File.ReadAllText(Path.Combine(warehousePath, "MdaOldVsNewKeys.json"));
                string usersExcelPath = Path.Combine(warehousePath, "MdaOldVsNewKeys.xlsx");
                string accountUpdateStatus = string.Empty;
                dynamic jsonExcelContents = new JArray();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage pck = new ExcelPackage();
                using (System.IO.FileStream s = System.IO.File.OpenRead(usersExcelPath))
                {
                    pck.Load(s);
                    ExcelWorksheet worksheet = pck.Workbook.Worksheets[0];

                    int colCount = worksheet.Dimension.End.Column;  //get Column Count
                    int rowCount = worksheet.Dimension.End.Row;     //get row count
                    for (int row = 2; row <= rowCount; row++)
                    {
                        var jsonCurrentRow = new JObject();
                        jsonCurrentRow["Old"] = worksheet.Cells[row, 1].Value.ToString();
                        jsonCurrentRow["New"] = worksheet.Cells[row, 2].Value.ToString();
                        jsonExcelContents.Add(jsonCurrentRow);
                    }
                }
                string excelContents = Newtonsoft.Json.JsonConvert.SerializeObject(jsonExcelContents);
                System.IO.File.WriteAllText(Path.Combine(warehousePath, "MdaOldVsNewKeys.json"), excelContents);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }

        public ActionResult SetLastSendDateForSendAutomation(int sendAutomationID)
        {
            if (sendAutomationID == 0)
            {
                return Content("SendAutomationId invalid");
            }
            var sendAutomationToChange = Services.SendAutomationServices.LoadByID(sendAutomationID);
            if (sendAutomationToChange == null || sendAutomationToChange.SendAutomationID == 0)
            {
                return Content("SendAutomation invalid");
            }
            var eightDaysAgo = DateTime.Now.AddDays(-8);
            sendAutomationToChange.LastMessageSentDate = eightDaysAgo;
            return Services.SendAutomationServices.Save(sendAutomationToChange) > 0 ? Content("OK") : Content("save failed");
        }

        public ActionResult UpdateAccountPrimaryDomainAndEmails()
        {
            List<string> missingAccounts = new List<string>();
            var accountKeyToAccount = BLL.Services.AccountServices.LoadAllAccounts()
                .ToDictionary(a => a.AccountKey, a => a);
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            //var emailsExcelPath = @"C:\Users\user\Desktop\Open Assignments\Notifications\AccountUpdateSubscribersMails\PrimaryDomainsMigrationAndAccountEmails.xlsx";
            var emailsExcelPath = @"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\warehouse\PrimaryDomainsMigrationAndAccountEmails.xlsx";

            using (System.IO.FileStream s = System.IO.File.OpenRead(emailsExcelPath))
            {
                pck.Load(s);
                var ws = pck.Workbook.Worksheets["Sheet1"];
                var start = ws.Dimension.Start;
                var end = ws.Dimension.End;
                for (int row = start.Row+1; row <= end.Row; row++)
                {
                    string accountKey = ws.Cells[row, start.Column].Value.ToString();
                    string emails = ws.Cells[row, start.Column + 1].Value?.ToString() ?? string.Empty;
                    if (emails.Contains("חסר"))
                    {
                        emails = string.Empty;
                    }
                    string primaryDomain = ws.Cells[row, start.Column + 2].Value?.ToString() ?? string.Empty;

                    if (!accountKeyToAccount.ContainsKey(accountKey))
                    {
                        missingAccounts.Add(accountKey);
                        continue;
                    }
                    var account = accountKeyToAccount[accountKey];
                    account.Data.Emails = emails.Split(',').ToList();
                    account.Data.PrimaryDomain = primaryDomain;
                    BLL.Services.AccountServices.Save(account);
                }
            }
            return Content(Newtonsoft.Json.JsonConvert.SerializeObject(missingAccounts));
        }
        public ActionResult WriteDummyAccountSubscribersLogForAccounts()
        {
            var now = DateTime.Now;
            var accounts = BLL.Services.AccountServices.LoadAllAccounts();
            foreach (var account in accounts)
            {
                Services.AccountSubscribersUpdateLogServices.CreateDummyLogForAccount(account, now);
            }
            return Content(true.ToString());
        }
        public ActionResult ActivateAccountSubscribersUpdateForAllAccounts()
        {
            var accounts = BLL.Services.AccountServices.LoadAllAccounts();
            foreach (var account in accounts)
            {
                account.Data.AutomationTypeToData = new Dictionary<BLL.AutomationTypes, BLL.AccountAutomationData>();
                account.Data.AutomationTypeToData[BLL.AutomationTypes.AccountSubscribersUpdate] = new BLL.AccountAutomationData();
                account.Data.AutomationTypeToData[BLL.AutomationTypes.AccountSubscribersUpdate].IsActive = true;
                BLL.Services.AccountServices.Save(account);
            }
            return Content(true.ToString());
        }
    }
}