﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using Common;
using Newtonsoft.Json.Linq;

namespace NotificationsApp.Controllers
{
    public class SendAutomationsController : Controller
    {

        public ActionResult Index()
        {
            var model = ModelsServices.SendAutomationModelServices.GetSendAutomationsModelByPage(1);
            return View("~/Views/SendAutomations/_MainSendAutomations.cshtml", model);
        }
        public ActionResult GetSendAutomationsByPage(int pageNum)
        {
            var model = ModelsServices.SendAutomationModelServices.GetSendAutomationsModelByPage(pageNum);
            return View("~/Views/SendAutomations/_SendAutomationsTable.cshtml", model);
        }
        public ActionResult GetSendAutomationsByPageAndFilters_Overloading(int pageNum, int statusToProcess,string accountsDataToProcess,bool isForSingleAccount)
        {
            List<string> accountsFilters = new List<string>();
            var statusToFilter = (BLL.SendAutomationStatus)statusToProcess;
            if (!string.IsNullOrEmpty(accountsDataToProcess))
            {
                accountsFilters = accountsDataToProcess.Split(',').ToList();
            }
            
            var model = ModelsServices.SendAutomationModelServices.GetSendAutomationsModelByPageAndFilters_Overloading(pageNum,statusToFilter, accountsFilters);
            model.IsForSingleAccount = isForSingleAccount;
            return PartialView("~/Views/SendAutomations/_SendAutomationsTable.cshtml", model);
        }
        public ActionResult GetSendAutomationsByPageAndFilters(Models.SendAutomationFiltersModel filtersToProcess)
        {
            List<string> accountsFilters = new List<string>();
            BLL.SendAutomationFilters filtersModel = ModelsServices.SendAutomationFiltersModelServices.ProcessFilters(filtersToProcess);
            var model = ModelsServices.SendAutomationModelServices.GetSendAutomationsModelByPageAndFilters_Model(filtersModel);
            model.IsForSingleAccount = filtersToProcess.IsForSingleAccount;
            return PartialView("~/Views/SendAutomations/_SendAutomationsTable.cshtml", model);
        }
        //[HttpPost] //causes requests to fail in production
        public ActionResult SaveAutomation(FormCollection fc)
        {
            try
            {
                BLL.SendAutomation automationToUpdate = new BLL.SendAutomation
                {
                    TemplateMessage = new BLL.Message()
                };
                dynamic inTemplateMessage = new JObject();
                var now = DateTime.Now;
                int accountID = -1;
                int automationID = -1;

                var fileManager = FileManager.GetInstance();
                ManagedFile prevTemplateNotificationImage = null;



                if (fc["automation-id"] != null && int.TryParse(fc["automation-id"], out automationID))
                {
                    automationToUpdate.SendAutomationID = automationID;
                    automationToUpdate = Services.SendAutomationServices.LoadByID(automationID);
                    if (automationToUpdate == null)
                    {
                        return Content("לא ניתן לערוך את האוטומציה");
                    }
                }

                if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0) //overrides or new image is linked
                {
                    HttpPostedFileBase file = Request.Files[0];
                    //var img = Image.FromStream(file.InputStream, true, true);
                    var newTemplateNotificationImage = fileManager.SaveFile(file);

                    if (!string.IsNullOrEmpty(automationToUpdate.TemplateMessage.ImageUrl)) 
                    {
                        prevTemplateNotificationImage = Newtonsoft.Json.JsonConvert.DeserializeObject<Common.ManagedFile>(automationToUpdate.TemplateMessage.ImageUrl);

                    }
                    //TODO: not ideal to save as string on BLL
                    automationToUpdate.TemplateMessage.ImageUrl = Newtonsoft.Json.JsonConvert.SerializeObject(newTemplateNotificationImage);
                }

                inTemplateMessage.DisplayName = fc["message-name"];
                inTemplateMessage.Headline = fc["message-headline"];
                inTemplateMessage.MessageText = fc["message-text"];
                inTemplateMessage.ButtonText = fc["message-redirect-btn-text"];
                inTemplateMessage.ButtonRedirectLink = fc["message-redirect-link"];
                if (!BLL.Services.MessageServices.ValidateInputAndPopulateMessage(inTemplateMessage, automationToUpdate.TemplateMessage, -1))
                {
                    //end and return result accordingly
                    return Content("פרטי ההודעה שגויים");
                }
                if (!int.TryParse(fc["account-id"], out accountID))
                {
                    return Content("מזהה החשבון שגוי");
                }
                automationToUpdate.AccountID = accountID;
                automationToUpdate.TemplateMessage.AccountID = accountID;
                automationToUpdate.DisplayName = fc["automation-name"];
                if (string.IsNullOrEmpty(automationToUpdate.DisplayName))
                {
                    return Content("שם האוטומציה חסר");
                }

                automationToUpdate.Day = (DayOfWeek)int.Parse(fc["automation-day"]);
                automationToUpdate.Hour = int.Parse(fc["automation-hour"]);

                automationToUpdate.IsActive = true;
                automationToUpdate.CreationDate = now;

                automationID = Services.SendAutomationServices.Save(automationToUpdate);
                if (automationID <= 0)
                {
                    return Content("השמירה נכשלה");
                }

                if (prevTemplateNotificationImage != null) //delete previous image only if save succeeded and image was updated
                {
                    fileManager.DeleteFile(prevTemplateNotificationImage);
                }

                return Content("השמירה התבצעה בהצלחה");
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }
        public ActionResult CreateOrUpdateAutomation(int sendAutomationID = 0)
        {
            var model = new Models.UpdateSendAutomationModel();
            if (sendAutomationID != 0)
            {
                model.SendAutomation = Services.SendAutomationServices.LoadByID(sendAutomationID);
                model.Account = BLL.Services.AccountServices.LoadAccountByID(model.SendAutomation.AccountID);
            }
            return View("~/Views/SendAutomations/_UpdateSendAutomations.cshtml", model);
        }
        public ActionResult CreateAutomationForAccount(int accountID)
        {
            var model = new Models.UpdateSendAutomationModel();
            ActionResult errorResult = Content("לא ניתן לייצר אוטומציה עבור חשבון זה כי אינו קיים במערכת");
            if (accountID == 0)
            {
                return errorResult;
            }
            model.Account = BLL.Services.AccountServices.LoadAccountByID(accountID);
            if (model.Account.AccountID == 0)
            {
                return errorResult;
            }
            return View("~/Views/SendAutomations/_UpdateSendAutomations.cshtml", model);
        }
        public ActionResult DeleteAutomation(int automationID)
        {
            bool isSuccess = Services.SendAutomationServices.Delete(automationID);
            return Content(isSuccess.ToString());
        }
        public ActionResult ToggleStatus(int automationID)
        {
            var automation = Services.SendAutomationServices.LoadByID(automationID);
            if (automation.SendAutomationID == 0)
            {
                return null;
            }
            automation.IsActive = !automation.IsActive;
            int saveStatus = Services.SendAutomationServices.Save(automation);
            if (saveStatus == -1)
            {
                return null;
            }
            return Content(automation.IsActive.ToString());
        }

    }

}