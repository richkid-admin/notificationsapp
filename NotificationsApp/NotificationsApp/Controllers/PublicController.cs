﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NotificationsApp.Controllers
{
    public class PublicController : Controller
    {
        // GET: Public
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult KeepAlive()
        {
            return new HttpStatusCodeResult(200);
            Task.Run(() =>
            {
                string sLog = string.Empty;
                dynamic log = new JObject();
                int statusCode = 500;
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12/*(SecurityProtocolType)(0xc0 | 0x300 | 0xc00);*/;
                    string notificationsApiUrl = "https://notify.apps.getmood.io/NotificationsAPI/Home/KeepAlive";
                    string workerServicesUrl = "https://notify.apps.getmood.io/WorkerServices/Home/KeepAlive";
                    string notificationsApiErrors = string.Empty;
                    string workerServicesErrors = string.Empty;
                    statusCode = KeepOtherAlive(workerServicesUrl, out workerServicesErrors);
                    statusCode = KeepOtherAlive(notificationsApiUrl, out notificationsApiErrors);
                    log.NotificationsApiErrors = notificationsApiErrors;
                    log.WorkerServicesErrors = workerServicesErrors;
                    log.StatusCode = statusCode;
                    log.Time = DateTime.Now.ToShortTimeString();
                    sLog = Newtonsoft.Json.JsonConvert.SerializeObject(log);
                    return new HttpStatusCodeResult(200);
                }
                catch (Exception ex)
                {
                    log.NotificationsApiErrors = ex.ToString();
                    return new HttpStatusCodeResult(500);
                }
                finally
                {
                    BLL.Services.LogServices.WriteLog("PushNotificationLogs/KeepAliveLogs", "KeepAliveLog", sLog);
                }
            });
            return new HttpStatusCodeResult(200);
            
        }

        private int KeepOtherAlive(string url, out string errors)
        {
            int statusCode = 200;
            string resp = string.Empty;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse PulseemResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                using (Stream stream = PulseemResponse.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string response = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                resp = ex.ToString();
                statusCode = 500;
            }
            errors = resp;
            return statusCode;
        }

    }
}