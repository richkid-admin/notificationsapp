﻿function HandleContentResult(data,onSuccessCallback,onFailCallback) {
    if (!data || data.toString().toLowerCase() !== "true") {
        onSuccessCallback();
    }
    else {
        onFailCallback();
    }
}