﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using WorkerServices;
namespace Workers
{
    public class KeepAliveWorker
    {

        public static void RunKeepAliveWorker()
        {
            Worker KeepAliveWorker = new Worker
            {
                RunFunction = KeepOthersAlive,
                TimeInterval = new TimeSpan(0, 5, 0),
                TimeDelay = TimeSpan.Zero
            };

            KeepAliveWorker.StartWorker();

        }
        private static bool KeepOtherAlive(string url, out string errors)
        {
            bool success = false;
            HttpStatusCode statusCode = HttpStatusCode.InternalServerError;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    statusCode = response.StatusCode;
                }
                errors = string.Empty;
            }
            catch (Exception ex)
            {
                errors = ex.ToString();
            }
            return statusCode.Equals(HttpStatusCode.OK);
        }
        private static bool KeepOthersAlive() 
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12/*(SecurityProtocolType)(0xc0 | 0x300 | 0xc00);*/;
            string errors = string.Empty;
            bool isApiAlive = KeepOtherAlive("https://notify.apps.getmood.io/NotificationsAPI/Home/KeepAlive", out errors);
            bool isWorkerServicesAlive = KeepOtherAlive("https://notify.apps.getmood.io/WorkerServices/Home/KeepAlive", out errors);
            return isApiAlive && isWorkerServicesAlive;
        }

    }
}
