﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Auxiliary
{
    public class Tools
    {
        public static bool CheckUserTypeNotAllowedToLogin()
        {
            bool redirectToLogin = (Navigation.CurrentUser == null || Navigation.CurrentUser.Type != BLL.UserType.Admin);

            return redirectToLogin;
        }
    }
}