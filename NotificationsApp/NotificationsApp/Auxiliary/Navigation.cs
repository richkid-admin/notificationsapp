﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Auxiliary
{
    public class Navigation
    {
        public static BLL.User CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["CurrentUser"] != null)
                    return (BLL.User)HttpContext.Current.Session["CurrentUser"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["CurrentUser"] = value;
            }
        }
        public static string SortParam
        {
            get
            {
                if (HttpContext.Current.Session["SortParam"] != null)
                    return (string)HttpContext.Current.Session["SortParam"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["SortParam"] = value;
            }
        }
        public static string SortValue
        {
            get
            {
                if (HttpContext.Current.Session["SortValue"] != null)
                    return (string)HttpContext.Current.Session["SortValue"];
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["SortValue"] = value;
            }
        }
    }
}