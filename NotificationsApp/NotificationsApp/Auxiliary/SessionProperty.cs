﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Auxiliary
{
    public class SessionProperty<T> //TODO: switch property with property name and add to session 
    {
        private T _Property;

        private Type _PropertyType { get; set; }

        public T Property
        {
            get
            {
                
                return (T)Convert.ChangeType(_Property, _PropertyType);
            }
            set
            {
                // insert desired logic here
                _Property = value;
                _PropertyType = _Property.GetType();
            }
        }
    }
}