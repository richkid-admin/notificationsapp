﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NotificationsApp.Filters
{
    public class LoginFilters : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (Auxiliary.Tools.CheckUserTypeNotAllowedToLogin()) // If entered here - user is not allowed to login
            {

                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Login" }, { "action", "Index" } });
            }
            // add block ip check
            base.OnActionExecuting(filterContext);
        }
    }
}