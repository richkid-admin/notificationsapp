﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NotificationsApp.Models;

namespace ModelsServices
{
    public class SendAutomationFiltersModelServices
    {
        public static BLL.SendAutomationFilters ProcessFilters(SendAutomationFiltersModel model)
        {
            BLL.SendAutomationFilters sendAutomationProcessedFilters = new BLL.SendAutomationFilters();
            if (!string.IsNullOrEmpty(model.AccountsData))
            {
                sendAutomationProcessedFilters.AccountsData = model.AccountsData.Split(',').ToList();
            }
            sendAutomationProcessedFilters.SendAutomationStatus = (BLL.SendAutomationStatus)model.Status;
            sendAutomationProcessedFilters.PageNumber = model.PageNumber;
            return sendAutomationProcessedFilters;
        }
    }
}