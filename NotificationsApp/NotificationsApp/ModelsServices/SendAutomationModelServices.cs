﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NotificationsApp.Models;
using Common;

namespace ModelsServices
{
    public class SendAutomationModelServices
    {
        public static MainSendAutomationModel GetSendAutomationsModelByPage(int pageNum)
        {
            var model = new MainSendAutomationModel();
            int nAutomationsInPage = 10;
            List<string> a = new List<string>();
            model.SendAutomations = Services.SendAutomationServices.LoadSendAutomationsWithPaging(pageNum, out int numOfPages, nAutomationsInPage);
            model.AccountIDToAccount = Services.AccountServices.LoadAccountsBySendAutomations(model.SendAutomations);
            model.NumOfPages = numOfPages;
            model.CurrentPage = pageNum;
            return model;

        }

        public static MainSendAutomationModel GetSendAutomationsModelByPageAndFilters_Overloading(int pageNum, BLL.SendAutomationStatus statusToFilter, List<string> accountsDataToFilter)
        {
            var model = new MainSendAutomationModel();
            int nAutomationsInPage = 10;
            //model.SendAutomations = Services.SendAutomationServices.GetSendAutomationsModelByPageAndFilters_Overloading(pageNum, statusToFilter, accountsDataToFilter, out int numOfPages, nAutomationsInPage);
            model.SendAutomations = Services.SendAutomationServices.GetSendAutomationsModelByPageAndFilters_Overloading(pageNum, statusToFilter, accountsDataToFilter, out int numOfPages, nAutomationsInPage);
            model.NumOfPages = numOfPages;
            model.CurrentPage = pageNum;
            model.AccountIDToAccount = Services.AccountServices.LoadAccountsBySendAutomations(model.SendAutomations);
            return model;
        }
        public static MainSendAutomationModel GetSendAutomationsModelByPageAndFilters_Model(BLL.SendAutomationFilters filters)
        {
            var model = new MainSendAutomationModel();
            model.SendAutomations = Services.SendAutomationServices.GetSendAutomationsModelByPageAndFilters_Model(filters);
            model.NumOfPages = filters.NumberOfPages;
            model.CurrentPage = filters.PageNumber;
            model.AccountIDToAccount = Services.AccountServices.LoadAccountsBySendAutomations(model.SendAutomations);
            return model;
        }

    }
}