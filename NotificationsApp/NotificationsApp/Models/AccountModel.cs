﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Models
{
    public class AccountModel : MainSendAutomationModel
    {
        public Account Account { get; set; }
        public List<Account> ListOfAccounts { get; set; }
        public List<Package> ListOfPackages { get; set; } 
        public List<AccountPackageLog> AccountPackageLogsForAccount { get; set; }
        public string SaveMsg { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMsg { get; set; }
    }
}