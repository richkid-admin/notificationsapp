﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Models
{
    public class MainSendAutomationModel
    {
        public List<BLL.SendAutomation> SendAutomations { get; set; }
        public Dictionary<int,BLL.Account> AccountIDToAccount { get; set; }
        public int NumOfPages { get; set; }
        public int CurrentPage { get; set; }
        public bool IsForSingleAccount { get; set; }

        public MainSendAutomationModel()
        {
            SendAutomations = new List<BLL.SendAutomation>();
            AccountIDToAccount = new Dictionary<int, BLL.Account>();
        }
        
        
    }
}