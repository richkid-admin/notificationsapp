﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Models
{
    public class UpdateSendAutomationModel
    {
        public BLL.SendAutomation SendAutomation { get; set; }
        public BLL.Account Account { get; set; }
        
    }
}