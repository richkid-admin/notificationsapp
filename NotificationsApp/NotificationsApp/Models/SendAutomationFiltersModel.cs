﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Models
{
    public class SendAutomationFiltersModel
    {
        public int PageNumber { get; set; }
        public string AccountsData { get; set; }
        public int Status { get; set; }
        public bool IsForSingleAccount { get; set; }
    }
}