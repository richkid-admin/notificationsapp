﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NotificationsApp.Models
{
    public class OperationalReportModel
    {
        public string SiteName { get; set; }
        public DateTime AccountCreationDate { get; set; }
        public int NumberOfSubscribers { get; set; }
        public string CurrentPackage{ get; set; }
        public string NeededPackage { get; set; }
        public int NumberOfMessagesInLase30Days { get; set; }
        public int NumberOfMessagesInLase90Days { get; set; }
    }
}