﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    //[TestClass]
    public class SendAutomationTests
    {
        private BLL.SendAutomation _SendAutomation { get; set; }

        public SendAutomationTests()
        {
            _SendAutomation = new BLL.SendAutomation()
            {
                CreationDate = DateTime.Now,
                AccountID = 119,//richkid shop
                DisplayName = "Test",
                TemplateMessage = new BLL.Message(),
            };
        }
        [TestInitialize]
        public void Setup()
        {
        }

        [TestCleanup]
        public void Cleanup()
        {
            Services.SendAutomationServices.Delete(_SendAutomation.SendAutomationID);
        }

        [TestMethod]
        public void ActiveAutomationsToSend_Now_ShouldReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.Hour;
            _SendAutomation.LastMessageSentDate = null;
            _SendAutomation.IsActive = true;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count > 0);
            Assert.AreEqual(_SendAutomation, activeAutomations[0]);
        }
        [TestMethod]
        public void ActiveAutomationsToSend_WeekAgo_ShouldReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.Hour;
            _SendAutomation.LastMessageSentDate = DateTime.Now.AddDays(-7);
            _SendAutomation.IsActive = true;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count > 0);
            Assert.AreEqual(_SendAutomation, activeAutomations[0]);
        }
        [TestMethod]
        public void ActiveAutomationsToSend_AnHourAgo_ShouldNotReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.Hour;
            _SendAutomation.LastMessageSentDate = DateTime.Now.AddHours(-1);
            _SendAutomation.IsActive = true;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count == 0);
        }
        [TestMethod]
        public void ActiveAutomationsToSend_AnHourFromNow_ShouldNotReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.Hour;
            _SendAutomation.LastMessageSentDate = DateTime.Now.AddHours(1);
            _SendAutomation.IsActive = true;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count == 0);
        }
        [TestMethod]
        public void ActiveAutomationsToSend_WeekPlusHourAgo_ShouldReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.Hour;
            _SendAutomation.LastMessageSentDate = DateTime.Now.AddDays(-7).AddHours(-1);
            _SendAutomation.IsActive = true;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count > 0);
        }

        [TestMethod]
        public void AutomationsToSend_WeekMinusHourAgo_ShouldNotReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.AddHours(1).Hour;
            _SendAutomation.LastMessageSentDate = DateTime.Now.AddDays(-7).AddHours(1);
            _SendAutomation.IsActive = true;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count == 0);
        }
        [TestMethod]
        public void ActiveAutomationsToSend_Inactive_ShouldNotReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.Hour;
            _SendAutomation.LastMessageSentDate = null;
            _SendAutomation.IsActive = false;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count == 0);
        }
        [TestMethod]
        public void ActiveAutomationsToSend_DifferentTime_ShouldNotReturn()
        {
            // Arrange
            _SendAutomation = new BLL.SendAutomation();
            _SendAutomation.Day = DateTime.Now.AddDays(-1).DayOfWeek;
            _SendAutomation.Hour = DateTime.Now.Hour;
            _SendAutomation.LastMessageSentDate = null;
            _SendAutomation.IsActive = false;
            _SendAutomation.DisplayName = "Test";
            _SendAutomation.SendAutomationID = Services.SendAutomationServices.Save(_SendAutomation);
            // Act
            var activeAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();

            // Assert
            Assert.IsTrue(activeAutomations.Count == 0);
        }

    }
}
