﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    public class AccountSubscribersUpdateTests
    {
        private Dictionary<int,BLL.AccountSubscribersUpdateLog> _AccountIDToLastSendLogForAccount { get; set; }
        private BLL.AccountSubscribersUpdateLog _NewLog { get; set; }
        private BLL.Account _CurrentAccount{ get; set; }

        public AccountSubscribersUpdateTests()
        {
            
        }
        [TestInitialize]
        public void Setup()
        {
            _NewLog = new BLL.AccountSubscribersUpdateLog();
            _CurrentAccount = new BLL.Account();
            _AccountIDToLastSendLogForAccount = new Dictionary<int, BLL.AccountSubscribersUpdateLog>();
            _AccountIDToLastSendLogForAccount[0] = new BLL.AccountSubscribersUpdateLog();
        }

        //[TestCleanup]
        //public void Cleanup()
        //{

        //}

        [TestMethod]
        public void IsNextStepReached_NoPreviousLog_NumberOfSubscribersUnder250_ShouldNotSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 249;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsFalse(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 0);
        }
        [TestMethod]
        public void IsNextStepReached_NoPreviousLog_NumberOfSubscribersIs250_ShouldSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 250;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsTrue(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 250);
        }
        [TestMethod]
        public void IsNextStepReached_NoPreviousLog_NumberOfSubscribersBetween250And300_ShouldSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 251;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsTrue(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 250);
        }
        [TestMethod]
        public void IsNextStepReached_NoPreviousLog_ShouldReturnNumberOfSubscribersAsNewStep()
        {
            // Arrange
            List<int> nSubsToTest = new List<int>() { 300, 400, 750, 1000};
            foreach (int subToTest in nSubsToTest)
            {
                _CurrentAccount.NumberOfSubscribers = subToTest;
                // Act
                bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
                // Assert
                Assert.AreEqual(_NewLog.NumberOfSubscribersStep, _CurrentAccount.NumberOfSubscribers);
            }
        }
        [TestMethod]
        public void IsNextStepReached_NoPreviousLog_IncreasingSteps_ShouldReturnClosestNextStep()
        {
            // Arrange
            List<int> nSubsToTest = new List<int>() { 9542, 14932, 40500 };
            _CurrentAccount.NumberOfSubscribers = 9542;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 9500);

            _CurrentAccount.NumberOfSubscribers = 14932;
            // Act
            isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 14500);

            _CurrentAccount.NumberOfSubscribers = 40500;
            // Act
            isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 40000);


        }

        [TestMethod]
        public void IsNextStepReached_WithPreviousLog_NumberOfSubscribersIsUnderFirstStepAndPrevStepUnderFirstStep_ShouldNotSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 249;
            var prevLog = new BLL.AccountSubscribersUpdateLog() { NumberOfSubscribersStep = 230 };
            _AccountIDToLastSendLogForAccount[_CurrentAccount.AccountID] =  prevLog;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsFalse(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 0);
        }
        [TestMethod]
        public void IsNextStepReached_WithPreviousLog_NumberOfSubscribersIsAtFirstStepAndPrevStepUnderFirstStep_ShouldSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 250;
            var prevLog = new BLL.AccountSubscribersUpdateLog() { NumberOfSubscribersStep = 230 };
            _AccountIDToLastSendLogForAccount[_CurrentAccount.AccountID] = prevLog;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsTrue(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 250);
        }
        [TestMethod]
        public void IsNextStepReached_WithPreviousLog_NumberOfSubscribersAndPrevStepAtSameStep_ShouldNotSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 250;
            var prevLog = new BLL.AccountSubscribersUpdateLog() { NumberOfSubscribersStep = 251 ,IsSuccessful = true};
            _AccountIDToLastSendLogForAccount[_CurrentAccount.AccountID] = prevLog;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsFalse(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 250);
        }
        [TestMethod]
        public void IsNextStepReached_WithPreviousLog_NumberOfSubscribersAtNextStep_ShouldSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 300;
            var prevLog = new BLL.AccountSubscribersUpdateLog() { NumberOfSubscribersStep = 250, IsSuccessful = true };
            _AccountIDToLastSendLogForAccount[_CurrentAccount.AccountID] = prevLog;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsTrue(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 300);
        }
        [TestMethod]
        public void IsNextStepReached_WithPreviousLog_NumberOfSubscribersAtLastStep_ReachedNextInternalStep_ShouldSend()
        {
            // Arrange
            _CurrentAccount.NumberOfSubscribers = 33000;
            var prevLog = new BLL.AccountSubscribersUpdateLog() { NumberOfSubscribersStep = 31000, IsSuccessful = true };
            _AccountIDToLastSendLogForAccount[_CurrentAccount.AccountID] = prevLog;
            // Act
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(_CurrentAccount, _AccountIDToLastSendLogForAccount, _NewLog);
            // Assert
            Assert.IsTrue(isNextStepReached);
            Assert.AreEqual(_NewLog.NumberOfSubscribersStep, 33000);
        }


    }
}
