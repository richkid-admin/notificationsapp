﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using BLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PostalServices 
{
    public class PulseemPostalService : PostalService
    {
        internal class MainAccountStatisticsResponse
        {
            internal class SubAccountStatistics
            {
                public int SubAccountId { get; set; }
                public string SubAccountName { get; set; }
                public string LoginUserName { get; set; }
                public int Subscribers { get; set; }
                public int UnSubscribers { get; set; }
            }
            internal class AccountStatistics
            {
                public int TotalSubscribers { get; set; }
                public int TotalUnSubscribers { get; set; }
            }

            internal IEnumerable<SubAccountStatistics> Result { get; set; }

            internal AccountStatistics Total { get; set; }
        }

        internal class SendNotificationResponse
        {
            public int NotifictionId { get; set; }
            public int ResponseType { get; set; }
            public List<String> ErrorList { get; set; }
        }
        internal class CreateSubAccountResponse
        {
            public string Status { get; set; }
            public string ErrorMessage { get; set; }
            public string DirectAccountPassword { get; set; }

        }
        internal class NotificationsStatisticsResponse
        {
            private static readonly Dictionary<int, string> StatusIDToDescription = new Dictionary<int, string>
        {
            { 0 , "Created" },
            { 1 , "Deleted" },
            { 2 , "Pending" },
            { 3 , "Sending" },
            { 4 , "Sent" },
            { 5 , "Scheduled" },
            { 11 , "Canceled" },
        };
            public int ID { get; set; }
            public int SubAccountID { get; set; }
            public string Title { get; set; }
            public string Name { get; set; }
            public bool IsSent { get; set; }
            public DateTime? SendDate { get; set; }
            public bool IsDeleted { get; set; }
            public int SentCount { get; set; }
            public int ReceivedCount { get; set; }
            public int ClickCount { get; set; }
            public int? FailedCount { get; set; }
            public bool? UnSubscribed { get; set; }
            public int StatusID { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime UpdatedDate { get; set; }
            public bool HasGroups { get; set; }
            public string StatusDescription
            {
                get
                {
                    return StatusIDToDescription[StatusID];
                }
            }

        }

        internal class SubAccountSubscribersCountResponse
        {
            public int Message { get; set; }
            public int ResponseType { get; set; }

        }

        private string GeneralBaseURL;
        private string PushNotificationsBaseURL;
        private readonly string RichkidAPIKEy;
        public PulseemPostalService()
        {
            //change of API
            //GeneralBaseURL = "https://api.pulseem.com/api/v1/"; 
            //PushNotificationsBaseURL = "https://ui-api.pulseem.com/api/v1/NotificationApi/";
            GeneralBaseURL = "https://api.pulseem.co.il/api/v1/";
            PushNotificationsBaseURL = "https://ui-api.pulseem.co.il/api/v1/NotificationApi/";
            //RichkidAPIKEy = "fi26pA[rL*i(bqgtV&uVBQox&b8zpizKi]HXb5GiF(]xiFKAj";  // OLD API Key => "C+oRbCBM7EifQEri7YBKag==";
            RichkidAPIKEy = "6@O@-#^Dty!wmMDN}%MbZIe4{rZxnAAoj^fD#2uVU59$JX)s3";
        }

        //public override string AddSubAccount(string subAccountName, string mainAccountAPIKey="")
        //{
        //    try
        //    {
        //        if (!Helpers.ContainsValue(subAccountName))
        //        {
        //            return string.Empty;
        //        }
        //        if (!Helpers.ContainsValue(mainAccountAPIKey))
        //        {
        //            mainAccountAPIKey = RichkidAPIKEy;
        //        }
        //        string accountCreationURL = GeneralBaseURL + "AccountsApi/AddNewSubaccountAndDirectAcount";
        //        string sCreationMessage = MessageBuilder.BuildAccountCreationMessage(subAccountName, mainAccountAPIKey);
        //        if (!Helpers.SendRequest("POST", mainAccountAPIKey, accountCreationURL, sCreationMessage, out string sCreationResponse))
        //        {
        //            //log the error with creationMessage - request sending failed 
        //            return string.Empty;
        //        }
        //        CreateSubAccountResponse creationResponseMessage = JsonConvert.DeserializeObject<CreateSubAccountResponse>(sCreationResponse);
        //        if (creationResponseMessage.Status.ToLower().Equals("error") || Helpers.ContainsValue(creationResponseMessage.ErrorMessage))
        //        {
        //            //log the error messages - request succeeded but intended action failed
        //            return string.Empty;
        //        }
        //        string SubAccountAPIKey = creationResponseMessage.DirectAccountPassword;
        //        return SubAccountAPIKey;
        //    }
        //    catch (Exception ex)
        //    {
        //        // log the error - unexpected error during deserialization or otherwise
        //        return string.Empty;
        //    }

        //}

        public override string AddSubAccount(string subAccountName, out bool success, string mainAccountAPIKey = "")
        {
            success = false;
            try
            {
                if (!Helpers.ContainsValue(subAccountName))
                {
                    return string.Empty;
                }
                if (!Helpers.ContainsValue(mainAccountAPIKey))
                {
                    mainAccountAPIKey = RichkidAPIKEy;
                }
                string accountCreationURL = GeneralBaseURL + "AccountsApi/AddNewSubaccountAndDirectAcount";
                string sCreationMessage = MessageBuilder.BuildAccountCreationMessage(subAccountName, mainAccountAPIKey);
                if (!Helpers.SendRequest("POST", mainAccountAPIKey, accountCreationURL, sCreationMessage, out string sCreationResponse))
                {
                    //log the error with creationMessage - request sending failed 
                    /*return string.Empty;*/
                    return sCreationResponse;
                }
                CreateSubAccountResponse creationResponseMessage = JsonConvert.DeserializeObject<CreateSubAccountResponse>(sCreationResponse);
                if (creationResponseMessage.Status.ToLower().Equals("error") || Helpers.ContainsValue(creationResponseMessage.ErrorMessage))
                {
                    //log the error messages - request succeeded but intended action failed
                    /*return string.Empty;*/
                    return sCreationResponse;
                }
                string SubAccountAPIKey = creationResponseMessage.DirectAccountPassword;
                success = true;
                return SubAccountAPIKey;
            }
            catch (Exception ex)
            {
                // log the error - unexpected error during deserialization or otherwise
                /*return string.Empty;*/
                return "General Exception - " + ex.Message + " - " + ex.StackTrace + " - " + ex.InnerException;
            }

        }


        public override NotificationStatistics GetNotificationStatisticsByID(int notificationID, string APIKey)
        {
            string NotificationStatisticsURL = PushNotificationsBaseURL + $"GetStatistics/{notificationID}";
            NotificationsStatisticsResponse pulseemStatistics;
            try
            {
                if (!Helpers.ContainsValue(APIKey))
                {
                    APIKey = RichkidAPIKEy;
                }

                if (!Helpers.SendRequest("GET", APIKey, NotificationStatisticsURL, "", out string sStatisticsRepsonse))
                {
                    //log the error with response as the error description - request sending failed 

                }
                string jsStatisticsRepsonse = (string)JsonConvert.DeserializeObject(sStatisticsRepsonse);
                pulseemStatistics = JsonConvert.DeserializeObject<NotificationsStatisticsResponse>(jsStatisticsRepsonse);

                if (!(Helpers.ContainsValue(pulseemStatistics.Title) && Helpers.ContainsValue(pulseemStatistics.Name) && pulseemStatistics.ID > 0))
                {
                    //log the error - won't accept message with no title or name. 
                    return null;
                }
                return Helpers.Convert(pulseemStatistics);
                
            }
            catch (Exception ex)
            {
                // log the error - unexpected error during deserialization or otherwise
                return null;
            }
        }

        public override int SendNotification(string APIKey, BLL.Message msg, DateTime sendDate, LanguageDirectionality textDirection = LanguageDirectionality.RightToLeft, int notificationID = -1)
        {
            notificationID = -1;
            string sendNotificationURL = PushNotificationsBaseURL + "SendNotification";
            string sNotificationMessage = MessageBuilder.BuildNewNotificationMessage(msg, sendDate, textDirection, notificationID);
            if (!Helpers.SendRequest("POST", APIKey, sendNotificationURL, sNotificationMessage, out string sNotificationResponse))
            {
                //log the error with sNotificationResponse - request sending failed 
            }
            SendNotificationResponse notificationResponseMessage;
            try
            {
                notificationResponseMessage = JsonConvert.DeserializeObject<SendNotificationResponse>(sNotificationResponse);
            }
            catch (Exception ex)
            {
                string jsResponse = (string)JsonConvert.DeserializeObject(sNotificationResponse);
                notificationResponseMessage = JsonConvert.DeserializeObject<SendNotificationResponse>(jsResponse);
            }
            if (notificationResponseMessage.ResponseType == -1)
            {
                //log error
                string sErrorList = notificationResponseMessage.ErrorList == null ? "Error information was not supplied by pulseem" : JsonConvert.SerializeObject(notificationResponseMessage.ErrorList);
            }
            notificationID = notificationResponseMessage.NotifictionId;
            return notificationID;

        }

        //[Obsolete("Fetching subscribers count per APIKey, not based on the 'parent' account ")]
        public override Dictionary<string, int> GetSubAccountsSubscribersCount(string mainAccountAPIKey = "")
        {
            Dictionary<string, int> accountIDToSubCount = new Dictionary<string, int>();
            if (!Helpers.ContainsValue(mainAccountAPIKey))
            {
                mainAccountAPIKey = RichkidAPIKEy;
            }
            string subsCountURL = PushNotificationsBaseURL + "GetAccountTotalSubscribers";
            try
            {
                if (!Helpers.SendRequest("GET", mainAccountAPIKey, subsCountURL, "", out string sSubscribersCountAllSubAccounts))
                {
                    //log the error with response as the error description - request sending failed 
                    return accountIDToSubCount;
                }
                //NOT OPERATIONAL 
                string jsResponse = JsonConvert.DeserializeObject<string>(sSubscribersCountAllSubAccounts);
                dynamic statisticsAllSubAccounts = JsonConvert.DeserializeObject<dynamic>(jsResponse);
                return null;
                //return statisticsAllSubAccounts.Result.ToDictionary(stats => stats.SubAccountId.ToString(), stats => stats.Subscribers);

            }
            catch (Exception ex)
            {
                // log the error - unexpected error during deserialization or otherwise
                return accountIDToSubCount;
            }
        }

        public override int GetAccountSubscribersCount(string APIKey) //returns -1 in case of errors to differentiate 
        {
            int subsCount = -1;
            if (!Helpers.ContainsValue(APIKey))
            {
                return subsCount;
            }
            string subsCountURL = PushNotificationsBaseURL + "GetSubscribersCount";
            try
            {
                if (!Helpers.SendRequest("GET", APIKey, subsCountURL, "", out string sSubsCountMessage))
                {
                    //log the error with response as the error description - request sending failed 
                    return subsCount;
                }
                string jsResponse = JsonConvert.DeserializeObject<string>(sSubsCountMessage);
                SubAccountSubscribersCountResponse subsCountMessage = JsonConvert.DeserializeObject<SubAccountSubscribersCountResponse>(jsResponse);
                if (subsCountMessage.ResponseType != 0)
                {
                    //log errors 
                }
                subsCount = subsCountMessage.Message;
                return subsCount;

            }
            catch (Exception ex)
            {
                // log the error - unexpected error during deserialization or otherwise
                return subsCount;
            }
        }
    }
}
