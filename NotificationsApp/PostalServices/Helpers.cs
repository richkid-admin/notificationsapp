﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PostalServices
{
    internal class Helpers
    {
        internal static bool ContainsValue(string input)
        {
            return !string.IsNullOrWhiteSpace(input);
        }
        internal static bool SendRequest(string method, string APIKey, string requestURL, string sMessage, out string serializedJResponse) //returns sResponse
        {
            bool success = true;
            Encoding encoding = Encoding.UTF8;
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(requestURL);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Headers.Add("APIKEY", APIKey);
                if (method == "POST")
                {
                    byte[] bMessage = encoding.GetBytes(sMessage);
                    httpWebRequest.ContentLength = bMessage.Length;
                    httpWebRequest.Method = method;

                    using (Stream requestBody = httpWebRequest.GetRequestStream())
                    {
                        requestBody.Write(bMessage, 0, bMessage.Length);
                    }
                }
                using (HttpWebResponse PulseemResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                using (Stream stream = PulseemResponse.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string sPulseemResponse = reader.ReadToEnd();
                    serializedJResponse = sPulseemResponse;
                    return success;
                }
            }

            catch (WebException ex)
            {
                JObject exceptionInformation = new JObject();
                exceptionInformation["Message"] = ex.Message;
                if (ex.InnerException != null)
                {
                    exceptionInformation["InnerMessage"] = ex.InnerException.Message;
                }
                exceptionInformation["StackTrace"] = ex.StackTrace;
                serializedJResponse = JsonConvert.SerializeObject(exceptionInformation);
                return !success;
            }



        }
        internal static NotificationStatistics Convert(PulseemPostalService.NotificationsStatisticsResponse pulseemNotificationsStatistics)
        {
            return new NotificationStatistics()
            {

                SubAccountID = pulseemNotificationsStatistics.SubAccountID,
                NotificationID = pulseemNotificationsStatistics.ID,
                Headline = pulseemNotificationsStatistics.Title,
                Name = pulseemNotificationsStatistics.Name,
                IsSent = pulseemNotificationsStatistics.IsSent,
                SendDate = pulseemNotificationsStatistics.SendDate ?? DateTime.Now,
                IsDeleted = pulseemNotificationsStatistics.IsDeleted,
                SentCount = pulseemNotificationsStatistics.SentCount,
                ReceivedCount = pulseemNotificationsStatistics.ReceivedCount,
                ClickCount = pulseemNotificationsStatistics.ClickCount,
                Status = pulseemNotificationsStatistics.StatusDescription,
                CreatedDate = pulseemNotificationsStatistics.CreatedDate,

            };

        }
    }
}
