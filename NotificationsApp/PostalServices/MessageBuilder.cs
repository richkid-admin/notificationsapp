﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostalServices
{
    public enum LanguageDirectionality
    {
        LeftToRight,
        RightToLeft,
    }
    internal class MessageBuilder
    {
        internal static string BuildAccountCreationMessage(string newAccountName, string mainAPIKey)
        {
            JObject creationMessage = new JObject
            {
                ["SubAccountName"] = newAccountName,
                ["AccountEmail"] = $"{newAccountName}@richkid-push.com",
                ["LoginUserName"] = $"Account:{newAccountName}",
                //creationMessage["LoginPassword"] = $"!{mainAccountKey}{newAccountName}!1@Aa"; 
                ["LoginPassword"] = "AaBbCcDd!!!@ABC" //Previous password makes the user credentials broken for some reason
            };
            return JsonConvert.SerializeObject(creationMessage);
        }

        internal static string BuildNewNotificationMessage(BLL.Message msg, DateTime sendDate, LanguageDirectionality textDirection, int notificationID)
        {
            
            //List<string> inputArgsRequireValidation = new List<string> { name, headLine, messageText };
            if (msg == null || !(Helpers.ContainsValue(msg.DisplayName) && Helpers.ContainsValue(msg.Headline) && Helpers.ContainsValue(msg.MessageText)) || sendDate == null) //Determined by Pulseem
            {
                return string.Empty;
            }
            string btnText = Helpers.ContainsValue(msg.ButtonText) ? msg.ButtonText : "לחץ כאן";
            
            JObject creationMessage = new JObject();


            creationMessage["ID"] = notificationID; //Default to signal Pulseem to generate a new NotificationID
            creationMessage["TemplateID"] = 0; //Deprecated in Pulseem's side, leave as 0.
            creationMessage["Name"] = msg.DisplayName;
            creationMessage["Title"] = msg.Headline;
            creationMessage["Body"] = msg.MessageText;
            creationMessage["Icon"] = msg?.Data?.LogoUrl ?? string.Empty; 
            creationMessage["Image"] = msg.ImageUrl;
            creationMessage["RedirectURL"] = msg.ButtonRedirectLink;
            creationMessage["Tag"] = ""; //currently not in use in Pulseem's side
            creationMessage["Direction"] = (int)textDirection;
            creationMessage["IsRenotify"] = true; //currently not in use, instructed by Pulseem to leave as True. 
            creationMessage["SendDate"] = sendDate;
            creationMessage["NotificationGroups"] = null; //Send to all subscribers by default
            creationMessage["RedirectButtonText"] = btnText;
            return JsonConvert.SerializeObject(creationMessage);

        }
    }
}
