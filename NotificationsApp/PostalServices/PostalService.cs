﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostalServices
{
    public class NotificationStatistics
    {
        public int NotificationID { get; set; }
        public int SubAccountID { get; set; }
        public string Headline { get; set; }
        public string Name { get; set; }
        public bool IsSent { get; set; }
        public DateTime SendDate { get; set; }
        public bool IsDeleted { get; set; }
        public int SentCount { get; set; }
        public int ReceivedCount { get; set; }
        public int ClickCount { get; set; }
        public string Status { get; set; }
        public DateTime CreatedDate { get; set; }

    }
    public abstract class PostalService
    {
        public abstract string AddSubAccount(string subAccountName, out bool success, string mainAccountAPIKey);
        public abstract int SendNotification(string subAccountAPIKey,BLL.Message messageToSend, DateTime sendDate, LanguageDirectionality textDirection = LanguageDirectionality.RightToLeft, int notificationID = -1);
        public abstract NotificationStatistics GetNotificationStatisticsByID(int notificationID, string subAccountAPIKey);
        public abstract Dictionary<string,int> GetSubAccountsSubscribersCount(string mainAccountAPIKey="");
        public abstract int GetAccountSubscribersCount(string subAccountAPIKey);

        public BLL.Message Convert(NotificationStatistics msgStats)
        {
            return new BLL.Message
            {
                AccountID = msgStats.SubAccountID,
                NotificationID = msgStats.NotificationID,
                Headline = msgStats.Headline,
                DisplayName = msgStats.Name,
                ClickCount = msgStats.ClickCount,
                //SentCount = msgStats.SentCount,
                ReceivedCount = msgStats.ReceivedCount,
                Status = msgStats.Status,
                //CreationDate = msgStats.CreatedDate, //Pulseem's create date, not ours
            };
        }

    }
}
