﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ManagedFile
    {
        public string FileID { get; set; }
        public string FileOriginalName { get; set; }
        public string FileExtension { get; set; }
    }
}
