﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class ExtensionMethods
    {
        public static T DeepClone<T>(this T obj) //must be [Serializable] 
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }
        public static bool IsEmpty<T>(this T obj) where T : ICollection
        {
            return !(obj != null && obj.Count > 0);
        }
        public static bool IsValidEmail(this string input)
        {
            var trimmedEmail = input.Trim();

            if (trimmedEmail.EndsWith("."))
            {
                return false; // suggested by @TK-421
            }
            var addr = new System.Net.Mail.MailAddress(input);
            return addr.Address.Equals(trimmedEmail);
        }
        public static bool IsValidUrl(this string input)
        {
            return Uri.TryCreate(input, UriKind.Absolute, out Uri uriResult)
                       && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }

        public static IQueryable<T> CommitPaging<T>(this IQueryable<T> objectsToPage, int nObjectsPerPage, int pageNum, out int pageCount)
        {
            int objectsCount = objectsToPage.Count();
            var pageCountNotRounded = (double)objectsCount / nObjectsPerPage;
            pageCount = (int)Math.Ceiling(pageCountNotRounded);
            return objectsToPage.Skip((pageNum - 1) * nObjectsPerPage).Take(nObjectsPerPage);
        }

        public static string ToDecimalString(this Enum enumValue)
        {
            return enumValue.ToString("D");
        }

        public static bool IsValidByInclusiveLength(this string input, int lengthInChars)
        {
            return !string.IsNullOrEmpty(input) && input.Length <= lengthInChars;
        }

    }

}
