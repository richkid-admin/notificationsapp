﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace Common
{
    public class FileManager
    {
        private static FileManager Instance { get; set; }

        private readonly string ProductionReadUrl = "https://notify.apps.getmood.io/warehouse";
        private readonly string LocalReadUrl = "http://localhost:63358/";

        private readonly string ProductionWritePath = @"C:\Inetpub\vhosts\notify.apps.getmood.io\httpdocs\warehouse";
        private readonly string LocalWritePath = @"C:\logs";

        private string PublicPath { get; set; }
        private string ServerPath { get; set; }
        public static FileManager GetInstance()
        {
            if (Instance == null)
            {
                Instance = new FileManager();
            }
            return Instance;
        }

        private FileManager()
        {
            // set env 
            var systemBaseUrl = System.Net.Dns.GetHostName().ToLower();
            string readFrom = string.Empty;
            string writeTo = string.Empty;
            if (systemBaseUrl.Contains("localhost") || systemBaseUrl.Equals("desktop-5000"))
            {
                readFrom = LocalReadUrl;
                writeTo = LocalWritePath;
            }
            else
            {
                readFrom = ProductionReadUrl;
                writeTo = ProductionWritePath;
            }
            //System.Environment.SetEnvironmentVariable("ReadUrl", readFrom);
            //System.Environment.SetEnvironmentVariable("WritePath", writeTo);
            PublicPath = readFrom;
            Directory.CreateDirectory(writeTo);
            ServerPath = writeTo;
        }
        public string GetFilePath(ManagedFile file, bool isPublic = false)
        {
            return Path.Combine( isPublic ? PublicPath : ServerPath, file.FileID + "_" + file.FileOriginalName + file.FileExtension);
        }
        public bool FileExists(ManagedFile file)
        {
            return File.Exists(GetFilePath(file));
        }
        public bool DeleteFile(ManagedFile file)
        {
            try
            {
                if (file != null && FileExists(file))
                {
                    string fPath = GetFilePath(file);
                    File.Delete(fPath);
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public ManagedFile SaveFile(HttpPostedFileBase inFileToSave) //returns full path to file
        {
            var outFileToSave = new ManagedFile();
            try
            {
                outFileToSave.FileOriginalName = Path.GetFileNameWithoutExtension(inFileToSave.FileName);
                outFileToSave.FileExtension = Path.GetExtension(inFileToSave.FileName);
                outFileToSave.FileID = Guid.NewGuid().ToString();
                string serverWriteFilePath = GetFilePath(outFileToSave,false);
                string publicReadFilePath = GetFilePath(outFileToSave,true);
                inFileToSave.SaveAs(serverWriteFilePath);
            }
            catch (Exception)
            {
            }
            return outFileToSave;

        }
        public string ReadFile(ManagedFile fileToRead)
        {
            try
            {
                string publicReadFilePath = GetFilePath(fileToRead, true);
                return File.ReadAllText(publicReadFilePath);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
