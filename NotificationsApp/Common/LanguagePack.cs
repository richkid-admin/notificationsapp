﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    //TODO: singleton
    public class LanguagePack 
    {
        public Dictionary<string, string> HebPack = new Dictionary<string, string>();
        public Dictionary<string, string> EngPack = new Dictionary<string, string>();
        public readonly string DayPrefix = "Day_";
        public LanguagePack()
        {
            AddDaysOfWeek();
        }


        //private void AddDaysOfWeek()
        //{
        //    HebPack.Add(DayPrefix + DayOfWeek.Sunday.ToDecimalString(), "ראשון");
        //    HebPack.Add(DayPrefix + DayOfWeek.Monday.ToDecimalString(), "שני");
        //    HebPack.Add(DayPrefix + DayOfWeek.Tuesday.ToDecimalString(), "שלישי");
        //    HebPack.Add(DayPrefix + DayOfWeek.Wednesday.ToDecimalString(), "רביעי");
        //    HebPack.Add(DayPrefix + DayOfWeek.Thursday.ToDecimalString(), "חמישי");
        //    HebPack.Add(DayPrefix + DayOfWeek.Friday.ToDecimalString(), "שישי");
        //    HebPack.Add(DayPrefix + DayOfWeek.Saturday.ToDecimalString(), "שבת");

        //    foreach (Enum day in Enum.GetValues(typeof(DayOfWeek)))
        //    {
        //        EngPack.Add(DayPrefix + day.ToDecimalString(), day.ToString());
        //    }


        //}
        private void AddDaysOfWeek()
        {
            HebPack.Add(DayPrefix + "0", "ראשון");
            HebPack.Add(DayPrefix + "1", "שני");
            HebPack.Add(DayPrefix + "2", "שלישי");
            HebPack.Add(DayPrefix + "3", "רביעי");
            HebPack.Add(DayPrefix + "4", "חמישי");
            HebPack.Add(DayPrefix + "5", "שישי");
            HebPack.Add(DayPrefix + "6", "שבת");

        }


    }
}
