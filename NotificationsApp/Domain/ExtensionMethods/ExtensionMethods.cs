﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Domain.ExtensionMethods
{
    public static class DomainExtensionMethods
    {
        public static IQueryable<TSource> WhereIf<TSource>(this IQueryable<TSource> source, bool condition, Expression<Func<TSource, bool>> predicate)
        {
            if (condition)
                return source.Where(predicate);

            return source;
        }
        public static IOrderedQueryable<Domain.Entities.SendAutomation> OrderByPriority(this IQueryable<Domain.Entities.SendAutomation> sendAutomations)
        {
            return sendAutomations
                .OrderByDescending(sa => sa.IsActive)
                .ThenBy(sa => sa.Day)
                .ThenBy(sa => sa.Hour);
        }
    }
}
