﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public class AccountPackageLogRepository : Repository
    {
        public AccountPackageLogRepository() : base()
        {

        }

        public Domain.Entities.AccountPackageLog LoadAccountPackageLogByAccountPackageLogID(int accountPackageLogID)
        {
            return this.PushNotificationsDB.AccountPackageLog.AsNoTracking().Where(a => a.AccountPackageLogID == accountPackageLogID).FirstOrDefault();
        }

        public List<Domain.Entities.AccountPackageLog> LoadAccountPackageLogsByAccountID(int accountID)
        {
            return this.PushNotificationsDB.AccountPackageLog.AsNoTracking().Where(a => a.AccountID == accountID).ToList();
        }

        public int Save(Domain.Entities.AccountPackageLog accountPackageLog)
        {
            AccountPackageLog tempAccountPackageLog = this.PushNotificationsDB.AccountPackageLog.FirstOrDefault(a => a.AccountPackageLogID == accountPackageLog.AccountPackageLogID);
            if (tempAccountPackageLog == null)
            {
                this.PushNotificationsDB.AccountPackageLog.Add(accountPackageLog);
            }
            else
            {
                this.PushNotificationsDB.Entry(tempAccountPackageLog).CurrentValues.SetValues(accountPackageLog);
            }
            this.PushNotificationsDB.SaveChanges();

            return accountPackageLog.AccountPackageLogID;
        }
    }
}
