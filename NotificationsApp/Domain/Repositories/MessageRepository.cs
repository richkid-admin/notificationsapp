﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public class MessageRepository : Repository
    {
        public MessageRepository() : base()
        {
        }

        public List<Domain.Entities.Message> LoadAllMessages()
        {
            return this.PushNotificationsDB.Message.AsNoTracking().ToList();
        }

        public List<Message> LoadAllMessagesByAccountKey(string accountKey)
        {
            int accountID = this.PushNotificationsDB.Account.AsNoTracking().Where(a => a.AccountKey == accountKey).FirstOrDefault().AccountID;
            return this.PushNotificationsDB.Message.AsNoTracking().Where(m => m.AccountID == accountID).ToList();
        }

        public Domain.Entities.Message LoadMessageByID(int messageID)
        {
            return this.PushNotificationsDB.Message.AsNoTracking().Where(m => m.MessageID == messageID).FirstOrDefault();
        }

        public List<Message> LoadArchiveMessagesWithPaging(int pageNum = 1, int messagesPerPage = 100)
        {
            return this.PushNotificationsDB.Message.AsNoTracking().OrderByDescending(m => m.CreationDate).Skip((pageNum - 1) * messagesPerPage).Take(messagesPerPage).ToList();
        }

        public int Save(Message message)
        {
            Message tempMessage = this.PushNotificationsDB.Message.FirstOrDefault(p => p.MessageID == message.MessageID);
            if (tempMessage == null)
            {
                this.PushNotificationsDB.Message.Add(message);
            }
            else
            {
                this.PushNotificationsDB.Entry(tempMessage).CurrentValues.SetValues(message);
            }
            this.PushNotificationsDB.SaveChanges();

            return message.MessageID;
        }

        public void Delete(Message message)
        {
            Message tempMessage = this.PushNotificationsDB.Message.First(m => m.MessageID == message.MessageID);
            this.PushNotificationsDB.Message.Remove(tempMessage);
            this.PushNotificationsDB.SaveChanges();
        }
        public Dictionary<int,List<int>> GetMessageIDsStartingFrom(DateTime earliestTime,bool succesfullySentOnly)
        {
            var messages = this.PushNotificationsDB.Message.Where(m => m.CreationDate >= earliestTime);

            if (succesfullySentOnly)
            {
                messages = messages.Where(m => !m.Status.Contains("Fail"));
            }


            return messages
                .Select(m => new { m.AccountID,m.NotificationID} )
                .GroupBy(m => m.AccountID)
                .ToDictionary(grp => grp.Key,grp => grp.Select(m => m.NotificationID).ToList());
        }

        
        public void UpdateStatistics(Dictionary<int,Message> NotificationIDToEntity)
        {
            List<Message> msgsToUpdate = this.PushNotificationsDB.Message.Where(msg => NotificationIDToEntity.Keys.Contains(msg.NotificationID)).ToList();
            foreach (Message msgToUpdate in msgsToUpdate)
            {
                int notificationID = msgToUpdate.NotificationID;
                msgToUpdate.ClickCount = NotificationIDToEntity[notificationID].ClickCount;
                msgToUpdate.ReceivedCount = NotificationIDToEntity[notificationID].ReceivedCount;
                //msgToUpdate.SentCount = NotificationIDToEntity[notificationID].SentCount; //should be based on subs number
                msgToUpdate.Status= NotificationIDToEntity[notificationID].Status;
                msgToUpdate.LastUpdateDate = DateTime.Now;
            }
            this.PushNotificationsDB.SaveChanges();
        }
    }
}