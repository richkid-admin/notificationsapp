﻿using System;

namespace Domain.Repositories
{
    abstract public class Repository : IDisposable
    {
        public Entities.PushNotificationsEntities PushNotificationsDB { get; set; }

        public Repository()
        {
            this.PushNotificationsDB = new Entities.PushNotificationsEntities();
            this.PushNotificationsDB.Configuration.LazyLoadingEnabled = false;
        }

        void IDisposable.Dispose()
        {
            this.PushNotificationsDB.Dispose();
        }

    }
}