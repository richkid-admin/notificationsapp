﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public class PackageRepository : Repository
    {
        public PackageRepository() : base()
        {
        }

        public List<Domain.Entities.Package> LoadAllPackages()
        {
            return this.PushNotificationsDB.Package.AsNoTracking().ToList();
        }

        public Domain.Entities.Package LoadPackageByID(int packageID)
        {
            return this.PushNotificationsDB.Package.AsNoTracking().Where(p => p.PackageID == packageID).FirstOrDefault();
        }

        public int Save(Package package)
        {
            Package tempPackage = this.PushNotificationsDB.Package.FirstOrDefault(p => p.PackageID == package.PackageID);
            if (tempPackage == null)
            {
                this.PushNotificationsDB.Package.Add(package);
            }
            else
            {
                this.PushNotificationsDB.Entry(tempPackage).CurrentValues.SetValues(package);
            }
            this.PushNotificationsDB.SaveChanges();

            return package.PackageID;
        }

        public void Delete(Package package)
        {
            Package tempPackage = this.PushNotificationsDB.Package.First(p => p.PackageID == package.PackageID);
            this.PushNotificationsDB.Package.Remove(tempPackage);
            this.PushNotificationsDB.SaveChanges();
        }
    }
}
