﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Domain.ExtensionMethods;

namespace Domain.Repositories
{
    public class TestRepository : Repository
    {
        public TestRepository() : base()
        {
        }

        public void TestQueryable()
        {
            bool isActive = false;
            List<string> accountsDataToFilter = new List<string>() { "123" };
            int pageNum = 1;
            int numOfPages = 0;
            int nAutomationsInPage = 10;

            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            var filteredSendAutomations = this.PushNotificationsDB.SendAutomation.AsQueryable();

            Console.WriteLine("Time for just queryable: " + timer.Elapsed.ToString() + "ms");

            filteredSendAutomations = filteredSendAutomations.Where(sa => sa.IsActive == isActive);
            filteredSendAutomations = filteredSendAutomations.Where(sa => accountsDataToFilter.Any(filter => filter.Contains(sa.Account.Name) || filter.Equals(sa.AccountID.ToString())));
            Console.WriteLine("Time for where queries: " + timer.Elapsed.ToString() + "ms");

            var allInclusive = filteredSendAutomations.OrderBy(sa => sa.IsActive)
                .ThenBy(sa => sa.CreationDate)
                .CommitPaging(nAutomationsInPage, pageNum, out numOfPages)
                .ToList();

            Console.WriteLine("Time for paging after queries and tolist: " + timer.Elapsed.ToString() + "ms");
            Console.ReadLine();
            timer.Stop();
        }
    }
}
