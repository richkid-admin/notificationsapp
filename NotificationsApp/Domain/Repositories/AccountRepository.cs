﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public class AccountRepository : Repository
    {
        public AccountRepository() : base()
        {
        }

        public List<Domain.Entities.Account> LoadAllAccounts()
        {
            return this.PushNotificationsDB.Account.AsNoTracking().ToList();
        }

        public Domain.Entities.Account LoadAccountByID(int accountID)
        {
            return this.PushNotificationsDB.Account.AsNoTracking().Where(a => a.AccountID == accountID).FirstOrDefault();
        }

        public bool CheckIfAccountExistByAccountKey(string accountKey)
        {
            return this.PushNotificationsDB.Account.AsNoTracking().Any(a => a.AccountKey == accountKey);
        }


        public Domain.Entities.Account LoadAccountByAccountKey(string accountKey)
        {
            return this.PushNotificationsDB.Account.AsNoTracking().Where(a => a.AccountKey == accountKey).FirstOrDefault();
        }

        public int LoadAccountIDByAccountKey(string accountKey)
        {
            return this.PushNotificationsDB.Account.AsNoTracking().Where(a => a.AccountKey == accountKey).FirstOrDefault().AccountID;
        }

        public List<Account> GetAccountsByFilters(Domain.Models.AccountsFiltersModel filters)
        {
            IQueryable<Account> accounts = null;
            if (filters.IsWithAutomations) {
                accounts = this.PushNotificationsDB.Message
                    .Include("Account")
                    .Where(m => m.SendAutomationID != null)
                    .Select(m => m.Account)
                    .Distinct();
            }
            else
            {
                accounts = this.PushNotificationsDB.Account;
            }

            
            if (filters.IsActive != null)
            {
                accounts = accounts.Where(a => a.PaymentApproved == filters.IsActive.GetValueOrDefault());
            }
            return accounts.ToList();
                
        }

        public int Save(Account account)
        {
            Account tempAccount = this.PushNotificationsDB.Account.FirstOrDefault(a => a.AccountID == account.AccountID);
            if (tempAccount == null)
            {
                this.PushNotificationsDB.Account.Add(account);
            }
            else
            {

                this.PushNotificationsDB.Entry(tempAccount).CurrentValues.SetValues(account);
            }
            this.PushNotificationsDB.SaveChanges();

            return account.AccountID;
        }

        public void Delete(Account account)
        {
            Account tempAccount = this.PushNotificationsDB.Account.First(a => a.AccountID == account.AccountID);
            this.PushNotificationsDB.Account.Remove(tempAccount);
            this.PushNotificationsDB.SaveChanges();
        }

        public List<Account> LoadAccountsByIds(ICollection<int> accountIDs)
        {
            //Func<int,bool> filterIDsCondition = (accountID) => exclude ? !accountIDs.Contains(accountID) : accountIDs.Contains(accountID);
            return this.PushNotificationsDB.Account.Where(acc => accountIDs.Contains(acc.AccountID)).ToList();
        }

        public void UpdateStatistics(Dictionary<int, int> AccountIDToSubscribersCount)
        {
            List<Account> loadedAccounts = this.PushNotificationsDB.Account.Where(acc => AccountIDToSubscribersCount.Keys.Contains(acc.AccountID)).ToList();
            foreach (Account accToUpdate in loadedAccounts)
            {
                accToUpdate.NumberOfSubscribers = AccountIDToSubscribersCount[accToUpdate.AccountID];
            }
            this.PushNotificationsDB.SaveChanges();
        }
    }
}
