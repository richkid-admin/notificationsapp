﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Domain.ExtensionMethods;
using Domain.Models;

namespace Domain.Repositories
{
    public class SendAutomationRepository : Repository
    {
        public SendAutomationRepository() : base()
        {
        }

        public List<Domain.Entities.SendAutomation> LoadSendAutomations(bool isActive)
        {

            return this.PushNotificationsDB.SendAutomation.AsNoTracking().Where(sa => sa.IsActive == isActive).ToList(); ;


        }
        public List<Domain.Entities.SendAutomation> LoadSendAutomationsWithPaging(int pageNum, int nAutomationsInPage, out int numOfPages)
        {
            return this.PushNotificationsDB.SendAutomation
                .OrderByPriority()
                .CommitPaging(nAutomationsInPage, pageNum, out numOfPages)
                .ToList();
        }

        public int Save(SendAutomation inSendAutomation)
        {
            var existingSendAutomation = this.PushNotificationsDB.SendAutomation.FirstOrDefault(sa => sa.SendAutomationID == inSendAutomation.SendAutomationID);
            if (existingSendAutomation == null)
            {
                this.PushNotificationsDB.SendAutomation.Add(inSendAutomation);
            }
            else
            {
                this.PushNotificationsDB.Entry(existingSendAutomation).CurrentValues.SetValues(inSendAutomation);
            }
            this.PushNotificationsDB.SaveChanges();

            return inSendAutomation.SendAutomationID;
        }

        public bool Delete(int sendAutomationID)
        {
            int changesMade = 0;
            var sendAutomationToDelete = this.PushNotificationsDB.SendAutomation.First(sa => sa.SendAutomationID == sendAutomationID);
            if (sendAutomationToDelete != null)
            {
                this.PushNotificationsDB.SendAutomation.Remove(sendAutomationToDelete);
                changesMade = this.PushNotificationsDB.SaveChanges();
            }
            return changesMade > 0;
        }

        public List<SendAutomation> GetSendAutomationsModelByPageAndFilters_Model(SendAutomationsFiltersModel filtersModel)
        {
            var filteredSendAutomations = this.PushNotificationsDB.SendAutomation.AsQueryable();
            if (filtersModel.SendAutomationStatus != null)
            {
                bool statusToFilter = filtersModel.SendAutomationStatus.GetValueOrDefault();
                filteredSendAutomations = filteredSendAutomations.Where(sa => sa.IsActive == statusToFilter);
            }
            if (filtersModel.AccountsData != null && filtersModel.AccountsData.Count > 0)
            {
                filteredSendAutomations = filteredSendAutomations.Where(sa => filtersModel.AccountsData.Any(filter => sa.Account.Name.Contains(filter) || sa.AccountID.ToString().Equals(filter)));
            }
            filteredSendAutomations = filteredSendAutomations
                .OrderByPriority()
                .CommitPaging(filtersModel.EntitiesPerPage, filtersModel.PageNumber, out int numOfPages);
            var filtersToReturn = filteredSendAutomations.ToList();
            filtersModel.NumberOfPages = numOfPages;
            return filtersToReturn;
        }



        public List<SendAutomation> LoadSendAutomationsWithPagingAndFilters_Overloading(int pageNum, bool isActive, List<string> accountsDataToFilter, int nAutomationsInPage, out int numOfPages)
        {
            return this.PushNotificationsDB.SendAutomation
                .Include("Account")
                .Where(sa => sa.IsActive == isActive && accountsDataToFilter.Any(filter => sa.Account.Name.Contains(filter) || sa.AccountID.ToString().Equals(filter)))
                .OrderByPriority()
                .CommitPaging(nAutomationsInPage, pageNum, out numOfPages)
                .ToList();

        }


        public List<SendAutomation> LoadSendAutomationsWithPagingAndFilters_Overloading(int pageNum, List<string> accountsDataToFilter, int nAutomationsInPage, out int numOfPages)
        {
            return this.PushNotificationsDB.SendAutomation
                .Include("Account")
                .Where(sa => accountsDataToFilter.Any(filter => sa.Account.Name.Contains(filter) || sa.AccountID.ToString().Equals(filter)))
                .OrderByPriority()
                .CommitPaging(nAutomationsInPage, pageNum, out numOfPages)
                .ToList();

        }
        public List<SendAutomation> LoadSendAutomationsWithPagingAndFilters_Overloading(int pageNum, bool isActive, int nAutomationsInPage, out int numOfPages)
        {
            return this.PushNotificationsDB.SendAutomation
                .Where(sa => sa.IsActive == isActive)
                .OrderByPriority()
                .CommitPaging(nAutomationsInPage, pageNum, out numOfPages)
                .ToList();

        }
        public List<SendAutomation> LoadActiveAutomationsForNow()
        {
            var now = DateTime.Now;
            var hourWithPastSafetyMargin = now.AddHours(-2).Hour;
            return this.PushNotificationsDB.SendAutomation.AsNoTracking()
                .Where(sa => sa.IsActive)
                .Where(sa => sa.Day == (int)now.DayOfWeek) //same day
                .Where(sa => (sa.Hour <= now.Hour) && ((sa.Hour == 0 ? 24 : sa.Hour) >= hourWithPastSafetyMargin)) //in hour interval
                .Where(sa => sa.LastMessageSentDate == null /*isFirstSend*/ || (System.Data.Entity.DbFunctions.DiffDays(sa.LastMessageSentDate, now) >= 6 /*7 causes edge cases problems*/) /*has been at least a week since last send*/)
                .ToList();
        }

        public SendAutomation LoadByID(int sendAutomationID)
        {
            return this.PushNotificationsDB.SendAutomation.AsNoTracking().Where(sa => sa.SendAutomationID == sendAutomationID).FirstOrDefault();
        }

        public List<SendAutomation> LoadSendAutomationsWithMessages()
        {
            return this.PushNotificationsDB.Message.Include("SendAutomation")
                .Where(m => m.SendAutomationID != null)
                .Distinct()
                .Select(m => m.SendAutomation)
                .ToList();

        }
    }
}
