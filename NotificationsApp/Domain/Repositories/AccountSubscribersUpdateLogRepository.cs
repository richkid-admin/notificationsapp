﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public class AccountSubscribersUpdateLogRepository : Repository
    {
        public AccountSubscribersUpdateLogRepository() : base()
        {

        }

        public List<AccountSubscribersUpdateLog> LoadLastLogForAllAccounts()
        {
            //groupby ensures only accounts with logs are selected, so no need to check if firstOrDefault returns null
            return this.PushNotificationsDB.AccountSubscribersUpdateLog.AsNoTracking()
                .GroupBy(a => a.AccountID) 
                .Select(aGrp => aGrp.OrderByDescending(a => a.UpdateSendDate).FirstOrDefault()/*last*/)
                .ToList();

        }

        public int Save(AccountSubscribersUpdateLog log)
        {
            var tempLog = this.PushNotificationsDB.AccountPackageLog.FirstOrDefault(a => a.AccountPackageLogID == log.AccountSubscribersUpdateLogID);
            if (tempLog == null)
            {
                this.PushNotificationsDB.AccountSubscribersUpdateLog.Add(log);
            }
            else
            {
                this.PushNotificationsDB.Entry(tempLog).CurrentValues.SetValues(log);
            }
            this.PushNotificationsDB.SaveChanges();

            return log.AccountSubscribersUpdateLogID;
        }
    }
}
