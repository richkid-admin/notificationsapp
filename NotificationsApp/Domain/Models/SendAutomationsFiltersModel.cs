﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class SendAutomationsFiltersModel : PagingModel
    {
        public List<string> AccountsData { get; set; }
        public bool? SendAutomationStatus { get; set; }

    }

}
