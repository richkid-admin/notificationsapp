﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class PagingModel
    {
        public int EntitiesPerPage { get; set; }
        public int PageNumber { get; set; }
        public int NumberOfPages { get; set; }

        public PagingModel()
        {
            EntitiesPerPage = 10;
        }
    }
}
