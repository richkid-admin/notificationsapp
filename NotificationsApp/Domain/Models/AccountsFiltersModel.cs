﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models
{
    public class AccountsFiltersModel : PagingModel
    {
        public bool IsWithAutomations { get; set; }
        public bool? IsActive { get; set; }

    }

}
