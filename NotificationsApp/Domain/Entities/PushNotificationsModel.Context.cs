﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Domain.Entities
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class PushNotificationsEntities : DbContext
    {
        public PushNotificationsEntities()
            : base("name=PushNotificationsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Package> Package { get; set; }
        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<AccountPackageLog> AccountPackageLog { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<SendAutomation> SendAutomation { get; set; }
        public virtual DbSet<AccountSubscribersUpdateLog> AccountSubscribersUpdateLog { get; set; }
    }
}
