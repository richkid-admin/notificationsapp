﻿using BLL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace NotificationsAppAPI.Controllers
{
    public class MessageController : Controller
    {
        class Message
        {
            public bool Check { get; set; }
        }
       

        [HttpPost]
        public bool CheckIfExists()
        {
            string requestBodyContent = new System.IO.StreamReader(Request.InputStream).ReadToEnd();

            dynamic JsonObj = new JObject();
            JsonObj = JsonConvert.DeserializeObject(requestBodyContent);
            Account account = new Account
            {
                Name = JsonObj.user,
                AccountKey = JsonObj.password
            };



            return true;
        }

        public static async Task<object> PostCallAPI(string url, object jsonObject)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    var content = new StringContent(jsonObject.ToString(), Encoding.UTF8, "application/json");
                    var response = await client.PostAsync(url, content);
                    if (response != null)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<object>(jsonString);
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return new Message { Check = true };
        }


    }
}
