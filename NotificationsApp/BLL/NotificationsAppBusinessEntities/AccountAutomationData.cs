﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class AccountAutomationData
    {
        public bool IsActive { get; set; }
        public AccountAutomationData()
        {
            IsActive = true;
        }
    }

    public enum AutomationTypes
    {
        None,
        AccountSubscribersUpdate
    }
}
