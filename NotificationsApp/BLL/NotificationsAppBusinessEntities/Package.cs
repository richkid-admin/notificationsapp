﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Package
    {
        public int PackageID { get; set; }
        public string Name { get; set; }
        public int MaxSubscribers { get; set; }
        public double Price { get; set; }
        public PackageData Data { get; set; }


        public Package()
        {
            this.Data = new PackageData();
        }

        [DataContract]
        public class PackageData
        {
            public PackageData()
            {
            }
        }
    }

}
