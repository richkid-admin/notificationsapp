﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class SendAutomation
    {
        public int SendAutomationID { get; set; }
        public int AccountID { get; set; }
        public string DisplayName{ get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? LastMessageSentDate { get; set; } 
        public Message TemplateMessage { get; set; }
        public DayOfWeek Day { get; set; }
        public int Hour { get; set; }
        //public GetMessageStrategies GetMessageStrategy { get; set; }
        public AutomationData AutomationData { get; set; } 


        public SendAutomation()
        {
            IsActive = true;
            CreationDate = DateTime.Now;
            AutomationData = new AutomationData();
        }
        // override object.Equals
        public override bool Equals(object obj)
        {

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var otherSendAutomation = (SendAutomation)obj;

            return otherSendAutomation.Day == this.Day && otherSendAutomation.Hour == this.Hour && otherSendAutomation.LastMessageSentDate.GetValueOrDefault().Date.Equals(this.LastMessageSentDate.GetValueOrDefault().Date);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here
            return base.GetHashCode();
        }
    }

    [DataContract]
    public class AutomationData
    {
        public bool HasSentMessages { get; set; }
        public AutomationData()
        {
        }
    }

    public enum SendAutomationStatus
    {
        All,
        NotActive,
        Active,
    }
    public enum AccountStatus
    {
        All,
        NotActive,
        Active,
    }
    public class SendAutomationFilters : SharedModels.PagingModel
    {
        public List<string> AccountsData { get; set; }
        public SendAutomationStatus SendAutomationStatus { get; set; }

    }

}
