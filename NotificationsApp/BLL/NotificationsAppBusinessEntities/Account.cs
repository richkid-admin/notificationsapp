﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Account
    {
        public int AccountID { get; set; }
        public string Name { get; set; }
        public int NumberOfSubscribers { get; set; }
        public string AccountKey { get; set; }
        public bool PaymentApproved { get; set; }
        public bool IsActivated { get; set; }
        public int PackageID { get; set; }
        public DateTime CreationDate { get; set; }
        public AccountData Data { get; set; }

        public Account()
        {
            PackageID = 1; //default 
        }
        [DataContract]
        public class AccountData
        {
            [DataMember]
            public PulseemCredentials Credentials { get; set; }
            [DataMember]
            public List<string> Emails { get; set; }
            [DataMember]
            public string PrimaryDomain { get; set; }
            [DataMember]
            public Dictionary<AutomationTypes, AccountAutomationData> AutomationTypeToData { get; set; }


            public AccountData(string APIKey)
            {
                this.Credentials = new PulseemCredentials(APIKey);
                this.Emails = new List<string>();
                this.AutomationTypeToData = new Dictionary<AutomationTypes, AccountAutomationData>();
            }

            [DataContract]
            public class PulseemCredentials
            {
                [DataMember]
                public string PulseemUsername { get; set; }

                [DataMember]
                public string PulseemPassword { get; set; }

                [DataMember]
                public string PulseemAPIKey { get; set; }

                [DataMember]
                public string PulseemNotificationKey { get; set; }

                public PulseemCredentials()
                {
                    PulseemUsername = string.Empty;
                    PulseemPassword = string.Empty;
                    PulseemAPIKey = string.Empty;
                    PulseemNotificationKey = string.Empty;
                }

                public PulseemCredentials(string APIKey)
                {
                    PulseemUsername = string.Empty;
                    PulseemPassword = string.Empty;
                    PulseemAPIKey = APIKey;
                    PulseemNotificationKey = string.Empty;
                }
            }
        }
    }
    public class AccountsFilters
    {
        //public int Status { get; set; }
        public int Status { get; set; }
        public bool IsWithAutomations { get; set; }
    }
}