﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Globalization;

namespace BLL
{
    [Serializable]
    public class Message
    {
        public int MessageID { get; set; }
        public int AccountID { get; set; }
        public int NotificationID { get; set; }
        public string DisplayName { get; set; }
        public string Headline { get; set; }
        public string MessageText { get; set; }
        public string ImageUrl { get; set; }
        public string ButtonRedirectLink { get; set; }
        public string ButtonText { get; set; }
        public DateTime CreationDate { get; set; }
        public int SentCount { get; set; }
        public int ClickCount { get; set; }
        public int ReceivedCount { get; set; }
        public string Status { get; set; }
        public int? SendAutomationID { get; set; }

        public MessageData Data { get; set; }

        //TODO: Move configurations to separate config file for entire system in common
        public static int MAX_REDIRECT_LINK_LENGTH { get; } = 500;
        public static int MAX_MESSAGE_HEADLINE_LENGTH { get; } = 50;
        public static int MAX_MESSAGE_TEXT_LENGTH { get; } = 100;


        public Message()
        {
            this.Data = new MessageData();
            Status = "Fail";
            CreationDate = DateTime.Now;
        }

        [DataContract]
        [Serializable]
        public class MessageData
        {
            [DataMember]
            public string LogoUrl { get; set; }
            public MessageData()
            {
                
            }
        }
    }

}