﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.SharedModels
{
    public class PagingModel
    {
        public static int EntitiesPerPage = 10;
        public int PageNumber { get; set; }
        public int NumberOfPages { get; set; }
    }
}
