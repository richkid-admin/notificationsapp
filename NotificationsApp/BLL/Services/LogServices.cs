﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class LogServices
    {
        public static void WriteLog(string Folder, string FileName, string DataToWrite)
        {
            try
            {
                string path = "C:/Inetpub/vhosts/notify.apps.getmood.io/httpdocs/Logs/" + Folder + "/";
                //string path = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\LogTests\"; //Tom local
                var fullPath = path + FileName + " - " + DateTime.Now.ToString("dd-MM-yyyy HH-mm") + ".txt";


                bool exists = System.IO.Directory.Exists(path);

                if (!exists)
                    System.IO.Directory.CreateDirectory(path);

                // Create a new file 
                using (StreamWriter sw = System.IO.File.CreateText(fullPath))
                {
                    sw.WriteLine(DataToWrite);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
