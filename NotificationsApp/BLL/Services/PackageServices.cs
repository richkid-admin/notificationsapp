﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class PackageServices
    {
        public static BLL.Package LoadPackageByID(int packageID)
        {
            BLL.Package BLLPackage = new Package();
            Domain.Entities.Package entityPackage = new Domain.Entities.Package();

            try
            {
                using (Domain.Repositories.PackageRepository packageRepository = new Domain.Repositories.PackageRepository())
                {
                    entityPackage = packageRepository.LoadPackageByID(packageID);
                }

                BLLPackage = entityPackage != null ? ConvertPackageEntityToBusiness(entityPackage) : new Package();
            }
            catch (Exception ex)
            {
                BLLPackage = new Package();
            }

            return BLLPackage;
        }


        public static List<BLL.Package> LoadAllPackages()
        {
            List<BLL.Package> listOfPackages = new List<Package>();
            List<Domain.Entities.Package> entityPackages = new List<Domain.Entities.Package>();

            try
            {
                using (Domain.Repositories.PackageRepository packageRepository = new Domain.Repositories.PackageRepository())
                {
                    entityPackages = packageRepository.LoadAllPackages();
                }
                if (entityPackages != null && entityPackages.Count > 0)
                {

                    foreach (Domain.Entities.Package entityPackage in entityPackages)
                    {
                        listOfPackages.Add(ConvertPackageEntityToBusiness(entityPackage));
                    }
                }
            }
            catch (Exception ex)
            {
                listOfPackages = new List<BLL.Package>();
            }

            return listOfPackages;
        }


        public static int Save(Package package)
        {
            Domain.Entities.Package entityPackage = ConvertPackageBusinessToEntity(package);

            try
            {
                using (Domain.Repositories.PackageRepository packageRepository = new Domain.Repositories.PackageRepository())
                {
                    package.PackageID = packageRepository.Save(entityPackage);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

            return package.PackageID;
        }


        public static bool Delete(int packageID)
        {
            bool success = true;

            try
            {
                using (Domain.Repositories.PackageRepository packageRepository = new Domain.Repositories.PackageRepository())
                {
                    Domain.Entities.Package entityPackage = packageRepository.LoadPackageByID(packageID);

                    if (entityPackage != null)
                    {
                        packageRepository.Delete(entityPackage);
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;
        }

        public static BLL.Package ConvertPackageEntityToBusiness(Domain.Entities.Package entityPackage)
        {
            BLL.Package BLLPackage = new Package();

            if (entityPackage == null)
            {
                return new Package();
            }

            BLLPackage.PackageID = entityPackage.PackageID;
            BLLPackage.MaxSubscribers = (int)entityPackage.MaxSubscribers;
            BLLPackage.Name = entityPackage.Name;
            BLLPackage.Price = (double)entityPackage.Price;

            BLLPackage.Data = JsonConvert.DeserializeObject<BLL.Package.PackageData>(entityPackage.Data);

            return BLLPackage;
        }

        public static Domain.Entities.Package ConvertPackageBusinessToEntity(BLL.Package BLLPackage)
        {
            Domain.Entities.Package entityPackage = new Domain.Entities.Package();

            if (BLLPackage == null)
            {
                return new Domain.Entities.Package();
            }

            entityPackage.PackageID = BLLPackage.PackageID;
            entityPackage.MaxSubscribers = BLLPackage.MaxSubscribers;
            entityPackage.Name = BLLPackage.Name;
            entityPackage.Price = BLLPackage.Price;

            entityPackage.Data = JsonConvert.SerializeObject(BLLPackage.Data);

            return entityPackage;
        }


        // TODO: relevant??
        public static Package CreatePackage(string packageName, int maxSubscribers, double price)
        {
            try
            {
                BLL.Package package = new Package
                {
                    Name = packageName,
                    MaxSubscribers = maxSubscribers,
                    Price = price
                };

                package.PackageID = Save(package);

                return package.PackageID > 0 ? package : null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static AccountPackageLog UpgradePackage(int newPackageID, Account account)
        {
            int packageToUpgradeID = 0;
            var currentPackageID = account.PackageID;
            packageToUpgradeID = LoadPackageByID(newPackageID).PackageID;

            if (!AccountServices.IsAccountActive(account))
            {
                throw new HttpRequestException(HttpStatusCode.MethodNotAllowed.ToString());
            }
            else
            {
                if (packageToUpgradeID == 0) // upgrade package not exsits
                {
                    throw new HttpRequestException(HttpStatusCode.NotFound.ToString());
                }
                else
                {
                    account.PackageID = packageToUpgradeID;
                    account.AccountID = AccountServices.Save(account);

                    if (account.AccountID == 0)
                    {
                        throw new HttpRequestException(HttpStatusCode.NoContent.ToString());
                    }
                    else
                    {
                        AccountPackageLog accountPackageLog = new AccountPackageLog
                        {
                            AccountID = account.AccountID,
                            CurrentPackageID = currentPackageID,
                            NewPackageID = account.PackageID,
                            TimeOfUpgrade = DateTime.Now
                        };

                        accountPackageLog.AccountPackageLogID = AccountPackageLogServices.Save(accountPackageLog);

                        if (accountPackageLog.AccountPackageLogID == 0)
                        {
                            throw new HttpRequestException(HttpStatusCode.InternalServerError.ToString());
                        }
                        else
                        {
                            return accountPackageLog;
                        }
                    }
                }
            }
        }
    }
}
