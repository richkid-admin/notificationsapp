﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class AccountPackageLogServices
    {
        public static AccountPackageLog LoadAccountPackageLogByAccountPackageLogID(int accountPackageLogID)
        {
            BLL.AccountPackageLog accountPackageLog = new AccountPackageLog();
            Domain.Entities.AccountPackageLog entityAccountPackageLog = new Domain.Entities.AccountPackageLog();

            try
            {
                using (Domain.Repositories.AccountPackageLogRepository accountPackageLogRepositoryRepository = new Domain.Repositories.AccountPackageLogRepository())
                {
                    entityAccountPackageLog = accountPackageLogRepositoryRepository.LoadAccountPackageLogByAccountPackageLogID(accountPackageLogID);
                }

                accountPackageLog = ConvertAccountPackageLogEntityToBusiness(entityAccountPackageLog);

            }
            catch (Exception ex)
            {
                accountPackageLog = new BLL.AccountPackageLog();
            }

            return accountPackageLog;
        }

        public static List<BLL.AccountPackageLog> LoadAccountPackageLogsByAccountID(int accountID)
        {
            List<BLL.AccountPackageLog> accountPackageLogs = new List<BLL.AccountPackageLog>();
            List<Domain.Entities.AccountPackageLog> entityAccountPackageLogs = new List<Domain.Entities.AccountPackageLog>();

            try
            {
                using (Domain.Repositories.AccountPackageLogRepository accountPackageLogRepositoryRepository = new Domain.Repositories.AccountPackageLogRepository())
                {
                    entityAccountPackageLogs = accountPackageLogRepositoryRepository.LoadAccountPackageLogsByAccountID(accountID);
                }

                if (entityAccountPackageLogs != null && entityAccountPackageLogs.Count > 0)
                {
                    foreach (var entityAccountPackageLog in entityAccountPackageLogs)
                    {
                        accountPackageLogs.Add(ConvertAccountPackageLogEntityToBusiness(entityAccountPackageLog));
                    }
                }
            }
            catch (Exception ex)
            {
                accountPackageLogs = new List<BLL.AccountPackageLog>();
            }

            return accountPackageLogs;
        }

        public static BLL.AccountPackageLog ConvertAccountPackageLogEntityToBusiness(Domain.Entities.AccountPackageLog entityAccountPackageLog)
        {
            BLL.AccountPackageLog accountPackageLog = new AccountPackageLog();

            if (entityAccountPackageLog == null)
            {
                return null;
            }

            accountPackageLog.AccountPackageLogID = entityAccountPackageLog.AccountPackageLogID;
            accountPackageLog.AccountID = entityAccountPackageLog.AccountID;
            accountPackageLog.CurrentPackageID = entityAccountPackageLog.CurrentPackageID;
            accountPackageLog.NewPackageID = entityAccountPackageLog.NewPackageID;
            accountPackageLog.TimeOfUpgrade = entityAccountPackageLog.TimeOfUpgrade;
            accountPackageLog.IsCharged = (bool)entityAccountPackageLog.IsCharged;

            return accountPackageLog;
        }

        public static Domain.Entities.AccountPackageLog ConvertAccountPackageLogBusinessToEntity(BLL.AccountPackageLog accountPackageLog)
        {
            Domain.Entities.AccountPackageLog entityAccountPackageLog = new Domain.Entities.AccountPackageLog();

            if (accountPackageLog == null)
            {
                return null;
            }

            entityAccountPackageLog.AccountPackageLogID = accountPackageLog.AccountPackageLogID;
            entityAccountPackageLog.AccountID = accountPackageLog.AccountID;
            entityAccountPackageLog.CurrentPackageID = accountPackageLog.CurrentPackageID;
            entityAccountPackageLog.NewPackageID = accountPackageLog.NewPackageID;
            entityAccountPackageLog.TimeOfUpgrade = accountPackageLog.TimeOfUpgrade;
            entityAccountPackageLog.IsCharged = accountPackageLog.IsCharged;
            return entityAccountPackageLog;
        }

        public static bool SaveListOfAccountsPackageLogs(List<AccountPackageLog> accountsPackageLogs)
        {
           

            try
            {
                using (Domain.Repositories.AccountPackageLogRepository accountPackageLogRepository = new Domain.Repositories.AccountPackageLogRepository())
                {
                    foreach(var accountPackageLog in accountsPackageLogs)
                    {
                        Domain.Entities.AccountPackageLog entityAccountPackageLog = ConvertAccountPackageLogBusinessToEntity(accountPackageLog);
                        accountPackageLog.AccountPackageLogID = accountPackageLogRepository.Save(entityAccountPackageLog);
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public static int Save(BLL.AccountPackageLog accountPackageLog)
        {
            Domain.Entities.AccountPackageLog entityAccountPackageLog = ConvertAccountPackageLogBusinessToEntity(accountPackageLog);

            try
            {
                using (Domain.Repositories.AccountPackageLogRepository accountPackageLogRepository = new Domain.Repositories.AccountPackageLogRepository())
                {
                    accountPackageLog.AccountPackageLogID = accountPackageLogRepository.Save(entityAccountPackageLog);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return accountPackageLog.AccountPackageLogID;
        }
    }
}
