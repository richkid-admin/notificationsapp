﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class MessageServices
    {
        public static BLL.Message LoadMessageByID(int messageID)
        {
            BLL.Message BLLMessage = new Message();
            Domain.Entities.Message entityMessage = new Domain.Entities.Message();

            try
            {

                using (Domain.Repositories.MessageRepository messageRepository = new Domain.Repositories.MessageRepository())
                {
                    entityMessage = messageRepository.LoadMessageByID(messageID);
                }

                BLLMessage = entityMessage != null ? ConvertMessageEntityToBusiness(entityMessage) : new Message();
            }
            catch (Exception ex)
            {
                BLLMessage = new Message();
            }

            return BLLMessage;
        }

        public static List<Message> LoadAllMessagesByAccountKey(string accountKey)
        {
            List<BLL.Message> listOfMessages = new List<Message>();
            List<Domain.Entities.Message> entityMessages = new List<Domain.Entities.Message>();

            try
            {
                using (Domain.Repositories.MessageRepository messageRepository = new Domain.Repositories.MessageRepository())
                {
                    entityMessages = messageRepository.LoadAllMessagesByAccountKey(accountKey);
                }
                if (entityMessages != null && entityMessages.Count > 0)
                {
                    foreach (Domain.Entities.Message entityMessage in entityMessages)
                    {
                        listOfMessages.Add(ConvertMessageEntityToBusiness(entityMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                listOfMessages = new List<BLL.Message>();
            }

            return listOfMessages;
        }

        public static List<BLL.Message> LoadAllMessages()
        {
            List<BLL.Message> listOfMessages = new List<Message>();
            List<Domain.Entities.Message> entityMessages = new List<Domain.Entities.Message>();

            try
            {
                using (Domain.Repositories.MessageRepository messageRepository = new Domain.Repositories.MessageRepository())
                {
                    entityMessages = messageRepository.LoadAllMessages();
                }
                if (entityMessages != null && entityMessages.Count > 0)
                {
                    foreach (Domain.Entities.Message entityMessage in entityMessages)
                    {
                        listOfMessages.Add(ConvertMessageEntityToBusiness(entityMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                listOfMessages = new List<BLL.Message>();
            }

            return listOfMessages;
        }


        public static bool ValidateInputAndPopulateMessage(dynamic tempMessage, Message message, int numberOfSubscribers)
        {
            // Required fields -> Message Display Name, Message Headline, Message Text -> Can not be null or empty
            // Validate required fields and populate message fields

            // required fields - if field is null or value is null or empty ("") -> Return false
            if (tempMessage.DisplayName != null && !string.IsNullOrWhiteSpace(tempMessage.DisplayName.Value))
            {
                message.DisplayName = tempMessage.DisplayName.Value;
            }
            else
            {
                return false;
            }
            if (tempMessage.Headline != null && !string.IsNullOrWhiteSpace(tempMessage.Headline.Value))
            {
                message.Headline = tempMessage.Headline.Value;
            }
            else
            {
                return false;
            }
            if (tempMessage.MessageText != null && !string.IsNullOrWhiteSpace(tempMessage.MessageText.Value))
            {
                message.MessageText = tempMessage.MessageText.Value;
            }
            else
            {
                return false;
            }
            message.ButtonRedirectLink = tempMessage.ButtonRedirectLink != null && !string.IsNullOrEmpty(tempMessage.ButtonRedirectLink.Value) ? tempMessage.ButtonRedirectLink.Value : "";
            if (message.ButtonRedirectLink.Length >= BLL.Message.MAX_REDIRECT_LINK_LENGTH)
            {
                return false;
            }

            message.ButtonText = tempMessage.ButtonText != null && !string.IsNullOrEmpty(tempMessage.ButtonText.Value) ? tempMessage.ButtonText.Value : "";
            message.ButtonRedirectLink = tempMessage.ButtonRedirectLink != null && !string.IsNullOrEmpty(tempMessage.ButtonRedirectLink.Value) ? tempMessage.ButtonRedirectLink.Value : "";
            if(tempMessage.Image != null)
            {
                message.ImageUrl = tempMessage.Image.ToString();
            }
            if (tempMessage.Logo != null)
            {
                message.Data.LogoUrl = tempMessage.Logo.ToString();
            }

            return true;
        }

        public static int Save(Message message)
        {
            Domain.Entities.Message entityMessage = ConvertMessageBusinessToEntity(message);

            try
            {
                using (Domain.Repositories.MessageRepository messageRepository = new Domain.Repositories.MessageRepository())
                {
                    message.MessageID = messageRepository.Save(entityMessage);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

            return message.MessageID;
        }

        public static void UpdateStatistics(List<Message> messagesBLLToUpdate)
        {
            //notificationID must have been allocated by postal services and can also be used to identify the message in our system. 
            //Dictionary<int, Domain.Entities.Message> notificationIDToEntity = messagesBLLToUpdate.ToDictionary(msg => msg.NotificationID, msg => ConvertMessageBusinessToEntity(msg));

            //ignores duplicate notificationIDs if exist
            Dictionary<int, Domain.Entities.Message> notificationIDToEntity = messagesBLLToUpdate
                  .GroupBy(m => m.NotificationID)
                  .Select(m => m.First())
                  .ToDictionary(m => m.NotificationID, m => ConvertMessageBusinessToEntity(m));
            using (Domain.Repositories.MessageRepository msgRepo = new Domain.Repositories.MessageRepository())
            {
                msgRepo.UpdateStatistics(notificationIDToEntity);
            }
        }

        public static bool Delete(int messageID)
        {
            bool success = true;

            try
            {
                using (Domain.Repositories.MessageRepository messageRepository = new Domain.Repositories.MessageRepository())
                {
                    Domain.Entities.Message entityMessage = messageRepository.LoadMessageByID(messageID);
                    if (entityMessage != null)
                    {
                        messageRepository.Delete(entityMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                success = false;
            }

            return success;
        }


        //public static bool CopyMessageFields(BLL.Message source, BLL.Message target)
        //{
        //    bool succeeded = true;

        //    try
        //    {
        //        target.ClickCount = source.ClickCount;
        //        target.SentCount = source.SentCount;
        //        target.ReceivedCount = source.ReceivedCount;
        //        target.Status = source.Status;
        //    }
        //    catch (Exception ex)
        //    {
        //        succeeded = false;
        //    }

        //    return succeeded;
        //}

        public static BLL.Message ConvertMessageEntityToBusiness(Domain.Entities.Message entityMessage)
        {
            BLL.Message BLLMessage = new Message();

            if (entityMessage == null)
            {
                return new Message();
            }

            BLLMessage.AccountID = entityMessage.AccountID;
            BLLMessage.MessageID = entityMessage.MessageID;
            BLLMessage.ButtonText = entityMessage.ButtonText;
            BLLMessage.ButtonRedirectLink = entityMessage.ButtonRedirectLink;
            BLLMessage.DisplayName = entityMessage.DisplayName;
            BLLMessage.Headline = entityMessage.Headline;
            BLLMessage.Status = entityMessage.Status;
            BLLMessage.ImageUrl = entityMessage.ImageUrl;
            BLLMessage.MessageText = entityMessage.MessageText;
            BLLMessage.CreationDate = entityMessage.CreationDate;
            BLLMessage.SentCount = (int)entityMessage.SentCount;
            BLLMessage.ClickCount = (int)entityMessage.ClickCount;
            BLLMessage.ReceivedCount = (int)entityMessage.ReceivedCount;
            BLLMessage.NotificationID = entityMessage.NotificationID;
            BLLMessage.SendAutomationID = entityMessage.SendAutomationID;

            BLLMessage.Data = JsonConvert.DeserializeObject<BLL.Message.MessageData>(entityMessage.Data);

            return BLLMessage;
        }

        public static Domain.Entities.Message ConvertMessageBusinessToEntity(BLL.Message BLLMessage)
        {
            Domain.Entities.Message entityMessage = new Domain.Entities.Message();

            if (BLLMessage == null)
            {
                return new Domain.Entities.Message();
            }

            entityMessage.AccountID = BLLMessage.AccountID;
            entityMessage.MessageID = BLLMessage.MessageID;
            entityMessage.ButtonText = BLLMessage.ButtonText;
            entityMessage.ButtonRedirectLink = BLLMessage.ButtonRedirectLink;
            entityMessage.DisplayName = BLLMessage.DisplayName;
            entityMessage.Headline = BLLMessage.Headline;
            entityMessage.Status = BLLMessage.Status;
            entityMessage.ImageUrl = BLLMessage.ImageUrl;
            entityMessage.MessageText = BLLMessage.MessageText;
            entityMessage.CreationDate = BLLMessage.CreationDate;
            entityMessage.SentCount = BLLMessage.SentCount;
            entityMessage.ClickCount = BLLMessage.ClickCount;
            entityMessage.ReceivedCount = BLLMessage.ReceivedCount;
            entityMessage.LastUpdateDate = DateTime.Now;
            entityMessage.NotificationID = BLLMessage.NotificationID;
            entityMessage.SendAutomationID = BLLMessage.SendAutomationID;

            entityMessage.Data = JsonConvert.SerializeObject(BLLMessage.Data);


            return entityMessage;
        }

        #region GenerateNewMessage - Currently not in use
        //public static Message GenerateNewMessage(int accountID, string displayName, string headline, string messageText, string buttonText, string redirectLink /*,Image image*/,int notificationID = -1)
        //{
        //    // TODO: is this method relevant for something? Create new message is in PostMessage method of the API
        //    try
        //    {
        //        Message message = new Message
        //        {
        //            AccountID = accountID,
        //            DisplayName = displayName,
        //            Headline = headline,
        //            MessageText = messageText,
        //            ButtonText = buttonText,
        //            ButtonRedirectLink = redirectLink,
        //            CreationDate = DateTime.Now,
        //            NotificationID = notificationID
        //            //TODO: take care of save 
        //            //Data??
        //        };

        //        message.MessageID = Save(message);

        //        return message.MessageID > 0 ? message : null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        #endregion

        public static List<BLL.Message> MessageArchive(int pageNum = 1, int itemPerPage = 100)
        {
            List<BLL.Message> archiveMessages = new List<Message>();
            List<Domain.Entities.Message> entityArchiveMessages = new List<Domain.Entities.Message>();

            try
            {
                using (Domain.Repositories.MessageRepository messageRepository = new Domain.Repositories.MessageRepository())
                {
                    entityArchiveMessages = messageRepository.LoadArchiveMessagesWithPaging(pageNum, itemPerPage);
                }
                if (entityArchiveMessages != null && entityArchiveMessages.Count > 0)
                {
                    foreach (Domain.Entities.Message entityArchiveMessage in entityArchiveMessages)
                    {
                        archiveMessages.Add(ConvertMessageEntityToBusiness(entityArchiveMessage));
                    }
                }
            }
            catch (Exception ex)
            {
                archiveMessages = new List<BLL.Message>();
            }

            return archiveMessages;
        }
        public static Dictionary<int, List<int>> GetMessageAccountToNotificationIDsFrom(DateTime earliestTimeInInterval, bool successfullySentOnly = true)
        {
            Dictionary<int, List<int>> messageAccountToNotificationIDs = new Dictionary<int, List<int>>();
            try
            {
                using (Domain.Repositories.MessageRepository msgRepo = new Domain.Repositories.MessageRepository())
                {
                    messageAccountToNotificationIDs = msgRepo.GetMessageIDsStartingFrom(earliestTimeInInterval, successfullySentOnly);
                }
                return messageAccountToNotificationIDs;
            }
            catch (Exception ex)
            {
                return messageAccountToNotificationIDs;
            }
        }

    }
}
