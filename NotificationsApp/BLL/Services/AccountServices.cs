﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLL.Services
{
    public class AccountServices
    {
        public static BLL.Account LoadAccountByID(int accountID)
        {
            BLL.Account BLLAccount = new Account();
            Domain.Entities.Account entityAccount = new Domain.Entities.Account();

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    entityAccount = accountRepository.LoadAccountByID(accountID);
                }

                BLLAccount = entityAccount != null ? ConvertAccountEntityToBusiness(entityAccount) : new Account();

            }
            catch (Exception ex)
            {
                BLLAccount = new Account();
            }
            return BLLAccount;
        }

        public static int LoadAccountIDByAccountKey(string accountKey)
        {
            int accountID = 0;

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    accountID = accountRepository.LoadAccountIDByAccountKey(accountKey);
                }                
            }
            catch (Exception ex)
            {
                accountID = 0;
            }

            return accountID;
        }

        public static BLL.Account LoadAccountByAccountKey(string accountKey)
        {
            BLL.Account BLLAccount = new Account();
            Domain.Entities.Account entityAccount = new Domain.Entities.Account();

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    entityAccount = accountRepository.LoadAccountByAccountKey(accountKey);
                }
                BLLAccount = entityAccount != null ? ConvertAccountEntityToBusiness(entityAccount) : new Account();
            }
            catch (Exception ex)
            {
                return null;
            }

            return BLLAccount;
        }

        public static List<BLL.Account> LoadAllAccounts()
        {
            List<BLL.Account> listOfAccounts = new List<Account>();
            List<Domain.Entities.Account> entityAccounts = new List<Domain.Entities.Account>();

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    entityAccounts = accountRepository.LoadAllAccounts();
                }

                if (entityAccounts != null && entityAccounts.Count > 0)
                {
                    foreach (var entityAccount in entityAccounts)
                    {
                        listOfAccounts.Add(ConvertAccountEntityToBusiness(entityAccount));
                    }
                }
            }
            catch (Exception ex)
            {
                listOfAccounts = new List<Account>();
            }

            return listOfAccounts;
        }

        public static void UpdateStatistics(Dictionary<int, int> AccountIDToSubscribersCount)
        {
            using (Domain.Repositories.AccountRepository accRepo = new Domain.Repositories.AccountRepository())
            {
                accRepo.UpdateStatistics(AccountIDToSubscribersCount);
            }
        }

        public static BLL.Account ConvertAccountEntityToBusiness(Domain.Entities.Account entityAccount)
        {
            BLL.Account BLLAccount = new Account();

            if (entityAccount == null)
            {
                return null;
            }

            BLLAccount.AccountKey = entityAccount.AccountKey;
            BLLAccount.AccountID = entityAccount.AccountID;
            BLLAccount.Name = entityAccount.Name;
            BLLAccount.NumberOfSubscribers = (int)entityAccount.NumberOfSubscribers;
            BLLAccount.PackageID = entityAccount.PackageID;
            BLLAccount.CreationDate = entityAccount.CreationDate;
            BLLAccount.PaymentApproved = entityAccount.PaymentApproved;

            BLLAccount.Data = JsonConvert.DeserializeObject<BLL.Account.AccountData>(entityAccount.Data);

            return BLLAccount;
        }

        public static Domain.Entities.Account ConvertAccountBusinessToEntity(BLL.Account BLLAccount)
        {
            Domain.Entities.Account entityAccount = new Domain.Entities.Account();

            if (BLLAccount == null)
            {
                return null;
            }

            entityAccount.AccountKey = BLLAccount.AccountKey;
            entityAccount.AccountID = BLLAccount.AccountID;
            entityAccount.Name = BLLAccount.Name;
            entityAccount.NumberOfSubscribers = BLLAccount.NumberOfSubscribers;
            entityAccount.PackageID = BLLAccount.PackageID;
            entityAccount.CreationDate = BLLAccount.CreationDate;
            entityAccount.PaymentApproved = BLLAccount.PaymentApproved;

            entityAccount.Data = JsonConvert.SerializeObject(BLLAccount.Data);

            return entityAccount;
        }


        public static int Save(BLL.Account account)
        {
            Domain.Entities.Account entityAccount = ConvertAccountBusinessToEntity(account);

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    account.AccountID = accountRepository.Save(entityAccount);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

            return account.AccountID;
        }

        public static bool Delete(int accountID)
        {
            bool success = true;

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    Domain.Entities.Account entityAccount = accountRepository.LoadAccountByID(accountID);
                    if (entityAccount != null)
                    {
                        accountRepository.Delete(entityAccount);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
                //success = false;
            }

            return success;
        }



        public static bool CheckIfAccountExistByAccountKey(string accountKey)
        {
            bool isExists = false;

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    isExists = accountRepository.CheckIfAccountExistByAccountKey(accountKey);
                }
            }
            catch (Exception ex)
            {
                isExists = false;
            }

            return isExists;
        }
        public static List<BLL.Account> LoadAccountsByIds(ICollection<int> accountIDs)
        {
            List<BLL.Account> listOfAccounts = new List<Account>();
            List<Domain.Entities.Account> entityAccounts = new List<Domain.Entities.Account>();

            try
            {
                using (Domain.Repositories.AccountRepository accountRepository = new Domain.Repositories.AccountRepository())
                {
                    entityAccounts = accountRepository.LoadAccountsByIds(accountIDs);
                }

                if (entityAccounts != null && entityAccounts.Count > 0)
                {
                    foreach (var entityAccount in entityAccounts)
                    {
                        listOfAccounts.Add(ConvertAccountEntityToBusiness(entityAccount));
                    }
                }
            }
            catch (Exception ex)
            {
                listOfAccounts = new List<Account>();
            }

            return listOfAccounts;
        }


        public static bool IsAccountActive(Account account)
        {
            return account.PaymentApproved || account.NumberOfSubscribers < 200;
        }
        public static bool IsSendingAllowed(BLL.Account account, BLL.Package package)
        {
            // Check conditions to send message:
            // 1. Active account => Number of subscribers <= 200 OR payment approved by Mor/Chen
            // 2. Number of subscribers <= Max Subscribers of current package of the account
            return account != null && package != null && account.NumberOfSubscribers <= package.MaxSubscribers && IsAccountActive(account);
        }
    }
}