﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class AccountSubscribersUpdateLog
    {
        public int AccountSubscribersUpdateLogID { get; set; }
        public int AccountID { get; set; }
        public int CurrentNumberOfSubscribers { get; set; }
        public int NumberOfSubscribersStep { get; set; }
        public DateTime UpdateSendDate { get; set; }
        public bool IsSuccessful { get; set; }
        public AccountSubscribersUpdateLogData Data { get; set; }
        public AccountSubscribersUpdateLog()
        {
            Data = new AccountSubscribersUpdateLogData();
        }
    }
    public class AccountSubscribersUpdateLogData
    {
        public List<string> Emails { get; set; }
        public string Errors { get; set; }

        public AccountSubscribersUpdateLogData()
        {
            Emails = new List<string>();
        }
    }
}
