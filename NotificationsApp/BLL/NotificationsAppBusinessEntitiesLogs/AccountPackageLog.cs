﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class AccountPackageLog
    {
        public int AccountPackageLogID { get; set; }
        public int AccountID { get; set; }
        public int CurrentPackageID { get; set; }
        public int NewPackageID { get; set; }
        public bool IsCharged { get; set; }
        public DateTime TimeOfUpgrade { get; set; }
    }
}
