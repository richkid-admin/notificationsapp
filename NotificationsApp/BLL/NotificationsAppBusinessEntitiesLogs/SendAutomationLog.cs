﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public enum SendAutomationWorkerIterationStatus
    {
        NoAutomationsLoaded,
        NoAccountsLoaded,
        NoPackagesLoaded,
        MessagesSent
    }
    public enum SendAutomationIterationStatus
    {
        AccountAndPackageLoaded,
        MessageCopied,
        MessageSent,
        Success 
    }
    public class SendAutomationIterationLog
    {
        public SendAutomationWorkerIterationStatus SendAutomationIterationStatus { get; set; }
        public HashSet<int> SendAutomationLogIDs { get; set; } //currently saved in file and has automationIDs
        public int MessagesSent { get; set; }
        public string Exception { get; set; }

        public SendAutomationIterationLog()
        {
            SendAutomationLogIDs = new HashSet<int>();
        }


    }
    public class SendAutomationLog
    {
        public int AutomationID { get; set; }
        public int AccountID { get; set; }
        public int PackageID { get; set; }
        public int MessageID { get; set; }
        public string Exception { get; set; }
        public SendAutomationIterationStatus SendAutomationStatus { get; set; }
    }
}
