﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{

    public enum PostMessageStatus
    {
        OnEntry,
        SendingNotAllowed,
        MessageNotValidatedAndCopied,
        MessageNotSavedBeforeSend,
        MessageNotSent,
        MessageNotSavedAfterSend,
        Success
    }
    public class PostMessageLog
    {
        public int PostMessageLogID { get; set; }
        public int AccountID { get; set; }
        public int PackageID { get; set; }
        public bool IsSendingAllowed { get; set; }
        public dynamic InMessage { get; set; }
        public BLL.Message OutMessage { get; set; }
        public PostMessageStatus PostMessageStatus { get; set; }
        public string StatusDescription { get; set; }

    }
}
