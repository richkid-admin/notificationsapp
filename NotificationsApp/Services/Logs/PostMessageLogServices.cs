﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PostMessageLogServices
    {
        public static int Save(BLL.PostMessageLog postMessageLog)
        {
            string sLog = Newtonsoft.Json.JsonConvert.SerializeObject(postMessageLog);
            string automationPrefix = postMessageLog.OutMessage.SendAutomationID.GetValueOrDefault() == 0 ? string.Empty : ("Automation_" + postMessageLog.OutMessage.SendAutomationID.ToString() + "_");
            string logFileName = automationPrefix + "Message_" + postMessageLog.OutMessage.MessageID.ToString();
            BLL.Services.LogServices.WriteLog("PushNotificationLogs/PostMessageLogs", logFileName, sLog);
            return postMessageLog.OutMessage.MessageID;
        }
    }
}
