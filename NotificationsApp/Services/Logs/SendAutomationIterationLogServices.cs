﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class SendAutomationIterationLogServices
    {
        public static int Save(BLL.SendAutomationIterationLog sendAutomationIterationLog)
        {
            string sLog = Newtonsoft.Json.JsonConvert.SerializeObject(sendAutomationIterationLog);
            string logFileName = "SendAutomationIterationLog";
            BLL.Services.LogServices.WriteLog("PushNotificationLogs/SendAutomationLogs", logFileName, sLog);
            return 0;
        }
    }
}
