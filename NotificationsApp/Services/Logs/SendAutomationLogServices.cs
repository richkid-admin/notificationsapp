﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class SendAutomationLogServices
    {
        public static int Save(BLL.SendAutomationLog sendAutomationLog)
        {
            string sLog = Newtonsoft.Json.JsonConvert.SerializeObject(sendAutomationLog);
            string logFileName = "AutomationID_" + sendAutomationLog.AutomationID.ToString();
            BLL.Services.LogServices.WriteLog("PushNotificationLogs/SendAutomationLogs", logFileName, sLog);
            return sendAutomationLog.AutomationID;
        }
    }
}
