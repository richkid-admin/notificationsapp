﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;
using Domain.Entities;
using Common;

namespace Services
{
    public class AccountSubscribersUpdateLogServices
    {
        public static Dictionary<int,BLL.AccountSubscribersUpdateLog> LoadLastLogForAccounts()
        {
            Dictionary<int, BLL.AccountSubscribersUpdateLog> accountSubscribersUpdateLogBLL = new Dictionary<int, BLL.AccountSubscribersUpdateLog>();
           var accountSubscribersUpdateLogsEntity = new List<Domain.Entities.AccountSubscribersUpdateLog>();
            try
            {
                using (var accountSubscribersUpdateLogRepo = new Domain.Repositories.AccountSubscribersUpdateLogRepository())
                {
                    accountSubscribersUpdateLogsEntity = accountSubscribersUpdateLogRepo.LoadLastLogForAllAccounts();
                }

                accountSubscribersUpdateLogBLL =  accountSubscribersUpdateLogsEntity
                    .Select(e => ConvertAccountEntityToBusiness(e))
                    .ToDictionary(e => e.AccountID, e => e);

            }
            catch (Exception ex)
            {
            }
            return accountSubscribersUpdateLogBLL;
        }
        public static int Save(BLL.AccountSubscribersUpdateLog logBLL)
        {
            var logEntity = ConvertAccountBusinessToEntity(logBLL);

            try
            {
                using (var accountSubscribersUpdateLogRepo = new Domain.Repositories.AccountSubscribersUpdateLogRepository())
                {
                    logEntity.AccountSubscribersUpdateLogID = accountSubscribersUpdateLogRepo.Save(logEntity);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }

            return logEntity.AccountSubscribersUpdateLogID;
        }

        public static BLL.AccountSubscribersUpdateLog ConvertAccountEntityToBusiness(Domain.Entities.AccountSubscribersUpdateLog accountSubscribersUpdateLogEntity)
        {
            var accountSubscribersUpdateLogBLL = new BLL.AccountSubscribersUpdateLog();

            if (accountSubscribersUpdateLogEntity == null)
            {
                return null;
            }

            accountSubscribersUpdateLogBLL.AccountSubscribersUpdateLogID = accountSubscribersUpdateLogEntity.AccountSubscribersUpdateLogID;
            accountSubscribersUpdateLogBLL.AccountID = accountSubscribersUpdateLogEntity.AccountID;
            accountSubscribersUpdateLogBLL.CurrentNumberOfSubscribers = accountSubscribersUpdateLogEntity.CurrentNumberOfSubscribers;
            accountSubscribersUpdateLogBLL.NumberOfSubscribersStep = accountSubscribersUpdateLogEntity.NumberOfSubscribersStep;
            accountSubscribersUpdateLogBLL.UpdateSendDate = accountSubscribersUpdateLogEntity.UpdateSendDate;
            accountSubscribersUpdateLogBLL.IsSuccessful = accountSubscribersUpdateLogEntity.IsSuccessful;
            accountSubscribersUpdateLogBLL.Data = Newtonsoft.Json.JsonConvert.DeserializeObject<AccountSubscribersUpdateLogData>(accountSubscribersUpdateLogEntity.Data);

            return accountSubscribersUpdateLogBLL;
        }

        public static Domain.Entities.AccountSubscribersUpdateLog ConvertAccountBusinessToEntity(BLL.AccountSubscribersUpdateLog accountSubscribersUpdateLogBLL)
        {
            var accountSubscribersUpdateLogEntity = new Domain.Entities.AccountSubscribersUpdateLog();

            if (accountSubscribersUpdateLogBLL == null)
            {
                return null;
            }
            accountSubscribersUpdateLogEntity.AccountSubscribersUpdateLogID = accountSubscribersUpdateLogBLL.AccountSubscribersUpdateLogID;
            accountSubscribersUpdateLogEntity.AccountID = accountSubscribersUpdateLogBLL.AccountID;
            accountSubscribersUpdateLogEntity.CurrentNumberOfSubscribers = accountSubscribersUpdateLogBLL.CurrentNumberOfSubscribers;
            accountSubscribersUpdateLogEntity.NumberOfSubscribersStep = accountSubscribersUpdateLogBLL.NumberOfSubscribersStep;
            accountSubscribersUpdateLogEntity.UpdateSendDate = accountSubscribersUpdateLogBLL.UpdateSendDate;
            accountSubscribersUpdateLogEntity.IsSuccessful = accountSubscribersUpdateLogBLL.IsSuccessful;
            accountSubscribersUpdateLogEntity.Data = Newtonsoft.Json.JsonConvert.SerializeObject(accountSubscribersUpdateLogBLL.Data);

            return accountSubscribersUpdateLogEntity;
        }
        public static bool IsNextStepReached(BLL.Account account, Dictionary<int,BLL.AccountSubscribersUpdateLog> accountIDToLastSendLogForAccount, BLL.AccountSubscribersUpdateLog newUpdateLog)
        {
            newUpdateLog.NumberOfSubscribersStep = GetNextUpdateStep(account.NumberOfSubscribers);
            if (newUpdateLog.NumberOfSubscribersStep <= 0)
            {
                return false;
            }
            //if (IsFirstSend(accountIDToLastSendLogForAccount, account))
            //{
            //    return true;
            //}
            var lastLogForAccount = accountIDToLastSendLogForAccount[account.AccountID];
            return !lastLogForAccount.IsSuccessful || newUpdateLog.NumberOfSubscribersStep > lastLogForAccount.NumberOfSubscribersStep;
        }
        private static bool IsFirstSend(Dictionary<int, BLL.AccountSubscribersUpdateLog> accountIDToLastSendLogForAccount,BLL.Account account)
        {
            //return !accountIDToLastSendLogForAccount.ContainsKey(account.AccountID);
            if (!accountIDToLastSendLogForAccount.ContainsKey(account.AccountID)) 
            {
                return true;
            }
            var lastLogForAccount = accountIDToLastSendLogForAccount[account.AccountID];
            return lastLogForAccount.Data.Errors.Equals("Dummy log for initial version upload - to prevent send to all accounts");

        }
        public static int GetNextUpdateStepByStepSize(int prevNumberOfSubscribers, int stepSize)
        {
            int closestStepSize = ((int)prevNumberOfSubscribers / stepSize) * stepSize;
            return closestStepSize;
        }
        public static int GetNextUpdateStep(int numberOfSubscribers)
        {
            int nextUpdateStep = -1;
            if (numberOfSubscribers < 250)
            {
                nextUpdateStep = 0;
            }
            else if (numberOfSubscribers >= 250 && numberOfSubscribers < 300)
            {
                nextUpdateStep = 250;
            }
            else if (numberOfSubscribers >= 300 && numberOfSubscribers < 400)
            {
                nextUpdateStep = 300;
            }
            else if (numberOfSubscribers >= 400 && numberOfSubscribers < 500)
            {
                nextUpdateStep = 400;
            }
            else if (numberOfSubscribers >= 500 && numberOfSubscribers < 600)
            {
                nextUpdateStep = 500;
            }
            else if (numberOfSubscribers >= 600 && numberOfSubscribers < 750)
            {
                nextUpdateStep = 600;
            }
            else if (numberOfSubscribers >= 750 && numberOfSubscribers <= 10000)
            {
                nextUpdateStep = GetNextUpdateStepByStepSize(numberOfSubscribers, 250);
            }
            else if (numberOfSubscribers > 10000 && numberOfSubscribers <= 30000)
            {
                nextUpdateStep = GetNextUpdateStepByStepSize(numberOfSubscribers, 500);
            }
            else if (numberOfSubscribers > 30000)
            {
                nextUpdateStep = GetNextUpdateStepByStepSize(numberOfSubscribers, 1000);
            }
            return nextUpdateStep;
        }

        public static bool ShouldAccountRecieveMail(BLL.Account account, Dictionary<int, BLL.AccountSubscribersUpdateLog> accountIDToLastSendLogForAccount, BLL.AccountSubscribersUpdateLog newUpdateLog, bool isTestEnv = true)
        {
            //if (!account.Data.AutomationTypeToData[AutomationTypes.AccountSubscribersUpdate].IsActive)
            if (account.PaymentApproved) //is already using the feature and should not be notified of updates
            {
                return false;
            }
            bool isNextStepReached = Services.AccountSubscribersUpdateLogServices.IsNextStepReached(account, accountIDToLastSendLogForAccount, newUpdateLog);
            if (!isNextStepReached)
            {
                return false;
            }
            bool isAccountWithRequiredFields = (!account.Data.Emails.IsEmpty() && !string.IsNullOrEmpty(account.Data.PrimaryDomain) && !account.Data.PrimaryDomain.Contains("חסר!")) || isTestEnv;
            return isAccountWithRequiredFields;



        }

        public static int CreateDummyLogForAccount(BLL.Account account,DateTime time)
        {
            var log = new BLL.AccountSubscribersUpdateLog();
            log.AccountID = account.AccountID;
            log.CurrentNumberOfSubscribers = account.NumberOfSubscribers;
            log.NumberOfSubscribersStep = Services.AccountSubscribersUpdateLogServices.GetNextUpdateStep(account.NumberOfSubscribers);
            log.IsSuccessful = true;
            log.UpdateSendDate = time;
            log.Data.Emails = new List<string>();
            log.Data.Errors = "Dummy log for initial version upload - to prevent send to all accounts";
            return Services.AccountSubscribersUpdateLogServices.Save(log);
        }
    }
}
