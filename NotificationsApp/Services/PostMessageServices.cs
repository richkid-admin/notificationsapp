﻿using BLL;
using BLL.Services;
using System;

namespace Services
{
    public class PostMessageServices
    {
        public static BLL.PostMessageLog PostMessage(dynamic inMessage, BLL.Account account, BLL.Package package, PostalServices.PostalService postalService,int automationID = 0)
        {
            dynamic a = true;
            var postMessageLog = new PostMessageLog();

            BLL.Message outMessage = new Message();

            postMessageLog.AccountID = account.AccountID;
            outMessage.AccountID = account.AccountID;
            if (automationID > 0)
            {
                outMessage.SendAutomationID = automationID;

            }

            postMessageLog.InMessage = inMessage;
            postMessageLog.OutMessage = outMessage;
            postMessageLog.PostMessageStatus = PostMessageStatus.OnEntry;

            try
            {
                // Check conditions to send message:
                // 1. Active account => Number of subscribers <= 200 OR payment approved by Mor/Chen
                // 2. Number of subscribers <= Max Subscribers of current package of the account
                postMessageLog.IsSendingAllowed = BLL.Services.AccountServices.IsSendingAllowed(account, package);
                if (postMessageLog.IsSendingAllowed)
                {
                    if (MessageServices.ValidateInputAndPopulateMessage(inMessage, outMessage, account.NumberOfSubscribers))
                    {
                        
                        //save message before sending notification
                        outMessage.MessageID = BLL.Services.MessageServices.Save(outMessage);

                        if (outMessage.MessageID == 0)
                        {
                            postMessageLog.PostMessageStatus = PostMessageStatus.MessageNotSavedBeforeSend;
                            postMessageLog.StatusDescription = "Message not saved correctly BEFORE sending to delivery service";
                            return postMessageLog;

                        }

                        outMessage.NotificationID = postalService.SendNotification(account.Data.Credentials.PulseemAPIKey, outMessage, DateTime.Now);
                        if (outMessage.NotificationID == 0)
                        {
                            postMessageLog.PostMessageStatus = PostMessageStatus.MessageNotSent;
                            postMessageLog.StatusDescription = "Message failed to be sent to pulseem";
                            return postMessageLog;
                        }
                        else
                        {
                            outMessage.Status = "Created";
                            outMessage.SentCount = account.NumberOfSubscribers; //business decision
                            outMessage.MessageID = MessageServices.Save(outMessage); // if we got here - message can be properly created and saved
                            if (outMessage.MessageID == 0)
                            {
                                postMessageLog.PostMessageStatus = PostMessageStatus.MessageNotSavedAfterSend;
                                postMessageLog.StatusDescription = "Message not saved correctly AFTER sending to delivery service";
                                return postMessageLog;
                            }
                            postMessageLog.PostMessageStatus = PostMessageStatus.Success;
                        }
                    }
                    else
                    {
                        postMessageLog.PostMessageStatus = PostMessageStatus.MessageNotValidatedAndCopied;
                        postMessageLog.StatusDescription = "Input message details received are not valid";
                    }
                }
                else
                {
                    postMessageLog.PostMessageStatus = PostMessageStatus.SendingNotAllowed;
                    postMessageLog.StatusDescription = "Sending of message not allowed on this account";
                }
            }
            catch (Exception ex)
            {
                postMessageLog.StatusDescription = ex.ToString();
            }
            finally
            {
                Services.PostMessageLogServices.Save(postMessageLog);
            }
            return postMessageLog;

        }
    }
}
