﻿using BLL;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class SendAutomationServices
    {
        public static List<BLL.SendAutomation> LoadAllSendAutomations(bool isActive)
        {
            List<Domain.Entities.SendAutomation> sendAutomationsEntities = null;
            List<BLL.SendAutomation> sendAutomationsBLL = new List<BLL.SendAutomation>();
            try
            {

                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    sendAutomationsEntities = sendAutomationRepo.LoadSendAutomations(isActive);
                }
                if (sendAutomationsEntities != null)
                {
                    foreach (var sendAutomationEntity in sendAutomationsEntities)
                    {
                        sendAutomationsBLL.Add(ConvertSendAutomationEntityToBusiness(sendAutomationEntity));
                    }
                    
                }
            }
            catch (Exception ex)
            {
            }

            return sendAutomationsBLL;
        }

        public static Domain.Models.SendAutomationsFiltersModel ConvertFiltersBLLToModel(BLL.SendAutomationFilters filtersBLL)
        {
            Domain.Models.SendAutomationsFiltersModel filtersModel = new Domain.Models.SendAutomationsFiltersModel();
            filtersModel.AccountsData = filtersBLL.AccountsData;
            switch (filtersBLL.SendAutomationStatus)
            {
                case SendAutomationStatus.All:
                    filtersModel.SendAutomationStatus = null;
                    break;
                case SendAutomationStatus.Active:
                    filtersModel.SendAutomationStatus = true;
                    break;
                case SendAutomationStatus.NotActive:
                    filtersModel.SendAutomationStatus = false;
                    break;
            }
            filtersModel.PageNumber = filtersBLL.PageNumber;
            filtersModel.NumberOfPages = filtersBLL.NumberOfPages;
            filtersModel.EntitiesPerPage = SendAutomationFilters.EntitiesPerPage;
            return filtersModel;



        }
        public static List<SendAutomation> GetSendAutomationsModelByPageAndFilters_Model(BLL.SendAutomationFilters filtersBLL)
        {
            List<Domain.Entities.SendAutomation> automationsEntities = new List<Domain.Entities.SendAutomation>();
            List<BLL.SendAutomation> automationsBLL = new List<BLL.SendAutomation>();
            var filtersModel = ConvertFiltersBLLToModel(filtersBLL);
            try
            {
                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    automationsEntities = sendAutomationRepo.GetSendAutomationsModelByPageAndFilters_Model(filtersModel);
                }
                filtersBLL.NumberOfPages = filtersModel.NumberOfPages;
                if (automationsEntities != null)
                {
                    foreach (var sendAutomationEntity in automationsEntities)
                    {
                        automationsBLL.Add(ConvertSendAutomationEntityToBusiness(sendAutomationEntity));
                    }

                }
            }
            catch (Exception ex)
            {
            }

            return automationsBLL;
        }
        public static List<SendAutomation> GetSendAutomationsModelByPageAndFilters_Overloading(int pageNum, BLL.SendAutomationStatus statusToFilter, List<string> accountsDataToFilter, out int numOfPages, int nAutomationsInPage = 10)
        {
            List<Domain.Entities.SendAutomation> automationsEntities = new List<Domain.Entities.SendAutomation>();
            List<BLL.SendAutomation> automationsBLL = new List<BLL.SendAutomation>();
            numOfPages = 0;
            try
            {
                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    bool allStatuses = statusToFilter.Equals(BLL.SendAutomationStatus.All);
                    bool isActive = statusToFilter.Equals(BLL.SendAutomationStatus.Active);
                    bool shouldFilterByAccounts = accountsDataToFilter != null && accountsDataToFilter.Count > 0;
                    if (allStatuses && !shouldFilterByAccounts)
                    {
                        automationsEntities = sendAutomationRepo.LoadSendAutomationsWithPaging(pageNum, nAutomationsInPage, out numOfPages);
                    }
                    else if (allStatuses && shouldFilterByAccounts)
                    {
                        automationsEntities = sendAutomationRepo.LoadSendAutomationsWithPagingAndFilters_Overloading(pageNum, accountsDataToFilter, nAutomationsInPage, out numOfPages);
                    }
                    else if (!shouldFilterByAccounts)
                    {
                        automationsEntities = sendAutomationRepo.LoadSendAutomationsWithPagingAndFilters_Overloading(pageNum,isActive, nAutomationsInPage, out numOfPages);
                    }
                    else
                    {
                        automationsEntities = sendAutomationRepo.LoadSendAutomationsWithPagingAndFilters_Overloading(pageNum, isActive, accountsDataToFilter, nAutomationsInPage, out numOfPages);
                    }
                    
                }
                if (automationsEntities != null)
                {
                    foreach (var sendAutomationEntity in automationsEntities)
                    {
                        automationsBLL.Add(ConvertSendAutomationEntityToBusiness(sendAutomationEntity));
                    }

                }
            }
            catch (Exception ex)
            {
            }

            return automationsBLL;
        }

        public static List<BLL.SendAutomation> LoadActiveAutomationsForNow()
        {
            List<Domain.Entities.SendAutomation> sendAutomationsEntities = null;
            List<BLL.SendAutomation> sendAutomationsBLL = new List<BLL.SendAutomation>();
            try
            {

                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    sendAutomationsEntities = sendAutomationRepo.LoadActiveAutomationsForNow();
                }
                if (sendAutomationsEntities != null)
                {
                    foreach (var sendAutomationEntity in sendAutomationsEntities)
                    {
                        sendAutomationsBLL.Add(ConvertSendAutomationEntityToBusiness(sendAutomationEntity));
                    }

                }
            }
            catch (Exception ex)
            {
            }

            return sendAutomationsBLL;
        }

        public static BLL.SendAutomation LoadByID(int sendAutomationID)
        {
            Domain.Entities.SendAutomation sendAutomationsEntity = null;
            BLL.SendAutomation sendAutomationBLL = null;
            try
            {

                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    sendAutomationsEntity = sendAutomationRepo.LoadByID(sendAutomationID);
                }
                if (sendAutomationsEntity != null)
                {
                    sendAutomationBLL = ConvertSendAutomationEntityToBusiness(sendAutomationsEntity);
                }
            }
            catch (Exception ex)
            {
            }

            return sendAutomationBLL;
        }
        public static List<BLL.SendAutomation> LoadSendAutomationsWithPaging(int pageNum, out int numOfPages, int nAutomationsInPage = 10)
        {
            List<Domain.Entities.SendAutomation> automationsEntities = new List<Domain.Entities.SendAutomation>();
            List<BLL.SendAutomation> automationsBLL = new List<BLL.SendAutomation>();
            numOfPages = 0;
            try
            {

                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    automationsEntities = sendAutomationRepo.LoadSendAutomationsWithPaging(pageNum, nAutomationsInPage,out numOfPages);
                }
                if (automationsEntities != null)
                {
                    foreach (var sendAutomationEntity in automationsEntities)
                    {
                        automationsBLL.Add(ConvertSendAutomationEntityToBusiness(sendAutomationEntity));
                    }

                }
            }
            catch (Exception ex)
            {
            }

            return automationsBLL;
        }

        public static int Save(BLL.SendAutomation sendAutomationBLL)
        {
            int savedSendAutomationID = -1;
            try
            {
                Domain.Entities.SendAutomation sendAutomationsEntity = ConvertSendAutomationBusinessToEntity(sendAutomationBLL);
                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    savedSendAutomationID = sendAutomationRepo.Save(sendAutomationsEntity);
                }
            }
            catch (Exception ex)
            {
            }

            return savedSendAutomationID;
        }
        public static bool Delete(int sendAutomationID)
        {
            bool isSuccess = true;
            bool dbDeleteSuccessful = false;
            try
            {
                var sendAutomationToDelete = Services.SendAutomationServices.LoadByID(sendAutomationID);
                
                using (Domain.Repositories.SendAutomationRepository automationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    dbDeleteSuccessful = automationRepo.Delete(sendAutomationID);
                }
                if (dbDeleteSuccessful && sendAutomationToDelete != null && !string.IsNullOrEmpty(sendAutomationToDelete.TemplateMessage.ImageUrl)) //delete previous image only if save succeeded and image was updated
                {
                    var fileManager = FileManager.GetInstance();
                    var templateImage = Newtonsoft.Json.JsonConvert.DeserializeObject<Common.ManagedFile>(sendAutomationToDelete.TemplateMessage.ImageUrl);
                    fileManager.DeleteFile(templateImage);
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
            }

            return isSuccess;
        }
        public static BLL.SendAutomation ConvertSendAutomationEntityToBusiness(Domain.Entities.SendAutomation sendAutomationEntity)
        {
            var sendAutomationBLL = new BLL.SendAutomation();
            try
            {
                sendAutomationBLL.SendAutomationID = sendAutomationEntity.SendAutomationID;
                sendAutomationBLL.AccountID = sendAutomationEntity.AccountID;
                sendAutomationBLL.IsActive = sendAutomationEntity.IsActive;
                sendAutomationBLL.DisplayName = sendAutomationEntity.DisplayName;
                sendAutomationBLL.CreationDate = sendAutomationEntity.CreationDate;
                sendAutomationBLL.LastMessageSentDate = sendAutomationEntity.LastMessageSentDate;
                sendAutomationBLL.TemplateMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<BLL.Message>(sendAutomationEntity.TemplateMessage);

                sendAutomationBLL.Day = (DayOfWeek)sendAutomationEntity.Day;
                sendAutomationBLL.Hour = sendAutomationEntity.Hour;


                sendAutomationBLL.AutomationData = Newtonsoft.Json.JsonConvert.DeserializeObject<BLL.AutomationData>(sendAutomationEntity.AutomationData);
                sendAutomationBLL.AutomationData.HasSentMessages = sendAutomationEntity.LastMessageSentDate != null;
                return sendAutomationBLL;
            }
            catch (Exception ex) 
            {

            }
            return sendAutomationBLL;
        }
        public static Domain.Entities.SendAutomation ConvertSendAutomationBusinessToEntity(BLL.SendAutomation sendAutomationBLL)
        {
            var sendAutomationEntity = new Domain.Entities.SendAutomation();
            try
            {
                sendAutomationEntity.SendAutomationID = sendAutomationBLL.SendAutomationID;
                sendAutomationEntity.AccountID = sendAutomationBLL.AccountID;
                sendAutomationEntity.IsActive = sendAutomationBLL.IsActive;
                sendAutomationEntity.DisplayName = sendAutomationBLL.DisplayName;
                sendAutomationEntity.CreationDate = sendAutomationBLL.CreationDate;
                sendAutomationEntity.LastMessageSentDate = sendAutomationBLL.LastMessageSentDate;
                sendAutomationEntity.TemplateMessage = Newtonsoft.Json.JsonConvert.SerializeObject(sendAutomationBLL.TemplateMessage);

                sendAutomationEntity.Day = (int)sendAutomationBLL.Day;
                sendAutomationEntity.Hour = sendAutomationBLL.Hour;

                sendAutomationEntity.AutomationData = Newtonsoft.Json.JsonConvert.SerializeObject(sendAutomationBLL.AutomationData);
                return sendAutomationEntity;
            }
            catch (Exception ex)
            {

            }
            return sendAutomationEntity;
        }


        public static bool ChangeLastSendDateToAutomation(int sendAutomationID,int daysAgo, int hoursToAdd)
        {
            var sendAutomation = Services.SendAutomationServices.LoadByID(sendAutomationID);
            if (sendAutomation == null)
            {
                return false;
            }
            var generatedLastSendDAte = DateTime.Now.AddDays(-daysAgo).AddHours(hoursToAdd);
            sendAutomation.LastMessageSentDate = generatedLastSendDAte;
            int savedID = Services.SendAutomationServices.Save(sendAutomation);
            return savedID > 0;
        }

        public static List<SendAutomation> LoadSendAutomationsWithMessages()
        {
            List<Domain.Entities.SendAutomation> sendAutomationsEntities = null;
            List<BLL.SendAutomation> sendAutomationsBLL = new List<BLL.SendAutomation>();
            try
            {

                using (Domain.Repositories.SendAutomationRepository sendAutomationRepo = new Domain.Repositories.SendAutomationRepository())
                {
                    sendAutomationsEntities = sendAutomationRepo.LoadSendAutomationsWithMessages();
                }
                if (sendAutomationsEntities != null)
                {
                    foreach (var sendAutomationEntity in sendAutomationsEntities)
                    {
                        sendAutomationsBLL.Add(ConvertSendAutomationEntityToBusiness(sendAutomationEntity));
                    }

                }
            }
            catch (Exception ex)
            {
                sendAutomationsBLL.Add(new SendAutomation() { TemplateMessage = new Message() { MessageText = ex.ToString() } });
            }

            return sendAutomationsBLL;
        }



    }
}
