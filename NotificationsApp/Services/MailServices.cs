﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class MailServices
    {
        public static void SendMailForAccountPackageUpgrade(BLL.AccountPackageLog accountPackageLog)
        {
            var account = BLL.Services.AccountServices.LoadAccountByID(accountPackageLog.AccountID);

            string content = "";
            var emailSubject = "בחשבון " + account.Name + " התבצע תהליך שדרוג";
            content += "<div style=\"direction: rtl;\">";
            content += "החשבון " + account.Name + " שודרג בהצלחה!" + "<br/>";
            content += "סוג החבילה המקורית: " + accountPackageLog.CurrentPackageID + "<br/>";
            content += "סוג החבילה החדשה: " + accountPackageLog.NewPackageID + "<br/>";
            content += "למנוי קיימים כרגע " + account.NumberOfSubscribers + " מנויים במערכת." + "<br/>";
            content += "</div>";

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.From = new System.Net.Mail.MailAddress("ads3@richkid.co.il");


            var destinationEmail = "mor@richkid.co.il"; // Mor Nahmani Mail
            var bcc = "chen.s@richkid.co.il"; // Chen Shaul Mail

            mail.To.Add(destinationEmail);
            mail.To.Add(bcc);

            mail.IsBodyHtml = true;
            mail.Body = content;
            mail.Subject = emailSubject;
            System.Net.Mail.SmtpClient x = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential("ads3@richkid.co.il", "wvxwhpknwqftdqrk")
            };

            x.Send(mail);
        }
        public static void SendMailAfterAutomationWorkerFail(dynamic log)
        {

            string content = "";
            var emailSubject = "שגיאה במערכת נוטיפיקציות - אוטומציות";
            content += "<div style=\"direction: rtl;\">";
            content += $"הפירוט המלא: ${Newtonsoft.Json.JsonConvert.SerializeObject(log)}";
            content += "</div>";

            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.From = new System.Net.Mail.MailAddress("ads3@richkid.co.il");


            var destinationEmail = "tom@richkid.co.il";
            var bcc = "tal.dvir@richkid.co.il";

            mail.To.Add(destinationEmail);
            mail.To.Add(bcc);

            mail.IsBodyHtml = true;
            mail.Body = content;
            mail.Subject = emailSubject;
            System.Net.Mail.SmtpClient x = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential("ads3@richkid.co.il", "wvxwhpknwqftdqrk")
            };

            x.Send(mail);
        }
        public static bool SendSubscribersUpdateMail(BLL.Account account, string mailContentAsHtml, out string errors, bool isTest = true)
        {
            errors = string.Empty;
            bool isSuccess = true;
            //System.Net.ServicePointManager.SecurityProtocol = (SecurityProtocolType)(0xc0 | 0x300 | 0xc00);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                      | SecurityProtocolType.Tls11
                                      | SecurityProtocolType.Tls12;

            try
            {
                string primaryDomain = account?.Data?.PrimaryDomain ?? string.Empty;
                string emailSubject = "כמות המנויים באתר " + primaryDomain + " הגיעה ל-" + account.NumberOfSubscribers.ToString();
                string mailFrom = "mood@richkid.co.il";
                //var creds = new System.Net.NetworkCredential("mood@richkid.co.il", "elad1011");
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                var destinationEmails = account.Data.Emails;
                if (isTest)
                {
                    destinationEmails = new List<string>() { "chen.s@richkid.co.il" };
                }
                else
                {
					//mail.Bcc.Add("chen.s@richkid.co.il");
					//mail.Bcc.Add("mor@richkid.co.il");
					////mail.Bcc.Add("idan.s@richkid.co.il");
					//mail.Bcc.Add("or@richkid.co.il");
					////mail.Bcc.Add("yotam.gil@richkid.co.il");
					////mail.Bcc.Add("amit@richkid.co.il");

					mail.Bcc.Add("mor@richkid.co.il");
					mail.Bcc.Add("rachel@richkid.co.il");
					mail.Bcc.Add("lee@richkid.co.il");
					mail.Bcc.Add("ben.m@richkid.co.il");

				}

                mail.From = new System.Net.Mail.MailAddress(mailFrom);
                foreach (var destinationEmail in destinationEmails)
                {
                    mail.To.Add(destinationEmail);
                }

                mail.IsBodyHtml = true;
                mail.Body = mailContentAsHtml;
                mail.Subject = emailSubject;
                System.Net.Mail.SmtpClient x = new System.Net.Mail.SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential("mood@richkid.co.il", "elad1011")
				//Credentials = creds
			};

                x.Send(mail);
            }
            catch (Exception ex)
            {
                errors = ex.ToString();
                isSuccess = false;
            }
            return isSuccess;
        }
    }
}
