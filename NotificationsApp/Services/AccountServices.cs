﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL;

namespace Services
{
    public class AccountServices
    {
        public static Dictionary<int,BLL.Account> LoadAccountsBySendAutomations(List<BLL.SendAutomation> sendAutomations)
        {
            //var sendAutomationAccountsIDs = sendAutomations
            //    .Select(sa => sa.AccountID)
            //    .ToList();
            var sendAutomationAccountsIDs = new HashSet<int>(sendAutomations.Select(sa => sa.AccountID));
            return BLL.Services.AccountServices.LoadAccountsByIds(sendAutomationAccountsIDs)
                .ToDictionary(a => a.AccountID, a => a);
        }

        public static List<Account> LoadAccountsByFilters(AccountsFilters filters)
        {
            //TODO: add convert same as sendautomations if there's time
            var filtersModel = ConvertFiltersBLLToModel(filters);
            List<Domain.Entities.Account> accountsEntities = new List<Domain.Entities.Account>();
            List<Account> accountsBLL = new List<Account>();
            try
            {
                using (Domain.Repositories.AccountRepository sendAutomationRepo = new Domain.Repositories.AccountRepository())
                {
                    accountsEntities = sendAutomationRepo.GetAccountsByFilters(filtersModel);
                }
                accountsBLL = accountsEntities
                    .Select(e => BLL.Services.AccountServices.ConvertAccountEntityToBusiness(e))
                    .ToList();
            }
            catch (Exception ex)
            {
            }

            return accountsBLL;
        }

        private static Domain.Models.AccountsFiltersModel ConvertFiltersBLLToModel(AccountsFilters filters)
        {
            Domain.Models.AccountsFiltersModel model = new Domain.Models.AccountsFiltersModel();
            switch (filters.Status)
            {
                case 0:
                    model.IsActive = null;
                    break;
                case 1:
                    model.IsActive = false;
                    break;
                case 2:
                    model.IsActive = true;
                    break;
            }
            model.IsWithAutomations = filters.IsWithAutomations;
            return model;
        }

        public static Account CreateAccount(string name, string APIKey)
        {
            try
            {
                var now = DateTime.Now;
                BLL.Account account = new Account
                {
                    //PackageID = 1, // default package
                    Name = name,
                    AccountKey = Guid.NewGuid().ToString(),
                    Data = new Account.AccountData(APIKey),
                    CreationDate = now,
                };

                account.AccountID = BLL.Services.AccountServices.Save(account);

                //requires first log to operate
                Services.AccountSubscribersUpdateLogServices.CreateDummyLogForAccount(account, now);

                return account.AccountID > 0 ? account : null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
