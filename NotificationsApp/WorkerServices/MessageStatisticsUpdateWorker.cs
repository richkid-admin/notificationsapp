﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkerServices
{
    public class MessageStatisticsUpdateWorker
    {

        public static void RunStatisticsUpdateWorker()
        {
            TimeSpan TimeBetweenSamples = new TimeSpan(0, 30, 0); //for initial upload
            //private static TimeSpan TimeBetweenSamples { get; } = new TimeSpan(6, 0, 0); //based on design
            Worker StatisticsUpdateWorker = new Worker
            {
                RunFunction = UpdateStatistics,

                ////int minutesDelay = 60 - DateTime.Now.Minute; // every round hour
                //int minutesDelay = (60 - DateTime.Now.Minute) % 10; // every round 10 minutes (10, 20, 30...)
                //AbandondCartsWorker.TimeDelay = new TimeSpan(0, minutesDelay, 0);            

                TimeInterval = TimeBetweenSamples
            };

            StatisticsUpdateWorker.StartWorker();

        }

        private static bool UpdateStatistics()
        {
            bool success = true;
            dynamic jarrLog = new JArray();
            try
            {
                Dictionary<int, BLL.Account> accountIDToAccount;
                Dictionary<int, List<int>> messagesSentAccountIDToNotificationIDs;
                List<BLL.Message> messagesToUpdateAllAccounts = new List<BLL.Message>();

                TimeSpan tsUpdatesMessageStartDate = new TimeSpan(7, 0, 0, 0); /*new TimeSpan(3,0, 0, 0);*/
                DateTime dtEarliestTimeInInterval = DateTime.Now - tsUpdatesMessageStartDate;

                PostalServices.PostalService pulseemPostalService = new PostalServices.PulseemPostalService();

                messagesSentAccountIDToNotificationIDs = BLL.Services.MessageServices.GetMessageAccountToNotificationIDsFrom(dtEarliestTimeInInterval, true);
                accountIDToAccount = BLL.Services.AccountServices.LoadAccountsByIds(messagesSentAccountIDToNotificationIDs.Keys).ToDictionary(acc => acc.AccountID, acc => acc);

                BLL.Account currAccount;
                BLL.Message currMessage;
                foreach (int accountIDWithSentMessages in messagesSentAccountIDToNotificationIDs.Keys) //seqeuentially 
                {
                    currAccount = accountIDToAccount[accountIDWithSentMessages];
                    foreach (int notificationID in messagesSentAccountIDToNotificationIDs[accountIDWithSentMessages])
                    {
                        PostalServices.NotificationStatistics msgStats = pulseemPostalService.GetNotificationStatisticsByID(notificationID, currAccount.Data.Credentials.PulseemAPIKey);
                        if (msgStats == null) //simple correctness verification of message, expecting better validation inside method
                        {
                            jarrLog.Add(new JObject()
                            {
                                ["NotificationID"] = notificationID,
                                ["Error"] = "Expected to get notification statistics from pulseem, received null"
                            });
                            continue;
                        }
                        currMessage = pulseemPostalService.Convert(msgStats);
                        //currMessage.SentCount = currAccount.NumberOfSubscribers; //business requirement
                        messagesToUpdateAllAccounts.Add(currMessage);
                    }
                    
                }
                BLL.Services.MessageServices.UpdateStatistics(messagesToUpdateAllAccounts);
                return success;
            }
            catch (Exception ex)
            {
                jarrLog.Add(new JObject()
                {
                    ["Exception"] = ex.ToString()
                });
                //log
                //maybe send some alert to someone
                return !success;
            }
            finally
            {
                if (jarrLog.Count > 0)
                {
                    var sLog = Newtonsoft.Json.JsonConvert.SerializeObject(jarrLog);
                    BLL.Services.LogServices.WriteLog("PushNotificationLogs/WorkerLogs", "StatisticsWorkerLog", sLog);
                }
            }
        }

    }
}
