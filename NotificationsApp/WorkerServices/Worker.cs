﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WorkerServices
{
    public class Worker
    {
        public Func<bool> SetFunction { get; set; }
        public Func<bool> RunFunction { get; set; }
        public TimeSpan TimeInterval { get; set; }
        public TimeSpan TimeDelay { get; set; }
        public Timer WorkerTimer { get; set; }


        public void StartWorker()
        {
            try
            {
                SetFunction?.Invoke();
                if (TimeDelay == null || TimeDelay == TimeSpan.MinValue)
                    TimeDelay = TimeSpan.Zero;

                WorkerTimer = new Timer(async obj => { await StartAction(); }, null, TimeDelay, TimeInterval);
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private async Task StartAction()
        {
            await Task.Delay(1);
            RunFunction?.Invoke();
        }
    }
}
