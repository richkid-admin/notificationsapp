﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WorkerServices
{
    public class SubscribersCountUpdateWorker
    {
        private static TimeSpan TimeBetweenSamples { get; } = new TimeSpan(0, 10, 0); //for initial upload
        //private static TimeSpan TimeBetweenSamples { get; } = new TimeSpan(24, 0, 0);

        public static void RunSubscribersCountUpdateWorker()
        {

            Worker SubscribersCountUpdateWorker = new Worker
            {
                RunFunction = UpdateAccountsSubscribers,
                TimeInterval = TimeBetweenSamples
            };

            SubscribersCountUpdateWorker.StartWorker();

        }

        /*
         * single API call from main accounts to get all sub accounts info
         * using 3rd party accountID for reference
         * currently can't be implemented because 3rd party ID is not returned on creation.
         */
        //private static bool UpdateSubAccountsCountSingleAPICall() 
        //{
        //    bool success = true;
        //    Dictionary<string, int> accountIDToSubAccountCount = new Dictionary<string, int>();
        //    PostalServices.PostalService pulseemPostalService = new PostalServices.PulseemPostalService();
        //    try
        //    {
        //        accountIDToSubAccountCount = pulseemPostalService.GetSubAccountsSubscribersCount();// working with Richkid account
        //        BLL.Services.AccountServices.UpdateStatistics(accountIDToSubAccountCount);
        //        return success;
        //    }
        //    catch (Exception ex)
        //    {
        //        //log
        //        //maybe send some alert to someone
        //        return !success;
        //    }
        //}
        private static bool UpdateAccountsSubscribers() //API call for each account separately, using APIKey as 3rd party ID. 
        {
            bool success = false;
            Dictionary<int, int> AccountIDToSubscribersCount = new Dictionary<int, int>();
            PostalServices.PostalService pulseemPostalService = new PostalServices.PulseemPostalService();
            try
            {
                List<BLL.Account> allAccounts = BLL.Services.AccountServices.LoadAllAccounts()
                    .Where(a => a.AccountID != 119)
                    .ToList();
                foreach(BLL.Account acc in allAccounts)
                {
                    var accountSubscribers = pulseemPostalService.GetAccountSubscribersCount(acc.Data.Credentials.PulseemAPIKey);
                    if (accountSubscribers != -1)
                    {
                        AccountIDToSubscribersCount[acc.AccountID] = accountSubscribers;
                    }
                }
                BLL.Services.AccountServices.UpdateStatistics(AccountIDToSubscribersCount);
                success = true;
            }
            catch (Exception ex)
            {
                //log
                //maybe send some alert to someone
            }
            return success;
        }

    }
}
