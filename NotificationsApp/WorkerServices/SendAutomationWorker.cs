﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;


namespace WorkerServices
{
    public class SendAutomationWorker : Worker
    {
        private static int TimeBetweenSamplesInMinutes { get; } = 30;


        public static void Run()
        {
            var timeBetweenIterations = new TimeSpan(0, TimeBetweenSamplesInMinutes, 0);
            int minutesLeftToFirstIteration = ((60 - DateTime.Now.Minute) % TimeBetweenSamplesInMinutes)/*round hours*/ + 1 /*safety margin*/; 
            var timeUntilfirstIteration = new TimeSpan(0, minutesLeftToFirstIteration, 0);
            

            Worker SendAutomationWorker = new Worker
            {
                RunFunction = SendMessageForAutomations,
                TimeInterval = timeBetweenIterations,
                TimeDelay = timeUntilfirstIteration
            };

            SendAutomationWorker.StartWorker();

        }
        /// <summary>
        ///load all accounts
        ///get all active automations that should be sent now
        ///get all packages
        ///foreach automation
        ///get message using automation template and maybe other params
        ///send message, save log(all handled by external func)
        ///save automation with lastSendDate
        ///</summary>
        /// <returns>success status : bool</returns>
        private static bool SendMessageForAutomations() 
        {
            PostalServices.PostalService pulseemPostalService = new PostalServices.PulseemPostalService();
            var sendAutomationIterationLog = new BLL.SendAutomationIterationLog();
            int messagesSent = 0;
            bool IsSuccess = false;
            try
            {

                var sendAutomations = Services.SendAutomationServices.LoadActiveAutomationsForNow();
                if (sendAutomations.IsEmpty())
                {
                    sendAutomationIterationLog.SendAutomationIterationStatus = BLL.SendAutomationWorkerIterationStatus.NoAutomationsLoaded;
                    return IsSuccess;
                }

                var automationsAccountIDToAccount = Services.AccountServices.LoadAccountsBySendAutomations(sendAutomations);
                if (automationsAccountIDToAccount.IsEmpty())
                {
                    sendAutomationIterationLog.SendAutomationIterationStatus = BLL.SendAutomationWorkerIterationStatus.NoAccountsLoaded;
                    return IsSuccess;
                }

                var allPackageIDToPackage = BLL.Services.PackageServices.LoadAllPackages().ToDictionary(p => p.PackageID, p => p);
                if (automationsAccountIDToAccount.IsEmpty())
                {
                    sendAutomationIterationLog.SendAutomationIterationStatus = BLL.SendAutomationWorkerIterationStatus.NoPackagesLoaded;
                    return IsSuccess;
                }

                foreach (var sendAutomation in sendAutomations)
                {
                    int sendAutomationLogID = SendMessage(sendAutomation, automationsAccountIDToAccount, allPackageIDToPackage, pulseemPostalService);
                    if (sendAutomationLogID > 0)
                    {
                        messagesSent++;
                        sendAutomationIterationLog.SendAutomationLogIDs.Add(sendAutomationLogID);
                    }
                }
                IsSuccess = true;
                sendAutomationIterationLog.SendAutomationIterationStatus = BLL.SendAutomationWorkerIterationStatus.MessagesSent;
            }
            catch (Exception ex)
            {
                sendAutomationIterationLog.Exception = ex.ToString();
            }
            finally
            {
                if (messagesSent > 0 || !string.IsNullOrEmpty(sendAutomationIterationLog.Exception))
                {
                    sendAutomationIterationLog.MessagesSent = messagesSent;
                    Services.SendAutomationIterationLogServices.Save(sendAutomationIterationLog);
                }
                //if (!IsSuccess)
                //{
                //    Services.MailServices.SendMailAfterAutomationWorkerFail(sendAutomationIterationLog);
                //}

            }
            return IsSuccess;

        }

        private static int SendMessage(BLL.SendAutomation sendAutomation,Dictionary<int,BLL.Account> automationsAccountIDToAccount,Dictionary<int,BLL.Package> allPackageIDToPackage,PostalServices.PostalService postalService)
        {
            BLL.Account accountForCurrentAutomation;
            BLL.Package packageForCurrentAccount;
            BLL.Message messageForCurrentAutomation;
            JObject jMessageForCurrentAutomation;
            BLL.SendAutomationLog logForCurrentAutomation = new BLL.SendAutomationLog
            {
                AutomationID = sendAutomation.SendAutomationID
            };
            int successStatus = 0;

            try
            {
                accountForCurrentAutomation = automationsAccountIDToAccount[sendAutomation.AccountID];
                logForCurrentAutomation.AccountID = accountForCurrentAutomation.AccountID;

                packageForCurrentAccount = allPackageIDToPackage[accountForCurrentAutomation.PackageID];
                logForCurrentAutomation.PackageID = packageForCurrentAccount.PackageID;

                logForCurrentAutomation.SendAutomationStatus = BLL.SendAutomationIterationStatus.AccountAndPackageLoaded;

                //future version to get message
                //var getMessageStrategy = BLL.GetMessageStrategyManager.GetStrategy(sendAutomation.GetMessageStrategy);
                //messageForCurrentAutomation = getMessageStrategy.GetMessage();

                //current version to get message
                //messageForCurrentAutomation = sendAutomation.TemplateMessage;
                messageForCurrentAutomation = sendAutomation.TemplateMessage.DeepClone();

                messageForCurrentAutomation.AccountID = accountForCurrentAutomation.AccountID;
                jMessageForCurrentAutomation = JObject.FromObject(sendAutomation.TemplateMessage);
                if (!string.IsNullOrEmpty(messageForCurrentAutomation.ImageUrl))
                {
                    jMessageForCurrentAutomation["Image"] = GetImagePublicPathForNotification(messageForCurrentAutomation);
                }

                logForCurrentAutomation.SendAutomationStatus = BLL.SendAutomationIterationStatus.MessageCopied;

                //Currently relying on messageText to be present and therefore expecting inner validation to return as true
                var postMessageLog = Services.PostMessageServices.PostMessage(jMessageForCurrentAutomation, accountForCurrentAutomation, packageForCurrentAccount, postalService,sendAutomation.SendAutomationID);
                if (!postMessageLog.PostMessageStatus.Equals(BLL.PostMessageStatus.Success))
                {
                    return successStatus;
                }
                logForCurrentAutomation.MessageID = postMessageLog.OutMessage.MessageID;
                logForCurrentAutomation.SendAutomationStatus = BLL.SendAutomationIterationStatus.MessageSent;

                sendAutomation.LastMessageSentDate = DateTime.Now;
                Services.SendAutomationServices.Save(sendAutomation);

                logForCurrentAutomation.SendAutomationStatus = BLL.SendAutomationIterationStatus.Success;

                
            }
            catch (Exception ex)
            {
                logForCurrentAutomation.Exception = ex.ToString();
            }
            finally
            {
                int sendAutomationLogID = Services.SendAutomationLogServices.Save(logForCurrentAutomation);

                sendAutomationLogID = sendAutomation.SendAutomationID; //TODO: save as table and change to its' ID

                if (logForCurrentAutomation.SendAutomationStatus.Equals(BLL.SendAutomationIterationStatus.Success))
                {
                    successStatus = sendAutomationLogID;
                }

            }

            return successStatus;
        }

        private static string GetImagePublicPathForNotification(BLL.Message messageForCurrentAutomation)
        {
            var fileManager = FileManager.GetInstance();
            var notificationImageData = Newtonsoft.Json.JsonConvert.DeserializeObject<Common.ManagedFile>(messageForCurrentAutomation.ImageUrl);
            return fileManager.GetFilePath(notificationImageData, true);
            
            //return messageForCurrentAutomation.ImageUrl;
        }
    }
}
