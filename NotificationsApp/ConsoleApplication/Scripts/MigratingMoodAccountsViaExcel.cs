﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication.Scripts
{
    public class MigratingMoodAccountsViaExcel
    {
        public static void MigrateAccountsFromMoodCMS(string env)
        {
            string usersExcelPath;
            string resultsExcelPath;
            if (env.Equals("production"))
            {
                string rootPath = @"C:\Inetpub\vhosts\getmood.io\httpdocs\warehouse\";
                usersExcelPath = rootPath + "MoodNotificationUsers.xlsx";
                resultsExcelPath = rootPath + "MoodNotificationUsersWithAccountKey.xlsx";
            }
            else
            {
                usersExcelPath = @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsers.xlsx";
                resultsExcelPath= @"C:\Users\user\Desktop\Open Assignments\Push Notifications\MoodNotificationUsersWithAccountKey.xlsx";
            }
            //read excel
            List<string> AccountKeys = new List<string>();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage pck = new ExcelPackage();
            using (System.IO.FileStream s = System.IO.File.OpenRead(usersExcelPath))
            {
                pck.Load(s);
                var ws = pck.Workbook.Worksheets["Sheet1"];
                ws.InsertColumn(5, 1); //new accountKeys col
                string currUsername;
                string currAPIKey;
                BLL.Account currAccount;
                for (int i = 2; i <= ws.Dimension.Rows; i++)
                {
                    //foreach mood account - get: name and key

                    currUsername = ws.Cells[i, 2].Value != null ? ws.Cells[i, 2].Value.ToString().Trim() : "";
                    currAPIKey = ws.Cells[i, 4].Value != null ? ws.Cells[i, 4].Value.ToString().Trim() : "";

                    //create notifications app account, salvage accountKey
                    //currAccount = BLL.Services.AccountServices.CreateAccount(currUsername, currAPIKey);
                    currAccount = Services.AccountServices.CreateAccount(currUsername, currAPIKey);
                    //save accountKey in excel
                    if (currAccount != null)
                    {
                        ws.Cells[i, 5].Value = currAccount.AccountKey;
                    }
                }

                pck.SaveAs(resultsExcelPath);
            }
        }
    }
}
