﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication.Scripts
{
    public class RestoreNumOfSubscribers
    {
        public static void RunScript()
        {
            string dir = @"C:\Users\user\Desktop\Open Assignments\NotificationsBugs";
            string fname = "AccountIDToSubsCount.json";
            string fullFilePath = Path.Combine(dir, fname);
            ReadNumOfSubscribersFromFile(fullFilePath);
        }
        public static bool Write(string fPath)
        {
            bool success = false;
            try
            {
                Dictionary<int, int> accountIDToAccountKey = new Dictionary<int, int>();
                List<BLL.Account> allAccounts = BLL.Services.AccountServices.LoadAllAccounts();
                foreach (var account in allAccounts)
                {
                    accountIDToAccountKey[account.AccountID] = account.NumberOfSubscribers;
                }
                string sAccountIDToAccountKey = Newtonsoft.Json.JsonConvert.SerializeObject(accountIDToAccountKey);
                File.WriteAllText(fPath, sAccountIDToAccountKey);
                success = true;
            }
            catch (Exception ex)
            {

            }
            return success;
        }
        public static bool ReadNumOfSubscribersFromFile(string fPath)
        {
            bool success = false;
            try
            {
                string sAccountIDToAccountKey = File.ReadAllText(fPath);
                var accountIDToAccountKey = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, int>>(sAccountIDToAccountKey);
                if (accountIDToAccountKey != null && accountIDToAccountKey.Count > 0)
                {
                    BLL.Services.AccountServices.UpdateStatistics(accountIDToAccountKey);
                }
                success = true;

    }
            catch (Exception ex)
            {
            }
            return success;
        }
    }
}
