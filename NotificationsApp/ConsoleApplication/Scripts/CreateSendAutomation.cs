﻿using Newtonsoft.Json.Linq;
using System;

namespace ConsoleApplication.Scripts
{
    public class CreateSendAutomation
    {
        public static void Execute()
        {
            DateTime whenToSend = DateTime.Now;
            var sendAutomation = new BLL.SendAutomation
            {
                CreationDate = DateTime.Now,
                IsActive = true,
                TemplateMessage = BLL.Services.MessageServices.LoadMessageByID(4),
                AccountID = 119, //shop
                DisplayName = "asdasd"
            };
            sendAutomation.Day = DateTime.Now.DayOfWeek;
            sendAutomation.Hour = DateTime.Now.Hour;
            int savedSendAutomation = Services.SendAutomationServices.Save(sendAutomation);
            System.Diagnostics.Debug.Assert(savedSendAutomation > 0);
        }
    }
}
