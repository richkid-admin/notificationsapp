﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication.Scripts.Test
{
    class SubAccountCreation
    {
        //public static string SubAccountCreationTest()
        //{
        //    string subAccountName = "TestingAccountCreationNew";
        //    var pulseem = new PostalServices.PulseemPostalService();
        //    string newAccountAPIKEy = pulseem.AddSubAccount(subAccountName);
        //    System.Diagnostics.Debug.WriteLine(newAccountAPIKEy);
        //    return newAccountAPIKEy;
        //}
        public static string BuildAccountCreationMessage(string subAccountName, int siteID)
        {
            JObject creationMessage = new JObject();
            creationMessage["SubAccountName"] = subAccountName;
            creationMessage["AccountEmail"] = $"{subAccountName}@richkid-push.com";
            creationMessage["LoginUserName"] = $"Site-{siteID}"; //might be better to define a global getter 
            //creationMessage["LoginPassword"] = $"!{siteID}{subAccountName}!1@Aa"; //static if requires extra effort
            creationMessage["LoginPassword"] = $"AaBbCcDd!!!@ABC";
            return JsonConvert.SerializeObject(creationMessage);
        }
        //public static bool TestingAddNewAccountPulseem()
        //{
        //    string accountCreationURLPulseem = "https://api.pulseem.com/api/v1/AccountsApi/AddNewSubaccountAndDirectAcount"; //separate to root and resource
        //    Encoding encoding = Encoding.UTF8;
        //    bool success = false;

        //    string subAccountName = "TomPushNotificationsTest";
        //    int siteID = 1234;
        //    string sCreationMessage = BuildAccountCreationMessage(subAccountName, siteID);

        //    byte[] bCreationMessage = encoding.GetBytes(sCreationMessage);

        //    var httpWebRequest = (HttpWebRequest)WebRequest.Create(accountCreationURLPulseem);
        //    httpWebRequest.ContentType = "application/json";
        //    httpWebRequest.Method = "POST";
        //    httpWebRequest.Headers.Add("APIKEY", "C+oRbCBM7EifQEri7YBKag==");
        //    httpWebRequest.ContentLength = bCreationMessage.Length;

        //    Stream requestStream = httpWebRequest.GetRequestStream();//Get the stream that holds request data by calling the GetRequestStream method. For example:
        //    requestStream.Write(bCreationMessage, 0, bCreationMessage.Length);//Write the data to the Stream object returned by the GetRequestStream 
        //    requestStream.Close();//Close the Stream object.

        //    HttpWebResponse myHttpWebResponse;
        //    try
        //    {
        //        myHttpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();//Send the request to the server by calling WebRequest.GetResponse. This method returns an object containing the server's response. 
        //        Stream responseStream = myHttpWebResponse.GetResponseStream();//To get the stream containing response data sent by the server, call the WebResponse.GetResponseStream method of your WebResponse object.

        //        StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);//Pipes the stream to a higher level stream reader with the required encoding format.

        //        string sResponse = myStreamReader.ReadToEnd();

        //        myStreamReader.Close();
        //        responseStream.Close();
        //        myHttpWebResponse.Close();

        //        CreationResponseMessage creationResponseMessage = JsonConvert.DeserializeObject<CreationResponseMessage>(sResponse);


        //        if (creationResponseMessage.ErrorMessage != null)
        //        {
        //            //log the error messages
        //        }
        //        success = creationResponseMessage.Status == "Success";
        //        return success;
        //    }
        //    catch (WebException ex)
        //    {
        //        //log the error messages
        //        return success;
        //    }
        //    finally
        //    {
        //        System.Diagnostics.Debug.WriteLine(success);
        //    }

        //}
        //class CreationResponseMessage
        //{
        //    public string Status { get; set; }
        //    public string ErrorMessage { get; set; }
        //}
    }
}
