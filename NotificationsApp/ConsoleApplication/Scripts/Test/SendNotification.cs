﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication.Scripts.Test
{
    class SendNotification
    {
        public static void PulseemSendNotificationTest()
        {
            string APIKey = "jf2jzcSBQBHtwfXsGfrZxw=="; //richkid shop
            string name = "Test Message";
            string title = "Tom Test Message Temp Solution";
            string msg = "Test message, please ignore";
            string btnText = "לחץ כאן בדיקה";
            string redirectURL = "https://www.google.com";
            string imgURL = "";
            int notificationID = -1;
            DateTime sendTime = DateTime.Now;
            PostalServices.LanguageDirectionality textDirection = PostalServices.LanguageDirectionality.RightToLeft;

            //int returnedNotificationID = PostalServices.PulseemPostalService.SendNotification(APIKey, name, title, msg, btnText, redirectURL,imgURL, sendTime, textDirection);
            BLL.Message msgToSend = new BLL.Message
            {
                DisplayName = name,
                Headline = title,
                MessageText = msg,
                ButtonText = btnText,
                ButtonRedirectLink = redirectURL,
                ImageUrl = imgURL,
            };
            var pulseem = new PostalServices.PulseemPostalService();
            int returnedNotificationID = pulseem.SendNotification(APIKey, msgToSend, sendTime, textDirection);

            System.Diagnostics.Debug.Assert(returnedNotificationID > 2000);

        }
        public class NotificationResponseMessage
        {
            public int NotifictionId { get; set; }
            public int ResponseType { get; set; }
            public List<string> ErrorList { get; set; }
        }
        public static void TestingSerialization()
        {
            string sResponse = "\"{\\\"NotifictionId\\\":2313,\\\"ResponseType\\\":0,\\\"ErrorList\\\":[]}\"";
            dynamic jObj = JsonConvert.DeserializeObject(sResponse);

            NotificationResponseMessage notificationResponseMessage = JsonConvert.DeserializeObject<NotificationResponseMessage>(jObj);
            System.Diagnostics.Debug.WriteLine(notificationResponseMessage.NotifictionId);



        }
    }
}
